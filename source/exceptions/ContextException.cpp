#include "exceptions/ContextException.hpp"

ContextException::ContextException(const std::string &file, int line, const std::string function, const std::string &msg, std::exception_ptr nested_ex) :
    NestedException(msg, nested_ex),
    _file(file),
    _function(function),
    _line(line)
{}

const std::string &ContextException::getFile() const
{
    return  _file;
}

int ContextException::getLine() const
{
    return _line;
}

const std::string ContextException::getFormattedMessage() const
{
    return _file + ":" + _function + ":" + std::to_string(_line) + ": " +  _msg;
}

const std::string ContextException::getClass() const
{
    return "ContextException";
}
