#include "exceptions/NestedException.hpp"

#include <sstream>

NestedException::NestedException(const std::string &msg, std::exception_ptr nested_ex) :
    _msg(msg),
    _nested(nested_ex)
{}

const std::exception_ptr NestedException::getNestedException() const
{
    return _nested;
}

// LCOV_EXCL_START
const std::string NestedException::getClass() const
{
    return "NestedException";
}

const std::string NestedException::getFormattedMessage() const
{
    return getClass() + " : " + _msg;
}
// LCOV_EXCL_STOP

const std::string NestedException::getMessage() const
{
    return _msg;
}

// LCOV_EXCL_START
const char *NestedException::what() const noexcept
{
    static std::string res;
    std::stringstream s;

    s << getFormattedMessage();
    if (_nested) {
        std::string msg;
        try {
            std::rethrow_exception(_nested);
        } catch (const std::exception &e) {
            msg = e.what();
        } catch (...) {
            msg = "Unknow exception";
        }
        std::stringstream in(msg);
        std::string tmp;

        s << std::endl << " Nested exception was : " << std::endl;
        while (std::getline(in, tmp)) {
            s << "    " << tmp << std::endl;
        }
    } else {
        s << std::endl;
    }
    res = s.str();
    return res.c_str();
}
// LCOV_EXCL_STOP
