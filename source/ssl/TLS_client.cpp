#include "logger/Logger.hpp"

#include "ssl/TLS_client.hpp"
#include "ssl/TLS_error.hpp"

#include <arpa/inet.h>
#include <openssl/err.h>
#include <unistd.h>

TLS_client::TLS_client(int fd, SSL_CTX *ctx, const struct sockaddr_in &addr, std::unique_ptr<IProtocol> protocol, time_t timeout) :
    _fd(fd),
    _ssl(SSL_new(ctx), SSL_free),
    _client_cert(nullptr, X509_free),
    _addr(addr),
    _protocol(std::move(protocol)),
    _timeout(timeout),
    _timeout_time(time(NULL) + timeout)
{
    int ret = 0;

    if (!_ssl.get()) {
        ERR_print_errors_fp(stderr);
        _deinit();
        throw TLS_error("Error: Failed to create SSL object");
    }
    if (!SSL_set_fd(_ssl.get(), _fd)) {
        ERR_print_errors_fp(stderr);
        _deinit();
        throw TLS_error("Error: Failed to set the socket fd");
    }
    ret = SSL_accept(_ssl.get());
    if (ret <= 0) {
        int err = SSL_get_error(_ssl.get(), ret);

        _deinit();
        _handle_SSL_error(err);
    }
    SSL_set_read_ahead(_ssl.get(), 1);
    Logger::log() << "SSL connection using " << SSL_get_cipher(_ssl.get()) << std::endl;

    // Check if the client has a certificate
    _client_cert.reset(SSL_get_peer_certificate(_ssl.get()));
    if (_client_cert != nullptr) {
        Logger::log() << "Client certificate:" << std::endl;

        char *str = X509_NAME_oneline(X509_get_subject_name(_client_cert.get()), nullptr, 0);
        if (str)
            Logger::log() << "\t subject: " << str << std::endl;
        OPENSSL_free (str);

        str = X509_NAME_oneline(X509_get_issuer_name(_client_cert.get()), nullptr, 0);
        if (str)
            Logger::log() << "\t issuer: " << str << std::endl;
        OPENSSL_free (str);

    /* We could do all sorts of certificate verification stuff here before
       deallocating the certificate. */

    } else
        Logger::log() << "Client does not have certificate." << std::endl;
}

TLS_client::~TLS_client()
{
    char str[INET_ADDRSTRLEN];

    inet_ntop(AF_INET, &_addr.sin_addr, str, INET_ADDRSTRLEN);
    _deinit();
    Logger::log() << "Closing connection from " << str << ", port " << _addr.sin_port << std::endl;
}

std::string TLS_client::read()
{
    int bytes = SSL_pending(_ssl.get());
    char buff[BUFF_SIZE];
    std::string res;

    // TODO test maximum read size
    Logger::log() << "bytes: " << bytes << std::endl;
    while (bytes > 0) {
        int ret = SSL_read(_ssl.get(), buff, BUFF_SIZE);

        if (ret <= 0) {
            try {
                _handle_SSL_error(SSL_get_error(_ssl.get(), ret));
            } catch (TLS_error &e) {
                throw TLS_error(std::string("Read failed: ") + e.what());
            }
            throw TLS_error("Read failed");
        }
        res.append(buff, ret);
        bytes -= ret;
    }
    return res;
}

void TLS_client::write(const std::string &str)
{
    int ret = SSL_write(_ssl.get(), str.c_str(), str.size());

    // TODO test maximum write size
    if (ret <= 0) {
        try {
            _handle_SSL_error(SSL_get_error(_ssl.get(), ret));
        } catch (TLS_error &e) {
            throw TLS_error(std::string("Write failed: ") + e.what());
        }
        throw TLS_error("Write failed");
    }
    _timeout_time = time(NULL) + _timeout;
}

int TLS_client::get_fd() const
{
    return _fd;
}

void TLS_client::_deinit()
{
    if (_shutdown)
        SSL_shutdown(_ssl.get());
    close(_fd);
}

time_t TLS_client::get_timeout_time() const
{
    return _timeout_time;
}

bool TLS_client::handle_commands()
{
    int ret;
    char buff[BUFF_SIZE];

    // Try to read something to check if the socket is still alive
    ret = SSL_peek(_ssl.get(), buff, BUFF_SIZE);
    // If the client closed the connection or an error occured,
    // return false to signal that we have to terminate the connection
    if (SSL_get_shutdown(_ssl.get()) != 0 || ret <= 0) {
        // If the client didn't send shutdown, that mean he force closed the
        // connection, so don't try to close properly
        if (ret == 0)
            _shutdown = false;
        return false;
    }
    try {
        std::string remaining;
        std::string answer = _protocol->handle_command(_buff + this->read(), remaining);

        _buff = std::move(remaining);
        Logger::log() << "A: " << answer.size() << " B: " << _buff.size();
        if (!answer.empty())
            this->write(answer);
        else if (_buff.empty())
            return false;
        return true;
    } catch (TLS_error &e) {
        Logger::error() << "Failed to execute client command: " << e.what() << std::endl;
        return false;
    } catch (const char *e) {
        Logger::error() << "ERROR COMMAND : " << e << std::endl;
        return false;
    }
}

void TLS_client::_handle_SSL_error(int e)
{
    switch (e) {
        case SSL_ERROR_NONE:
            return;
        case SSL_ERROR_ZERO_RETURN:
            throw TLS_error("Not implemented 1");
        case SSL_ERROR_WANT_READ | SSL_ERROR_WANT_WRITE:
            throw TLS_error("Not implemented 2");
        case SSL_ERROR_WANT_CONNECT | SSL_ERROR_WANT_ACCEPT:
            throw TLS_error("Not implemented 3");
        case SSL_ERROR_WANT_X509_LOOKUP:
            throw TLS_error("Not implemented 4");
        case SSL_ERROR_WANT_ASYNC:
            throw TLS_error("Not implemented 5");
        case SSL_ERROR_WANT_ASYNC_JOB:
            throw TLS_error("Not implemented 6");
        case SSL_ERROR_WANT_CLIENT_HELLO_CB:
            throw TLS_error("Not implemented 7");
        case SSL_ERROR_SYSCALL:
            _shutdown = false;
            ERR_print_errors_fp(stderr);
            throw TLS_error("Syscall error", true);
        case SSL_ERROR_SSL:
            _shutdown = false;
            ERR_print_errors_fp(stderr);
            throw TLS_error("SSL protocol error");
        default:
            Logger::error() << "Unknown SSL error code : " << e;
            return;
    }
}
