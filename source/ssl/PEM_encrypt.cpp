#include "ssl/PEM_encrypt.hpp"
#include "exceptions/ContextException.hpp"
#include "logger/Logger.hpp"

#include <openssl/err.h>

#include <memory>
#include <functional>
#include <filesystem> // NOTE require c++17

PEM_encrypt::PEM_encrypt(const std::string &filename) :
    _pkey(nullptr, EVP_PKEY_free)
{
    FILE *fp = fopen(filename.c_str(), "r");

    if (!fp)
        throw NewContextException("Error opening private key file : " + filename);
    _pkey.reset(PEM_read_PrivateKey(fp, nullptr, nullptr, nullptr));
    fclose(fp);
    if (!_pkey)
        throw NewContextException("Error loading private key : " + filename);
    _out_block_size = EVP_PKEY_size(_pkey.get());
    _in_block_size = _out_block_size / 2;
}

size_t PEM_encrypt::get_in_size() const
{
    return _in_block_size;
}

size_t PEM_encrypt::get_out_size() const
{
    return _out_block_size;
}

bool PEM_encrypt::encrypt_file(std::fstream &src, std::fstream &dst)
{
    std::unique_ptr<EVP_PKEY_CTX, std::function<void(EVP_PKEY_CTX *)>> ctx = {EVP_PKEY_CTX_new(_pkey.get(), nullptr), EVP_PKEY_CTX_free};
    int err = EVP_PKEY_encrypt_init(ctx.get());
    size_t count;
    char buff[_in_block_size];
    size_t outlen;

    if (err <= 0) {
        Logger::error() << "Error init PKEY : " << ERR_error_string(err, nullptr);
        return false;
    }
    err = EVP_PKEY_CTX_set_rsa_padding(ctx.get(), RSA_PKCS1_OAEP_PADDING);
    if (err <= 0) {
        Logger::error() << "Error set padding : " << ERR_error_string(err, nullptr);
        return false;
    }
    do {
        std::unique_ptr<unsigned char, std::function<void(void *)>> tmp = {nullptr, [](void *ptr){OPENSSL_free(ptr);}};

        src.read(buff, _in_block_size);
        count = src.gcount();
        if (count == 0)
            break;
        // First call with nullptr to get the size to malloc
        err = EVP_PKEY_encrypt(ctx.get(), nullptr, &outlen, reinterpret_cast<unsigned char *>(buff), count);
        if (err <= 0) {
            Logger::error() << "Error pre-encrypt : " << ERR_error_string(err, nullptr);
            return false;
        }
        tmp.reset(static_cast<unsigned char *>(OPENSSL_malloc(outlen)));
        err = EVP_PKEY_encrypt(ctx.get(), tmp.get(), &outlen, reinterpret_cast<unsigned char *>(buff), count);
        if (err <= 0) {
            Logger::error() << "Error encrypt : " << ERR_error_string(err, nullptr);
            return false;
        }
        dst.write(reinterpret_cast<char *>(tmp.get()), outlen);
        if (dst.fail()) {
            Logger::error() << "Failed to write";
            return false;
        }
        tmp = nullptr;
    } while (count == _in_block_size);
    return true;
}

// TODO Change this function to use ostream and istream instead of fstreams
bool PEM_encrypt::decrypt_file(std::fstream &in, std::fstream &out)
{
    std::unique_ptr<EVP_PKEY_CTX, std::function<void(EVP_PKEY_CTX *)>> ctx = {EVP_PKEY_CTX_new(_pkey.get(), nullptr), EVP_PKEY_CTX_free};
    int err = EVP_PKEY_decrypt_init(ctx.get());
    size_t count;
    char buff[_out_block_size];
    size_t outlen;

    if (err <= 0) {
        Logger::error() << "Error init PKEY : " << ERR_error_string(err, nullptr);
        return false;
    }
    err = EVP_PKEY_CTX_set_rsa_padding(ctx.get(), RSA_PKCS1_OAEP_PADDING);
    if (err <= 0) {
        Logger::error() << "Error set padding : " << ERR_error_string(err, nullptr);
        return false;
    }
    do {
        std::unique_ptr<unsigned char, std::function<void(void *)>> tmp = {nullptr, [](void *ptr){OPENSSL_free(ptr);}};

        in.read(buff, _out_block_size);
        count = in.gcount();
        if (count == 0)
            break;
        // First call with nullptr to get the size to malloc
        err = EVP_PKEY_decrypt(ctx.get(), nullptr, &outlen, reinterpret_cast<unsigned char *>(buff), count);
        if (err <= 0) {
            Logger::error() << "Error pre-decrypt : " << ERR_error_string(err, nullptr);
            return false;
        }
        tmp.reset(static_cast<unsigned char *>(OPENSSL_malloc(outlen)));
        err = EVP_PKEY_decrypt(ctx.get(), tmp.get(), &outlen, reinterpret_cast<unsigned char *>(buff), count);
        if (err <= 0) {
            Logger::error() << "Error decrypt : " << ERR_error_string(err, nullptr);
            return false;
        }
        out.write(reinterpret_cast<char *>(tmp.get()), outlen);
        if (out.fail()) {
            Logger::error() << "Failed to write";
            return false;
        }
        tmp = nullptr;
    } while (count == _out_block_size);
    return true;
}
