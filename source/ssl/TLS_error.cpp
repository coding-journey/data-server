#include "ssl/TLS_error.hpp"

#include <errno.h>
#include <string.h>

TLS_error::TLS_error(const std::string &err, bool use_errno) :
    _err(err)
{
    if (use_errno) {
        _err += ": ";
        _err += strerror(errno);
    }
}

const char *TLS_error::what() const noexcept
{
    return _err.c_str();
}
