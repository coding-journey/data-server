#include <sqlite_orm/sqlite_orm.h>

#include "logger/Logger.hpp"

#include "sqlite_orm/database_schema.hpp"

static auto init_storage(const std::string &path)
{
    return sqlite_orm::make_storage(path,
        sqlite_orm::make_table("CRASH_REPORT",
            sqlite_orm::make_column("id", &CrashReport::id, sqlite_orm::primary_key(), sqlite_orm::autoincrement()),
            sqlite_orm::make_column("issue_id", &CrashReport::issue_id),
            sqlite_orm::make_column("stacktrace", &CrashReport::stacktrace),
            sqlite_orm::make_column("message", &CrashReport::message),
            sqlite_orm::make_column("scene_name", &CrashReport::scene_name),
            sqlite_orm::make_column("software", &CrashReport::software),
            sqlite_orm::make_column("operating_system", &CrashReport::operating_system),
            sqlite_orm::make_column("ram_usage", &CrashReport::ram_usage),
            sqlite_orm::make_column("date", &CrashReport::date),
            sqlite_orm::make_column("player_id", &CrashReport::player_id),
            sqlite_orm::make_column("uuid", &CrashReport::uuid),
            sqlite_orm::make_column("count", &CrashReport::count),
            sqlite_orm::foreign_key(&CrashReport::issue_id).references(&Issue::id).on_delete.cascade()
        ),
        sqlite_orm::make_table("ISSUE",
            sqlite_orm::make_column("id", &Issue::id, sqlite_orm::primary_key(), sqlite_orm::autoincrement()),
            sqlite_orm::make_column("report_id", &Issue::original_report_id),
            sqlite_orm::make_column("total_count", &Issue::total_count),
            sqlite_orm::foreign_key(&Issue::original_report_id).references(&CrashReport::id)
        )
    );
}

static std::string SoftwareTypeToString(SoftwareType soft)
{
    switch (soft) {
        case SoftwareType::Game:
            return "game";
        case SoftwareType::Editor:
            return "editor";
        default:
            throw std::domain_error("Invalid SoftwareType enum value");
    }
}

static std::string OperatingSystemToString(OperatingSystem os)
{
    switch (os) {
        case OperatingSystem::Other:
            return "other";
        case OperatingSystem::Linux:
            return "linux";
        case OperatingSystem::Windows:
            return "windows";
        case OperatingSystem::Mac:
            return "mac";
        default:
            throw std::domain_error("Invalid OperatingSystem enum value");
    }
}

using Storage = decltype(init_storage(""));

namespace sqlite_orm {
    template<>
    struct type_printer<OperatingSystem> : public text_printer {};
    template<>
    struct type_printer<SoftwareType> : public text_printer {};

    template<>
    struct statement_binder<OperatingSystem> {
        int bind(sqlite3_stmt *stmt, int index, const OperatingSystem &value) {
            return statement_binder<std::string>().bind(stmt, index, OperatingSystemToString(value));
        }
    };
    template<>
    struct statement_binder<SoftwareType> {
        int bind(sqlite3_stmt *stmt, int index, const SoftwareType &value) {
            return statement_binder<std::string>().bind(stmt, index, SoftwareTypeToString(value));
        }
    };

    template<>
    struct field_printer<OperatingSystem> {
        std::string operator()(const OperatingSystem &t) const {
            return OperatingSystemToString(t);
        }
    };
    template<>
    struct field_printer<SoftwareType> {
        std::string operator()(const SoftwareType &t) const {
            return SoftwareTypeToString(t);
        }
    };

    template<>
    struct row_extractor<OperatingSystem> {
        OperatingSystem extract(const char *row_value) {
            std::string value = row_value;

            if (value == "other") {
                return OperatingSystem::Other;
            } else if (value == "linux") {
                return OperatingSystem::Linux;
            } else if (value == "windows") {
                return OperatingSystem::Windows;
            } else if (value == "mac") {
                return OperatingSystem::Mac;
            } else {
                throw std::runtime_error("incorrect OperatingSystem string (" + value + ")");
            }
        }

        OperatingSystem extract(sqlite3_stmt *stmt, int columnIndex) {
            auto value = sqlite3_column_text(stmt, columnIndex);
            return this->extract((const char *)value);
        }
    };
    template<>
    struct row_extractor<SoftwareType> {
        SoftwareType extract(const char *row_value) {
            std::string value = row_value;

            if (value == "editor") {
                return SoftwareType::Editor;
            } else if (value == "game") {
                return SoftwareType::Game;
            } else {
                throw std::runtime_error("incorrect SoftwareType string (" + value + ")");
            }
        }

        SoftwareType extract(sqlite3_stmt *stmt, int columnIndex) {
            auto value = sqlite3_column_text(stmt, columnIndex);
            return this->extract((const char *)value);
        }
    };
} // sqlite_orm

struct db_holder {
    Storage db;
};

#include <iostream>

SQLite_database::SQLite_database(const std::string &path) :
    _db(new db_holder{init_storage(path)})
{
    _db->db.storage_base::on_open = [](sqlite3 *ptr) {
        if (sqlite3_exec(ptr, "PRAGMA foreign_keys = ON;", nullptr, nullptr, nullptr) != SQLITE_OK)
            Logger::warn() << "SQLITE PRAGMA FAILED";
    };
    _db->db.sync_schema();
}

SQLite_database::~SQLite_database()
{
    delete _db;
}

void SQLite_database::report_crash(const std::string &stacktrace, const std::string &message, std::shared_ptr<std::string> scene_name, SoftwareType software, OperatingSystem os, float ram, const std::string &uuid, std::shared_ptr<std::string> playerid)
{
    using namespace sqlite_orm;

    auto issues = _db->db.select(&CrashReport::issue_id, where(c(&CrashReport::stacktrace) == stacktrace
                   and c(&CrashReport::message) == message
                   and c(&CrashReport::software) == software
                   and c(&CrashReport::scene_name) == scene_name
//                   and c(&CrashReport::operating_system) == os
    ));
    auto dup_report = _db->db.select(&CrashReport::id, where(c(&CrashReport::stacktrace) == stacktrace
                   and c(&CrashReport::message) == message
                   and c(&CrashReport::software) == software
                   and c(&CrashReport::scene_name) == scene_name
                   and c(&CrashReport::operating_system) == os
                   and c(&CrashReport::uuid) == uuid
    ));
    CrashReport report{
        .id = 0,
        .issue_id = 0,
        .stacktrace = stacktrace,
        .message = message,
        .scene_name = std::move(scene_name),
        .software = software,
        .operating_system = os,
        .ram_usage = ram,
        .date = _db->db.select(datetime("now")).front(),
        .player_id = std::move(playerid),
        .uuid = uuid,
        .count = 1
    };

    if (dup_report.size() != 0) {
        auto dup_rep = _db->db.get<CrashReport>(dup_report.front());
        auto issue = _db->db.get<Issue>(dup_rep.issue_id);

        dup_rep.count++;
        issue.total_count++;
        _db->db.transaction([&] () {
            _db->db.update(dup_rep);
            _db->db.update(issue);
            return true;
        });
        return;
    }
    if (issues.size() != 0) {
        auto issue = _db->db.get<Issue>(issues.front());

        report.issue_id = issue.id;
        issue.total_count++;
        _db->db.transaction([&] () {
            _db->db.update(issue);
            _db->db.insert(report);
            return true;
        });
    } else {
        Issue issue {
            .id = 0,
            .original_report_id = nullptr,
            .total_count = 1,
        };

        _db->db.transaction([&] () {
            issue.id = _db->db.insert(issue);
            report.issue_id = issue.id;
            issue.original_report_id = std::make_unique<int>(_db->db.insert(report));
            _db->db.update(issue);
            return true;
        });
    }
}
