#include "cj_protocol/5.1.1/CJ_Protocol.hpp"
#include "cj_protocol/5.1.1/CommandData.hpp"
#include "cj_protocol/BitString.hpp"

#include <bits/stdc++.h>

using namespace CJ_Protocol_5_1_1;

bool ICommandData::needLogin() const
{
    return _needLogin;
}

void ICommandData::setNeedLogin(bool value)
{
    _needLogin = value;
}

std::uint16_t ICommandData::getSize() const
{
    return _size;
}

void ICommandData::setSize(std::uint16_t value)
{
    _size = value;
}

ICommandData::ICommandData(std::uint16_t size, const std::string &) :
    _size(size),
    _needLogin(true)
{}

NoData::NoData(std::uint16_t size, const std::string &data) :
    ICommandData(size, data)
{
    setNeedLogin(false);
}

NoDataLogin::NoDataLogin(std::uint16_t size, const std::string &data) :
    ICommandData(size, data)
{}

std::string NoDataLogin::display() const
{
    return "NoDataLogin()";
}

std::string NoData::display() const
{
    return "NoData()";
}

RequestVersionData::RequestVersionData(std::uint16_t size, const std::string &data) :
    NoData(size, data)
{
    BitString str(data);

    _version = str.extract_bits<std::uint16_t>();
    str.insert_bits(data, 16); // Reload to extract for each subversion
    _major = str.extract_bits<std::uint8_t>(3);
    _minor = str.extract_bits<std::uint8_t>(5);
    _patch = str.extract_bits<std::uint8_t>();
}

std::string RequestVersionData::display() const
{
    std::stringstream ss;

    ss << "RequestVersionData(";
    ss << "Version: " << _major + 1 << "." << _minor + 1 << "." << _patch + 1 << ")";
    return ss.str();
}

std::uint16_t RequestVersionData::getVersion() const noexcept
{
    return _version;
}

std::uint8_t RequestVersionData::getMajor() const noexcept
{
    return _major;
}

std::uint8_t RequestVersionData::getMinor() const noexcept
{
    return _minor;
}

std::uint8_t RequestVersionData::getPatch() const noexcept
{
    return _patch;
}

DeleteAccountData::DeleteAccountData(std::uint16_t _size, const std::string &data) :
    ICommandData(_size, data)
{
    BitString str(data);
    auto size = str.extract_bits<std::uint8_t>();

    _token = str.extract_bytes<std::string>(size);
}

std::string DeleteAccountData::display() const
{
    std::stringstream ss;

    ss << "DeleteAccountData(";
    ss << "Token: " << _token << ")";
    return ss.str();
}

std::string DeleteAccountData::getToken() const
{
    return _token;
}

LoginData::LoginData(std::uint16_t size, const std::string &data) :
    NoData(size, data)
{
    BitString str(data);

    _is_token = str.extract_bits<std::uint8_t>(1);
    str.extract_bits<std::uint8_t>(7); // Unused
    if (_is_token) {
        _token = str.extract_bytes<std::string>(Protocol::token_size);
    } else {
        auto size = str.extract_bits<std::uint8_t>();

        _username = str.extract_bytes<std::string>(size);
        size = str.extract_bits<std::uint8_t>();
        _password = str.extract_bytes<std::string>(size);
    }
}

std::string LoginData::display() const
{
    std::stringstream ss;

    ss << "LoginData(";
    if (_is_token)
        ss << "Token: " << _token << ")";
    else {
        ss << "Username: " << _username << ", ";
        ss << "Password: " << std::string(_password.size(), '*') << ")";
    }
    return ss.str();
}

std::string LoginData::getToken() const noexcept
{
    return _token;
}

std::string LoginData::getUsername() const noexcept
{
    return _username;
}

std::string LoginData::getPassword() const noexcept
{
    return _password;
}

bool LoginData::isToken() const noexcept
{
    return _is_token;
}

RegisterData::RegisterData(std::uint16_t _size, const std::string &data) :
    NoData(_size, data)
{
    BitString str(data);
    auto size = str.extract_bits<std::uint8_t>();

    _email = str.extract_bytes<std::string>(size);
    size = str.extract_bits<std::uint8_t>();
    _username = str.extract_bytes<std::string>(size);
    size = str.extract_bits<std::uint8_t>();
    _password = str.extract_bytes<std::string>(size);
}

std::string RegisterData::display() const
{
    std::stringstream ss;

    ss << "RegisterData(";
    ss << "Email: " << _email << ", ";
    ss << "Username: " << _username << ", ";
    ss << "Password: " << std::string(_password.size(), '*') << ")";
    return ss.str();
}

std::string RegisterData::getUsername() const noexcept
{
    return _username;
}

std::string RegisterData::getPassword() const noexcept
{
    return _password;
}

std::string RegisterData::getEmail() const noexcept
{
    return _email;
}

DownloadData::DownloadData(std::uint16_t _size, const std::string &data) :
    ICommandData(_size, data)
{
    BitString str(data);
    auto size = str.extract_bits<std::uint8_t>();

    _filename = str.extract_bytes<std::string>(size);
}

std::string DownloadData::display() const
{
    std::stringstream ss;

    ss << "DownloadData(";
    ss << "Filename: " << _filename << ")";
    return ss.str();
}

std::string DownloadData::getFilename() const
{
    return _filename;
}

UploadData::UploadData(std::uint16_t _size, const std::string &data) :
    ICommandData(_size, data)
{
    BitString str(data);
    std::uint16_t size = str.extract_bits<std::uint8_t>();

    _filename = str.extract_bytes<std::string>(size);
    _timestamp = str.extract_bits<std::uint64_t>();
    _public = str.extract_bits<std::uint8_t>(1);
    str.extract_bits<std::uint8_t>(4); // UNUSED bits
    size = str.extract_bits<std::uint16_t>(11);
    if (size >= Protocol::fragmented_size) {
        size = str.extract_bits<std::uint16_t>();
        for (auto i = 0; i < size; i++) {
            std::uint16_t block_size = str.extract_bits<std::uint16_t>();

            _content += str.extract_bytes<std::string>(block_size);
        }
    } else {
        _content = str.extract_bytes<std::string>(size);
    }
}

std::string UploadData::display() const
{
    std::stringstream ss;

    ss << "UploadData(";
    ss << "Filename: " << _filename << ", ";
    ss << "Timestamp: " << _timestamp << ", ";
    ss << "Public: " << std::to_string(_public) << ", ";
    return ss.str();
}

void UploadData::unfragment(const std::string &data)
{
    _content = data;
}

std::string UploadData::getFilename() const
{
    return _filename;
}

std::uint64_t UploadData::getTimestamp() const
{
    return _timestamp;
}

std::string UploadData::getContent() const
{
    return _content;
}

bool UploadData::isPublic() const
{
    return _public;
}

UploadProjectData::UploadProjectData(std::uint16_t _size, const std::string &data) :
    ICommandData(_size, data)
{
    BitString str(data);
    std::uint16_t size = str.extract_bits<std::uint8_t>();

    _filename = str.extract_bytes<std::string>(size);
    _timestamp = str.extract_bits<std::uint64_t>();
    _game_version = str.extract_bits<std::uint8_t>();
    _metadata = str.extract_bits<std::uint8_t>(1);
    str.extract_bits<std::uint8_t>(4); // UNUSED bits
    size = str.extract_bits<std::uint16_t>(11);
    if (size >= Protocol::fragmented_size) {
        size = str.extract_bits<std::uint16_t>();
        for (auto i = 0; i < size; i++) {
            std::uint16_t block_size = str.extract_bits<std::uint16_t>();

            _content += str.extract_bytes<std::string>(block_size);
        }
    } else {
        _content = str.extract_bytes<std::string>(size);
    }
}

std::string UploadProjectData::display() const
{
    std::stringstream ss;

    ss << "UploadProjectData(";
    ss << "Filename: " << _filename << ", ";
    ss << "Timestamp: " << _timestamp << ", ";
    ss << "Is Metadata: " << std::to_string(_metadata) << ", ";
    return ss.str();
}

void UploadProjectData::unfragment(const std::string &data)
{
    _content = data;
}

std::string UploadProjectData::getFilename() const
{
    return _filename;
}

std::uint64_t UploadProjectData::getTimestamp() const
{
    return _timestamp;
}

std::string UploadProjectData::getContent() const
{
    return _content;
}

std::uint8_t UploadProjectData::getGameVersion() const
{
    return _game_version;
}

bool UploadProjectData::isMetadata() const
{
    return _metadata;
}

ListPublicFilesData::ListPublicFilesData(std::uint16_t _size, const std::string &data) :
    NoData(_size, data)
{
    BitString str(data);
    auto size = str.extract_bits<std::uint8_t>();

    _username = str.extract_bytes<std::string>(size);
}

std::string ListPublicFilesData::display() const
{
    std::stringstream ss;

    ss << "ListPublicFilesData(";
    ss << "Username: " << _username << ")";
    return ss.str();
}

const std::string ListPublicFilesData::getUsername() const
{
    return _username;
}

DownloadPublicFileData::DownloadPublicFileData(std::uint16_t _size, const std::string &data) :
    NoData(_size, data)
{
    BitString str(data);
    auto size = str.extract_bits<std::uint8_t>();

    _username = str.extract_bytes<std::string>(size);
    size = str.extract_bits<std::uint8_t>();
    _filename = str.extract_bytes<std::string>(size);
}

std::string DownloadPublicFileData::display() const
{
    std::stringstream ss;

    ss << "DownloadPublicFileData(";
    ss << "Username: " << _username << ", ";
    ss << "Filename: " << _filename << ")";
    return ss.str();
}

const std::string DownloadPublicFileData::getUsername() const
{
    return _username;
}

const std::string DownloadPublicFileData::getFilename() const
{
    return _filename;
}


GetCategoriesData::GetCategoriesData(std::uint16_t size, const std::string &data) :
    NoData(size, data)
{
    BitString str(data);
    auto len = str.extract_bits<std::uint8_t>();

    _lang = str.extract_bytes<std::string>(len);
}

std::string GetCategoriesData::display() const
{
    std::stringstream ss;

    ss << "GetCategoriesData(";
    ss << "Language: " << _lang << ")";
    return ss.str();
}

const std::string GetCategoriesData::getLanguage() const
{
    return _lang;
}

GetFunctionsData::GetFunctionsData(std::uint16_t size, const std::string &data) :
    NoData(size, data)
{
    BitString str(data);
    auto len = str.extract_bits<std::uint8_t>();

    _lang = str.extract_bytes<std::string>(len);
    len = str.extract_bits<std::uint8_t>();
    _categ = str.extract_bytes<std::string>(len);
}

std::string GetFunctionsData::display() const
{
    std::stringstream ss;

    ss << "GetFunctionsData(";
    ss << "Language: " << _lang << ", ";
    ss << "Categorie: " << _categ << ")";
    return ss.str();
}

const std::string GetFunctionsData::getLanguage() const
{
    return _lang;
}

const std::string GetFunctionsData::getCategory() const
{
    return _categ;
}

GetFunctionData::GetFunctionData(std::uint16_t size, const std::string &data):
    NoData(size, data)
{
    BitString str(data);
    auto len = str.extract_bits<std::uint8_t>();

    _lang = str.extract_bytes<std::string>(len);
    len = str.extract_bits<std::uint8_t>();
    _name = str.extract_bytes<std::string>(len);
}

std::string GetFunctionData::display() const
{
    std::stringstream ss;

    ss << "GetFunctionData(";
    ss << "Language: " << _lang << ", ";
    ss << "Function Name: " << _name << ")";
    return ss.str();
}

const std::string GetFunctionData::getLanguage() const
{
    return _lang;
}

const std::string GetFunctionData::getName() const
{
    return _name;
}

ReportCrashData::ReportCrashData(std::uint16_t size, const std::string &data) :
    NoData(size, data)
{
    BitString str(data);
    bool soft;
    std::uint8_t os;
    float tmp;

    _stacktrace = str.extract_bytes<std::string>(str.extract_bits<std::uint16_t>());
    _message = str.extract_bytes<std::string>(str.extract_bits<std::uint16_t>());
    _scene_name = str.extract_bytes<std::string>(str.extract_bits<std::uint8_t>());
    soft = str.extract_bits<std::uint8_t>(1);
    str.extract_bits<std::uint8_t>(5); // UNUSED
    os = str.extract_bits<std::uint8_t>(2);
    _ram = str.extract_bits<std::uint8_t>();
    tmp = str.extract_bits<std::uint8_t>();
    while (tmp > 1)
        tmp = tmp / 10;
    _ram += tmp;
    _soft = (soft ? SoftwareType::Editor : SoftwareType::Game);
    switch (os) {
        case 1:
            _os = OperatingSystem::Linux;
            break;
        case 2:
            _os = OperatingSystem::Windows;
            break;
        case 3:
            _os = OperatingSystem::Mac;
            break;
        default:
            _os = OperatingSystem::Other;
    }
    _uuid = str.extract_bytes<std::string>(36);
}

std::string ReportCrashData::display() const
{
    std::stringstream ss;

    ss << "ReportCrashData(";
    ss << "Message: " << _message << ", ";
    ss << "SceneName: " << _scene_name << ", ";
    ss << "Ram: " << _ram << "%, ";
    ss << "Software: " << (_soft == SoftwareType::Editor ? "Editor" : "Game") << ", ";
    ss << "OS: ";
    switch (_os) {
        case OperatingSystem::Linux:
            ss << "Linux";
            break;
        case OperatingSystem::Windows:
            ss << "Windows";
            break;
        case OperatingSystem::Mac:
            ss << "Mac";
            break;
        default:
            ss << "Other";
            break;
    }
    ss << ")";
    return ss.str();
}

const std::string ReportCrashData::getSceneName() const
{
    return _scene_name;
}

const std::string ReportCrashData::getStacktrace() const
{
    return _stacktrace;
}

const std::string ReportCrashData::getMessage() const
{
    return _message;
}

SoftwareType ReportCrashData::getSoftware() const
{
    return _soft;
}

OperatingSystem ReportCrashData::getOS() const
{
    return _os;
}

float ReportCrashData::getRAM() const
{
    return _ram;
}

const std::string ReportCrashData::getUUID() const
{
    return _uuid;
}
