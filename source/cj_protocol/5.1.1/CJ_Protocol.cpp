#include <algorithm>

#include "cj_protocol/5.1.1/CJ_Protocol.hpp"
#include "cj_protocol/BitString.hpp"
#include "cj_protocol/CJ_ProtocolManager.hpp"
#include "cj_protocol/CustomFileData.hpp"
#include "cj_protocol/TMP_file.hpp"
#include "cj_protocol/CJProtocolExceptions.hpp"

#include "database/ISerialisableData.hpp"

#include "logger/Logger.hpp"

using namespace CJ_Protocol_5_1_1;

const std::uint8_t Protocol::major_version = 3; // 0-7
const std::uint8_t Protocol::minor_version = 0; // 0-31
const std::uint8_t Protocol::patch_version = 0; // 0-255

const std::uint8_t Protocol::token_size = 36;
const std::uint16_t Protocol::fragmented_size = 2047;

// TODO Remove this fonction and use the VersionResponseData class
static std::uint16_t convert_version(std::uint8_t major, std::uint8_t minor, std::uint8_t patch)
{
    BitString str;

    str.insert_bits<std::uint8_t>(major, 3);
    str.insert_bits<std::uint8_t>(minor, 5);
    str.insert_bits<std::uint8_t>(patch, 8);
    return str.extract_bits<std::uint16_t>();
}

// TODO test units on this class (possible so do it)
Protocol::Protocol(CJ_ProtocolManager *m) :
    _version(convert_version(major_version, minor_version, patch_version)),
    _m(m),
    _dbs(_m->getDBs())
{
    Logger::log() << "Using CJ Protocol Version " <<
        major_version + 1 << "." << minor_version + 1 << "." << patch_version + 1;
}

std::uint64_t Protocol::getVersion() const
{
    return _version;
}

template<typename T, class A>
static T *dynamic_cast_throw(A *ptr)
{
    T *res = dynamic_cast<T *>(ptr); // TODO replace with std::dynamic_pointer_cast

    if (res)
        return res;
    throw "Err"; // TODO
}

void Protocol::disconnect()
{
    _user = nullptr;
    _db = nullptr;
    _delete_token = "";
}

Response Protocol::create_response(const IResponseData &data, CommandCode code, StatusCode status)
{
    return Response(status, code, data.getSize(), data.toString());
}

// TODO split in sub functions
Response Protocol::process_command(Command &cmd)
{
    if (cmd._data == nullptr)
        return Response(StatusCode::ERR_UNKNOWN_COMMAND, cmd._command_code);
    if (!_user && cmd._data->needLogin())
        return Response(StatusCode::ERR_MISSING_LOGIN, cmd._command_code);
    switch (cmd._command_code) {
        case CommandCode::VERSION:
            return create_response(
                VersionResponseData(Protocol::major_version, Protocol::minor_version, Protocol::patch_version),
                CommandCode::VERSION);
        case CommandCode::REQUEST_SERVER_VERSION: {
            auto *data = dynamic_cast_throw<RequestVersionData>(cmd._data.get());

            try {
                std::uint16_t version = _m->change_version(data->getMajor(), data->getMinor(), data->getPatch());

                return create_response(VersionResponseData(version), CommandCode::REQUEST_SERVER_VERSION);
            } catch (...) {
                return Response(StatusCode::ERR_VERSION, CommandCode::REQUEST_SERVER_VERSION);
            }
        }
        case CommandCode::DELETE_ACCOUNT: {
            auto *data = dynamic_cast_throw<DeleteAccountData>(cmd._data.get());

            if (_delete_token == "" || _delete_token != data->getToken()) {
                _delete_token = UserData::generateToken();
                return create_response(DeleteAccountResponseData(_delete_token), CommandCode::DELETE_ACCOUNT);
            }
            _db->drop_database();
            _dbs.getUsersDB()->remove(_user->getToken());
            this->disconnect();
            return create_response(DeleteAccountResponseData(""), CommandCode::DELETE_ACCOUNT);
        }
        case CommandCode::REGISTER: {
            auto *data = dynamic_cast_throw<RegisterData>(cmd._data.get());
            auto &users_db = _dbs.getUsersDB();
            std::shared_ptr<UserData> user;
            bool username_dup = false;

            if (!UserData::checkPassword(data->getPassword()))
                return Response(StatusCode::ERR_INVALID_PASSWORD, CommandCode::REGISTER);
            user = users_db->querry_file([&data, &username_dup](const std::shared_ptr<ISerialisableData> &user) -> bool {
                auto tmp = std::dynamic_pointer_cast<UserData>(user);

                if (!tmp)
                    return false;
                if  (tmp->getUsername() == data->getUsername()) {
                    username_dup = true;
                    return true;
                }
                return tmp->getEmail() == data->getEmail();
            });
            if (user)
                return Response(username_dup ? StatusCode::ERR_USERNAME_TAKEN : StatusCode::ERR_EMAIL_TAKEN, CommandCode::REGISTER);
            _user = std::make_shared<UserData>(data->getEmail(), data->getUsername(), UserData::hashPassword(data->getPassword()), UserData::generateToken());
            users_db->insert(_user);
            _db = _dbs.getDatabases()->createDatabase(_user->getUsername());
            return create_response(LoginResponseData(*_user), CommandCode::REGISTER);
        }
        case CommandCode::LOGIN: {
            auto *data = dynamic_cast_throw<LoginData>(cmd._data.get());
            auto &users_db = _dbs.getUsersDB();
            std::shared_ptr<UserData> user;

            if (!data->isToken()) {
                user = users_db->querry_file([&data](const std::shared_ptr<ISerialisableData> &user) -> bool {
                    auto tmp = std::dynamic_pointer_cast<UserData>(user);

                    if (!tmp)
                        return false;
                    return tmp->getUsername() == data->getUsername() || tmp->getEmail() == data->getUsername();
                });
                if (!user || !user->verifyPassword(data->getPassword()))
                    user = nullptr;
            } else {
                user = users_db->querry_file(data->getToken());
            }
            if (!user)
                return Response(StatusCode::ERR_INVALID_LOGIN, CommandCode::LOGIN);
            _user = user;
            _db = _dbs.getDatabases()->createDatabase(user->getUsername());
            return create_response(LoginResponseData(*user), CommandCode::LOGIN);
        }
        case CommandCode::LOGOUT: {
            _user->setToken(_user->generateToken());
            this->disconnect();
            return Response(StatusCode::STATUS_OK, CommandCode::LOGOUT);
        }
        case CommandCode::LIST_FILES: {
            auto files = _db->list_files();
            std::vector<std::shared_ptr<const FileData>> vec;

            vec.reserve(files.size());
            std::transform(files.begin(), files.end(), std::back_inserter(vec), [](const auto &p){return p.second;});
            return create_response(ListFilesResponseData(vec), CommandCode::LIST_FILES);
        }
        case CommandCode::DOWNLOAD_FILE: {
            auto *data = dynamic_cast_throw<DownloadData>(cmd._data.get());
            auto file = _db->querry_file(data->getFilename());

            if (!file)
                return Response(StatusCode::ERR_NO_SUCH_FILE, CommandCode::DOWNLOAD_FILE);
            return create_response(DownloadResponseData(file), CommandCode::DOWNLOAD_FILE);
        }
        case CommandCode::UPLOAD_FILE: {
            auto *data = dynamic_cast_throw<UploadData>(cmd._data.get());
            std::shared_ptr<FileData> file = std::make_shared<FileData>(
                data->getFilename(), data->getTimestamp(), data->getContent(), CustomFileData(data->isPublic(), false)
            );

            if (_db->size() > std::numeric_limits<std::uint16_t>::max()) // TODO Check
                return Response(StatusCode::ERR_TOO_MANY_FILES, CommandCode::UPLOAD_FILE);
            _db->insert(file);
            return Response(StatusCode::STATUS_OK, CommandCode::UPLOAD_FILE);
        }
        case CommandCode::DELETE_FILE: {
            auto *data = dynamic_cast_throw<DownloadData>(cmd._data.get());

            if (!_db->remove(data->getFilename()))
                return Response(StatusCode::ERR_NO_SUCH_FILE, CommandCode::DELETE_FILE);
            return Response(StatusCode::STATUS_OK, CommandCode::DELETE_FILE);
        }
        case CommandCode::LIST_FILE: {
            auto *data = dynamic_cast_throw<DownloadData>(cmd._data.get());
            auto file = _db->querry_file(data->getFilename());

            if (!file)
                return Response(StatusCode::ERR_NO_SUCH_FILE, CommandCode::LIST_FILE);
            return create_response(ListFileResponseData(file), CommandCode::LIST_FILE);
        }
        case CommandCode::UPLOAD_PROJECT: {
            auto *data = dynamic_cast_throw<UploadProjectData>(cmd._data.get());
            auto &proj_db = _dbs.getProjectsDB();
            const std::string &project_name = data->getFilename();
            const std::string &filename = project_name + (data->isMetadata() ? ".meta.zip" : ".zip");
            std::shared_ptr<ProjectData> project = proj_db->querry_file(_user->getUsername() + "-" + project_name);
            std::shared_ptr<FileData> file = std::make_shared<FileData>(
                filename, data->getTimestamp(), data->getContent(), CustomFileData(true, true)
            );

            if (!project) {
                project = std::make_shared<ProjectData>();
            }
            if (_db->size() > std::numeric_limits<std::uint16_t>::max()) // TODO Check
                return Response(StatusCode::ERR_TOO_MANY_FILES, CommandCode::UPLOAD_PROJECT);
            if (data->isMetadata()) {
                project->setTimestamp(data->getTimestamp());
                project->setMetaFilename(filename);
            } else {
                project->setFilename(filename);
            }
            project->setGameVersion(data->getGameVersion());
            project->setProjectName(project_name);
            project->setOwner(_user->getUsername());
            proj_db->insert(project);
            _db->insert(file);
            return Response(StatusCode::STATUS_OK, CommandCode::UPLOAD_PROJECT);
        }
        case CommandCode::LIST_PROJECTS: {
            auto &projects = _dbs.getProjectsDB()->list_files();
            std::vector<std::shared_ptr<const ProjectData>> vec;

            vec.reserve(projects.size());
            for (const auto &iter : projects) {
                if (!iter.second->getMetaFilename().empty() && !iter.second->getFilename().empty()) // Skip uncomplete project
                    vec.push_back(iter.second);
            }
            return create_response(ListProjectsResponseData(vec), CommandCode::LIST_PROJECTS);
        }
        case CommandCode::DELETE_PROJECT: {
            auto *data = dynamic_cast_throw<DownloadData>(cmd._data.get());
            auto &db = _dbs.getProjectsDB();

            if (db->remove(_user->getUsername() + "-" + data->getFilename()))
                return Response(StatusCode::STATUS_OK, CommandCode::DELETE_PROJECT);
            return Response(StatusCode::ERR_NO_SUCH_PROJECT, CommandCode::DELETE_PROJECT);
        }
        case CommandCode::LIST_USERS: {
            auto users = _dbs.getUsersDB()->list_files();
            std::vector<std::shared_ptr<UserData>> vec;

            vec.reserve(users.size());
            std::transform(users.begin(), users.end(), std::back_inserter(vec), [](const auto &p){return p.second;});
            return create_response(ListUsersResponseData(vec), CommandCode::LIST_USERS);
        }
        case CommandCode::LIST_PUBLIC_FILES: {
            auto *data = dynamic_cast_throw<ListPublicFilesData>(cmd._data.get());
            auto users = _dbs.getUsersDB()->list_files();
            std::unordered_map<std::string, std::shared_ptr<FileData>> files;
            std::vector<std::shared_ptr<const FileData>> vec;

            if (users.end() == std::find_if(users.begin(), users.end(), [&data](auto &user){ return data->getUsername() == user.second->getUsername(); }))
                return Response(StatusCode::ERR_UNKNOWN_USERNAME, CommandCode::LIST_PUBLIC_FILES);
            files = _dbs.getDatabases()->getDatabase(data->getUsername())->list_files();
            for (auto iter = files.begin(); iter != files.end();) {
                if (!iter->second->template getCustomData<CustomFileData>().isPublic())
                    iter = files.erase(iter);
                else
                    iter++;
            }
            vec.reserve(files.size());
            std::transform(files.begin(), files.end(), std::back_inserter(vec), [](const auto &p){return p.second;});
            return create_response(ListFilesResponseData(vec), CommandCode::LIST_PUBLIC_FILES);
        }
        case CommandCode::DOWNLOAD_PUBLIC_FILE: {
            auto *data = dynamic_cast_throw<DownloadPublicFileData>(cmd._data.get());
            auto file = _dbs.getDatabases()->getDatabase(data->getUsername())->querry_file(data->getFilename());
            auto users = _dbs.getUsersDB()->list_files();

            if (users.end() == std::find_if(users.begin(), users.end(), [&data](auto &user){ return data->getUsername() == user.second->getUsername(); }))
                return Response(StatusCode::ERR_UNKNOWN_USERNAME, CommandCode::DOWNLOAD_PUBLIC_FILE);
            if (!file || !file->template getCustomData<CustomFileData>().isPublic())
                return Response(StatusCode::ERR_NO_SUCH_FILE, CommandCode::DOWNLOAD_PUBLIC_FILE);
            return create_response(DownloadResponseData(file), CommandCode::DOWNLOAD_PUBLIC_FILE);
        }
        case CommandCode::GET_LANGUAGES: {
            auto &wiki_db = _dbs.getWikiDB();

            return create_response(ListResponseData<WikiDatabase::languages_t>(wiki_db->getVersion(), wiki_db->getLanguages()), CommandCode::GET_LANGUAGES);
        }
        case CommandCode::GET_CATEGORIES: {
            auto *data = dynamic_cast_throw<GetCategoriesData>(cmd._data.get());
            auto &wiki_db = _dbs.getWikiDB();

            try {
                return create_response(ListResponseData<WikiDatabase::categories_t>(wiki_db->getVersion(), wiki_db->getCategories(data->getLanguage())), CommandCode::GET_CATEGORIES);
            } catch (LanguageNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_LANGUAGE, cmd._command_code);
            }
        }
        case CommandCode::GET_FUNCTIONS_NAMES: {
            auto *data = dynamic_cast_throw<GetFunctionsData>(cmd._data.get());
            auto &wiki_db = _dbs.getWikiDB();

            try {
                return create_response(ListResponseData<WikiDatabase::functions_t>(wiki_db->getVersion(), wiki_db->getFunctions(data->getLanguage(), data->getCategory())), CommandCode::GET_FUNCTIONS_NAMES);
            } catch (LanguageNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_LANGUAGE, cmd._command_code);
            } catch (CategoryNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_CATEGORY, cmd._command_code);
            }
        }
        case CommandCode::GET_FUNCTIONS: {
            auto *data = dynamic_cast_throw<GetFunctionsData>(cmd._data.get());
            auto &wiki_db = _dbs.getWikiDB();

            try {
                return create_response(FunctionsResponseData(wiki_db->getVersion(), wiki_db->getFunctions(data->getLanguage(), data->getCategory())), CommandCode::GET_FUNCTIONS);
            } catch (LanguageNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_LANGUAGE, cmd._command_code);
            } catch (CategoryNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_CATEGORY, cmd._command_code);
            }
        }
        case CommandCode::GET_FUNCTION: {
            auto *data = dynamic_cast_throw<GetFunctionData>(cmd._data.get());
            auto &wiki_db = _dbs.getWikiDB();

            try {
                return create_response(FunctionResponseData(wiki_db->getVersion(), wiki_db->getFunction(data->getLanguage(), data->getName())), CommandCode::GET_FUNCTION);
            } catch (LanguageNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_LANGUAGE, cmd._command_code);
            } catch (FunctionNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_FUNCTION, cmd._command_code);
            }
        }
        case CommandCode::REPORT_CRASH: {
            auto *data = dynamic_cast_throw<ReportCrashData>(cmd._data.get());

            _dbs.getCrashReportDB()->report_crash(data->getStacktrace(), data->getMessage(), std::make_shared<std::string>(data->getSceneName()), data->getSoftware(), data->getOS(), data->getRAM(), data->getUUID(), (_user ? std::make_shared<std::string>(_user->getUsername()) : nullptr));
            return Response(StatusCode::STATUS_OK, CommandCode::REPORT_CRASH);
        }
        default:
            return Response(StatusCode::ERR_UNKNOWN_COMMAND, cmd._command_code);
    }
}

std::string Protocol::handle_command(const std::string &command, std::string &remaining)
{
    std::stringstream stream(command);
    std::string res;
    std::streampos pos;

    while (stream.good()) {
        Command cmd;

        pos = stream.tellg();
        try {
            cmd = Command::fromString(stream);

            if (cmd._data.get() == nullptr) {
                Logger::error() << "PROTOCOL ERROR: Unkown code " << cmd._command_code;
                res += Response(StatusCode::ERR_PROTOCOL_MISSMATCH, 0).toString();
                continue;
            }
            Logger::debug() << "INPUT: ";
            Logger::debug() << cmd.display();
            try {
                auto tmp = process_command(cmd);
                res += tmp.toString();

                Logger::debug() << "OUTPUT: " << std::endl;
                Logger::debug() << tmp.display();
                Logger::log() << "Command ok";
            } catch (std::exception &e) {
                Logger::error() << "INTERNAL SERVER ERROR : " << e.what();
                res += Response(StatusCode::ERR_INTERNAL_SERVER_EROR, cmd._command_code).toString();
            } catch (...) { // TODO check exception type to log and maybe do thing with it ?
                Logger::error() << "INTERNAL SERVER ERROR";
                res += Response(StatusCode::ERR_INTERNAL_SERVER_EROR, cmd._command_code).toString();
            }
        } catch (const std::uint8_t &code) {
            Logger::error() << "PARAMETER ERROR";
            res += Response(StatusCode::ERR_PARAMETER_ERROR, code).toString();
        } catch (const char *str) {
            Logger::error() << "PROTOCOL ERROR" << str;
            res += Response(StatusCode::ERR_PROTOCOL_MISSMATCH, 0).toString();
        } catch (CJProtocolMissingBytes &e) {
            Logger::warn() << "Missing bytes: " << e.getFormattedMessage();
            remaining = command.substr(pos);
            return res;
        } catch (...) {
            Logger::error() << "PROTOCOL ERROR";
            res += Response(StatusCode::ERR_PROTOCOL_MISSMATCH, 0).toString();
        }
        stream.peek();
    }
    return res;
}

std::string FragmentedBlock::getData() const
{
    return data;
}

std::uint16_t FragmentedBlock::getSize() const
{
    return data.size();
}

FragmentedBlock::FragmentedBlock(std::string input) :
    data(std::move(input))
{}

std::vector<FragmentedBlock> FragmentedBlock::getBlocks(const std::string &input)
{
    std::vector<FragmentedBlock> blocks;
    std::stringstream ss(input);
    constexpr std::uint16_t size = std::numeric_limits<std::uint16_t>::max();
    std::array<char, size> buff = {};

    while (ss) {
        ss.read(buff.data(), size);
        if (ss.gcount()) // TODO check if needed
            blocks.emplace_back(std::string(buff.data(), ss.gcount()));
    }
    return blocks;
}

std::string FragmentedBlock::getBlocksString(const std::string &input)
{
    std::vector<FragmentedBlock> blocks = FragmentedBlock::getBlocks(input);
    BitString str;

    if (blocks.size() > std::numeric_limits<std::uint16_t>::max())
        throw "Error"; // TODO
    str.insert_bits<std::uint16_t>(blocks.size());
    for (const auto &block : blocks) {
        str.insert_bits<std::uint16_t>(block.getSize());
        str.insert_bits<std::string>(block.getData());
    }
    return str.extract_bytes<std::string>();
}
