#include "cj_protocol/TMP_file.hpp"
#include "exceptions/ContextException.hpp"
#include "logger/Logger.hpp"
#include "CJ_utils.hpp"

#include <filesystem> // NOTE require c++17

inline static std::ios_base::openmode compute_mode(const std::string &filename, bool del)
{
    std::ios_base::openmode mode = (std::ios::in | std::ios::out | std::ios::binary |
        // Don't add truncate flag if the file exists and we don't delete it,
        // we may want to keep it's content
        ((std::filesystem::exists(filename) && !del) ? /* noop */std::ios::out : std::ios::trunc)
    );
    return mode;
}

TMP_file::TMP_file(const std::string &filename, bool del) :
    std::fstream(filename, compute_mode(filename, del)),
    _filename(filename),
    _del(del)
{
    std::error_code err;

    if (!is_open())
        throw NewContextException("Failed to open the tmp file : " + filename);
    // Only lock the file if we delete it
    if (_del)
        // This ensure that no one will be able to read the file but us since we already opened it
        std::filesystem::permissions(filename, std::filesystem::perms::owner_exec, std::filesystem::perm_options::replace, err);
    if (err)
        throw NewContextException("Failed to set file permissions: " + err.message());
    if (_del && !std::filesystem::remove(filename, err))
        throw NewContextException("Failed to delete the tmp file");
}

TMP_file::~TMP_file()
{
    // If we didn't delete the file and it's still there, delete it
    if (!_del && std::filesystem::exists(_filename)) {
        std::error_code err;

        std::filesystem::remove(_filename, err);
        if (err)
            Logger::error() << "Failed to delete tmp file : " + _filename << std::endl;
    }
    close();
}

const std::string &TMP_file::get_filename() const
{
    return _filename;
}

bool TMP_file::rename(const std::string &name)
{
    // Default perms (0664), we set theses to ensure that we don't move a file
    // with locked permissions (in the current implementation this is useless
    // cause if permissions are locked the file is deleted so we can't move it,
    // but it might be usefull in the future)
    const std::filesystem::perms default_perms =
        std::filesystem::perms::owner_read | std::filesystem::perms::owner_write |
        std::filesystem::perms::group_read | std::filesystem::perms::group_write |
        std::filesystem::perms::others_read;
    std::error_code err;

    std::filesystem::permissions(_filename, default_perms, std::filesystem::perm_options::replace, err);
    if (err)
        return false;
    std::filesystem::rename(_filename, name, err);
    if (err)
        return false;
    // We set del to true, because the original filename doesn't exist anymore, we can consider it deleted
    _del = true;
    return true;
}

void TMP_file::rewind()
{
    seekg(std::ios_base::beg);
}

void TMP_file::keep(bool state)
{
    _del = state;
}

std::string TMP_file::temp_filename()
{
    return std::filesystem::temp_directory_path().string() + "/." + CJ_utils::random_string(16);
}
