#include "cj_protocol/CJProtocolExceptions.hpp"

CJProtocolException::CJProtocolException(const std::string &file, int line, const std::string function, const std::string &msg, std::exception_ptr nested_ex) :
  ContextException(file, line, function, msg, nested_ex)
{}


CJProtocolMismatch::CJProtocolMismatch(const std::string &file, int line, const std::string function, const std::string &msg, std::exception_ptr nested_ex) :
  CJProtocolException(file, line, function, msg, nested_ex)
{}

CJProtocolMissingBytes::CJProtocolMissingBytes(const std::string &file, int line, const std::string function, const std::string &msg, std::exception_ptr nested_ex) :
  CJProtocolException(file, line, function, msg, nested_ex)
{}
