#include "cj_protocol/BitString.hpp"

#include <cstring>

BitString::BitString(const std::string &str, std::uint64_t nb_bits)
{
    insert_bits(str, nb_bits);
}

template<>
std::string BitString::extract_bits<std::string>(std::uint64_t nb)
{
    std::string res;

    if (_nb_bits % 8 == 0 && nb > 8) {
        std::uint64_t count = std::min(_nb_bits / 8, nb / 8);

        res = _buff.substr(0, count);
        _buff = _buff.substr(count);
        _nb_bits -= 8 * count;
        nb -= 8 * count;
    }
    while (nb > 0) {
        auto c = extract_bits<std::uint8_t>(nb > 8 ? 8 : nb);

        res.push_back(c);
        nb -= 8;
    }
    return res;
}

template <>
std::string BitString::extract_bits<std::string>()
{
    return extract_bits<std::string>(_nb_bits);
}

template<>
std::string BitString::extract_bytes<std::string>(std::uint64_t nb)
{
    return extract_bits<std::string>(nb * 8);
}

template <>
std::string BitString::extract_bytes<std::string>()
{
    return extract_bits<std::string>(_nb_bits - (_nb_bits % 8)); // TODO check
}

template <>
void BitString::insert_bytes<std::string>(const std::string &bits, std::uint64_t nb)
{
    insert_bits<std::string>(bits, nb * 8);
}

template <>
void BitString::insert_bytes<std::string>(const std::string &bits)
{
    insert_bits<std::string>(bits, bits.size() * 8);
}

template <>
void BitString::insert_bits<std::string>(const std::string &bits, std::uint64_t nb)
{
    std::uint64_t i = 0;

    if (_nb_bits % 8 == 0 && nb > 8) {
        i = std::min(_nb_bits / 8, nb / 8);

        _buff += bits.substr(0, i);
        _nb_bits += 8 * i;
        nb -= 8 * i;
    }
    for (; nb > 0; i++) {
        std::uint64_t tmp = nb > 8 ? 8 : nb;

        insert_bits((std::uint8_t)bits[i], tmp);
        nb -= tmp;
    }
}

template <>
void BitString::insert_bits<std::string>(const std::string &bits) {
    insert_bits<std::string>(bits, bits.size() * 8);
}

template <>
void BitString::insert_bits<const char *>(const char * const &bits, std::uint64_t nb) {
    std::uint64_t bytes = nb / 8;

    bytes += nb % 8 == 0 ? 0 : 1;
    insert_bits(std::string(bits, bytes), nb);
}

template <>
void BitString::insert_bits<const char *>(const char * const &bits) {
    auto len = std::strlen(bits);

    insert_bits(std::string(bits, len), len * 8);
}

template <>
void BitString::insert_bits<char *>(char * const &bits, std::uint64_t nb) {
    std::uint64_t bytes = nb / 8;

    bytes += nb % 8 == 0 ? 0 : 1;
    insert_bits(std::string(bits, bytes), nb);
}

template <>
void BitString::insert_bits<char *>(char * const &bits) {
    auto len = std::strlen(bits);

    insert_bits(std::string(bits, len), len * 8);
}

template <>
void BitString::insert_bytes<char *>(char * const &bits)
{
    auto len = std::strlen(bits);

    insert_bits(std::string(bits, len), len * 8);
}

template <>
void BitString::insert_bytes<char *>(char * const &bits, std::uint64_t nb)
{
    insert_bits(std::string(bits, nb), nb * 8);
}

template <>
void BitString::insert_bytes<const char *>(const char * const &bits)
{
    auto len = std::strlen(bits);

    insert_bits(std::string(bits, len), len * 8);
}

template <>
void BitString::insert_bytes<const char *>(const char * const &bits, std::uint64_t nb)
{
    insert_bits(std::string(bits, nb), nb * 8);
}
