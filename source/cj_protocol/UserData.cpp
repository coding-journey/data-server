#include "cj_protocol/CJ_Protocol.hpp"
#include "cj_protocol/UserData.hpp"

#include "exceptions/ContextException.hpp"

#include <algorithm>
#include <uuid/uuid.h>
#include <xcrypt.h>

UserData::UserData(std::string email, std::string username, std::string password_hash, std::string token) :
    _email(std::move(email)),
    _username(std::move(username)),
    _password_hash(std::move(password_hash)),
    _token(std::move(token))
{}

std::string UserData::getKey() const
{
    return _token;
}

bool UserData::isValid() const
{
    if (_token.size() != CJ_Protocol::Protocol::token_size)
        return false;
    if (_password_hash.size() == 0 || _username.size() == 0)
        return false;
    if (_username.size() > UserData::max_username)
        return false;
    return true;
}

bool UserData::operator==(const ISerialisableData &other) const
{
    try {
        auto a = dynamic_cast<const UserData &>(other);

        return a == *this;
    } catch (const std::bad_cast &) {
        return false;
    }
}

bool UserData::operator==(const UserData &other) const
{
    return this->_username == other._username &&
        this->_password_hash == other._password_hash &&
        this->_token == other._token;
}

std::string UserData::serialize() const
{
    std::uint8_t size = _email.size();
    std::string res;

    if (!isValid())
      throw NewContextException(getCantSerializeExceptionMessage());
    res += ISerialisableData::hton((std::uint8_t)current_version);
    res += std::string((char *)&size, sizeof(std::uint8_t));
    res += _email;
    size = _username.size();
    res += std::string((char *)&size, sizeof(std::uint8_t));
    res += _username;
    size = _password_hash.size();
    res += std::string((char *)&size, sizeof(std::uint8_t));
    res += _password_hash;
    res += _token;
    return res;
}

const std::uint8_t UserData::current_version = 0;

void UserData::unserialize(std::istream &data)
{
    std::uint8_t version;
    std::uint8_t size = 0;

    if (data.peek() == std::istream::traits_type::eof())
        throw NewContextException(getNotSerializedExceptionMessage());
    version = ISerialisableData::hton((std::uint8_t)data.get());
    if (version != current_version)
        return migrate(version, data);
    size = ISerialisableData::hton((std::uint8_t)data.get());
    _email = extract_chars(data, size);
    size = ISerialisableData::hton((std::uint8_t)data.get());
    _username = extract_chars(data, size);
    size = ISerialisableData::hton((std::uint8_t)data.get());
    _password_hash = extract_chars(data, size);
    _token = extract_chars(data, CJ_Protocol::Protocol::token_size);
}

void UserData::migrate(std::uint8_t version, std::istream &data)
{
    std::uint8_t size = 0;

    Logger::warn() << "[UserData] MIGRATION...";
    switch (version) {
        default:
            size = version;
            _email = extract_chars(data, size);
            size = ISerialisableData::hton((std::uint8_t)data.get());
            _username = extract_chars(data, size);
            size = ISerialisableData::hton((std::uint8_t)data.get());
            _password_hash = extract_chars(data, size);
            _token = extract_chars(data, CJ_Protocol::Protocol::token_size);
//            throw NewContextException(getNotSerializedExceptionMessage());
    }
}

std::string UserData::generateToken()
{
    uuid_t uuid;
    std::vector<char> res;

    res.reserve(CJ_Protocol::Protocol::token_size + 1);
    uuid_generate(uuid);
    uuid_unparse_lower(uuid, res.data());
    return res.data();
}

bool UserData::checkPassword(const std::string &password)
{
    bool lower = false;
    bool upper = false;
    bool digit = false;
    bool other = false;
    int count = 0;

    if (password.size() < min_password || password.size() > max_password)
        return false;
    for (const auto &c : password) {
        if (std::islower(c))
            lower = true;
        else if (std::isupper(c))
            upper = true;
        else if (std::isdigit(c))
            digit = true;
        else
            other = true;
    }
    count = lower + upper + digit + other;
    return count > 2;
}

std::string UserData::hashPassword(const std::string &password)
{
    std::string tmp = generateToken();
    char *salt = crypt_gensalt("$2a$", 12, tmp.c_str(), 16);
    char *hash = crypt(password.c_str(), salt);

    return hash;
}

bool UserData::verifyPassword(const std::string &password, const std::string &hash)
{
    char *test = crypt(password.c_str(), hash.c_str());

    return hash.compare(test) == 0;
}

bool UserData::verifyPassword(const std::string &password)
{
    return verifyPassword(password, _password_hash);
}
