#include "cj_protocol/DatabasesManager.hpp"
#include "cj_protocol/TMP_file.hpp"
#include "logger/Logger.hpp"

#include "ssl/PEM_encrypt.hpp"

DatabasesManager::DatabasesManager(std::string rsa, const std::string &db_dir) :
    _rsa(std::move(rsa)),
    _crash_report_db(std::make_unique<SQLite_database>(db_dir + "/" + crashreport_db_name + ".db")),
    _wiki_db(std::make_unique<WikiDatabase>(db_dir))
{
    _openFun = [this](const std::string &filename, std::ios::openmode mode) -> std::fstream {
        if (mode & std::ios::in) {
            TMP_file decrypted;
            std::fstream encrypted(filename, mode);
            PEM_encrypt crypto(_rsa);

            if (!crypto.decrypt_file(encrypted, decrypted))
                return std::fstream(); // On error return a closed stream
            decrypted.rewind();
            return std::move(decrypted);
        } else {
            return std::move(TMP_file()); // TODO maybe use a uniqueptr to avoid the slicing ?
        }
    };
    _closeFun = [this](const std::string &filename, std::fstream &stream) {
        std::fstream encrypted(filename, std::ios::out | std::ios::trunc | std::ios::binary);
        PEM_encrypt crypto(_rsa);

        stream.seekg(0);
        if (!crypto.encrypt_file(stream, encrypted))
            Logger::error() << "Failed to encrypt file ! " << filename;
        encrypted.close();
        stream.close();
    };

    _users_db = std::make_unique<FileDatabase<UserData>>(db_dir);
    _users_db->setOpenCallback(_openFun);
    _users_db->setCloseCallback(_closeFun);
    _users_db->select_database(user_db_name);

    _projects_db = std::make_unique<FileDatabase<ProjectData>>(db_dir);
    _projects_db->setOpenCallback(_openFun);
    _projects_db->setCloseCallback(_closeFun);
    _projects_db->select_database(project_db_name);

    _dbs = std::make_unique<DatabaseCollection<FileDatabase<FileData>>>(db_dir);
    _dbs->setOpenCallback(_openFun);
    _dbs->setCloseCallback(_closeFun);

    _cj_db = _dbs->createDatabase("CodingJourney");
}

std::unique_ptr<DatabaseCollection<FileDatabase<FileData>>> &DatabasesManager::getDatabases()
{
    return _dbs;
}

std::unique_ptr<FileDatabase<UserData>> &DatabasesManager::getUsersDB()
{
    return _users_db;
}

std::unique_ptr<FileDatabase<ProjectData>> &DatabasesManager::getProjectsDB()
{
    return _projects_db;
}

std::unique_ptr<SQLite_database> &DatabasesManager::getCrashReportDB()
{
    return _crash_report_db;
}

std::unique_ptr<WikiDatabase> &DatabasesManager::getWikiDB()
{
    return _wiki_db;
}
