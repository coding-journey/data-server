#include "cj_protocol/4.1.1/Response.hpp"
#include "cj_protocol/4.1.1/CJ_Protocol.hpp"

#include <sstream>

using namespace CJ_Protocol_4_1_1;

// TODO use BitString here ?

Response::Response(std::uint8_t status, std::uint8_t command, std::uint16_t size, std::string data) :
    _status_code(status),
    _command_code(command),
    _data_size(size),
    _data(std::move(data))
{}

Response::Response(std::uint8_t status, std::uint8_t command) :
    _status_code(status),
    _command_code(command),
    _data_size(0),
    _data("")
{}

std::string Response::toString() const
{
    std::uint8_t status = _status_code;
    std::uint8_t code = _command_code;
    std::uint16_t size = _data_size;
    std::string res((char *)&status, sizeof(std::uint8_t));

    code = (code << 3) + (size >> 8);
    res += std::string((char *)&code, sizeof(std::uint8_t));
    code = (size << 8) >> 8;
    res += std::string(((char *)&code), sizeof(std::uint8_t));
    res += _data;
    return res;
}

std::string Response::display() const
{
    std::stringstream ss;

    ss << "CommandeCode: " << std::to_string(_command_code) << std::endl;
    ss << "Status: " << std::to_string(_status_code) << std::endl;

    return ss.str();
}

bool Response::operator==(const Response &o)
{
    return _status_code == o._status_code &&
        _command_code == o._command_code &&
        _data_size == o._data_size &&
        _data == o._data;
}
