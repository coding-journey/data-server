#include "cj_protocol/4.1.1/CJ_Protocol.hpp"
#include "cj_protocol/4.1.1/ResponseData.hpp"
#include "cj_protocol/BitString.hpp"
#include "cj_protocol/CustomFileData.hpp"

#include <openssl/md5.h>
static_assert(MD5_DIGEST_LENGTH == 16, "MD5_DIGEST is not 16 octets");

using namespace CJ_Protocol_4_1_1;

std::uint16_t IResponseData::getSize() const
{
    return _fragmented ? Protocol::fragmented_size : _data.size();
}

std::string IResponseData::toString() const
{
    return _data;
}

void IResponseData::setData(const std::string &data)
{
    _fragmented = data.size() >= Protocol::fragmented_size;
    if (_fragmented)
        _data = FragmentedBlock::getBlocksString(data);
    else
        _data = data;
}

VersionResponseData::VersionResponseData(std::uint16_t version)
{
    BitString str;

    str.insert_bits(version);
    setData(str.extract_bits<std::string>());
}

VersionResponseData::VersionResponseData(std::uint8_t major, std::uint8_t minor, std::uint8_t patch)
{
    BitString str;

    str.insert_bits<std::uint8_t>(major, 3);
    str.insert_bits<std::uint8_t>(minor, 5);
    str.insert_bits<std::uint8_t>(patch, 8);
    setData(str.extract_bits<std::string>());
}

std::string VersionResponseData::display() const
{
    return "VersionResponseData";
}

DeleteAccountResponseData::DeleteAccountResponseData(const std::string &token)
{
    BitString str;

    if (token.size() > std::numeric_limits<std::uint8_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint8_t>(token.size());
    str.insert_bits(token);
    setData(str.extract_bits<std::string>());
}

std::string DeleteAccountResponseData::display() const
{
    return "DeleteAccountResponseData";
}

LoginResponseData::LoginResponseData(const UserData &user) :
    LoginResponseData(user.getToken(), user.getUsername(), user.getEmail())
{}

LoginResponseData::LoginResponseData(const std::string &token, const std::string &username, const std::string &email)
{
    BitString str;

    if (token.size() != Protocol::token_size)
        throw "Err"; // TODO
    if (username.size() > std::numeric_limits<std::uint8_t>::max())
        throw "Err"; // TODO
    if (email.size() > std::numeric_limits<std::uint8_t>::max())
        throw "Err"; // TODO
    str.insert_bits(token);
    str.insert_bits<std::uint8_t>(username.size());
    str.insert_bits(username);
    str.insert_bits<std::uint8_t>(email.size());
    str.insert_bits(email);
    setData(str.extract_bits<std::string>());
}

std::string LoginResponseData::display() const
{
    return "LoginResponseData";
}

ListFilesResponseData::ListFilesResponseData(const std::vector<std::shared_ptr<const FileData>> &files)
{
    BitString str;

    if (files.size() > std::numeric_limits<std::uint16_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint16_t>(files.size());
    for (const auto &iter : files) {
        std::string filename = iter->getKey();
        std::string content = iter->getContent();
        unsigned char *buff = MD5((const unsigned char *)content.c_str(), content.size(), nullptr);
        const CustomFileData &data = iter->getCustomData<CustomFileData>();

        if (filename.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(filename.size());
        str.insert_bits(filename);
        str.insert_bits<std::uint64_t>(iter->getTimestamp());
        str.insert_bits<std::uint8_t>(data.isPublic(), 1);
        str.insert_bits<std::uint8_t>(data.isProject(), 1);
        str.insert_bits<std::uint8_t>(0, 6); // Unused
        str.insert_bytes((const char *)buff, MD5_DIGEST_LENGTH);
    }
    setData(str.extract_bits<std::string>());
}

std::string ListFilesResponseData::display() const
{
    return "ListFilesResponseData";
}

ListProjectsResponseData::ListProjectsResponseData(const std::vector<std::shared_ptr<const ProjectData>> &files)
{
    BitString str;

    if (files.size() > std::numeric_limits<std::uint64_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint64_t>(files.size());
    for (const auto &iter : files) {
        std::string project_name = iter->getProjectName();
        std::string owner = iter->getOwner();
        std::string meta_filename = iter->getMetaFilename();
        std::string filename = iter->getFilename();

        if (project_name.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        if (owner.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        if (meta_filename.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        if (filename.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(project_name.size());
        str.insert_bits(project_name);
        str.insert_bits<std::uint8_t>(owner.size());
        str.insert_bits(owner);
        str.insert_bits<std::uint8_t>(meta_filename.size());
        str.insert_bits(meta_filename);
        str.insert_bits<std::uint8_t>(filename.size());
        str.insert_bits(filename);
        str.insert_bits<std::uint64_t>(iter->getTimestamp());
        str.insert_bits<std::uint8_t>(iter->isApproved(), 1);
        str.insert_bits<std::uint8_t>(0, 7); // UNUSED
    }
    setData(str.extract_bits<std::string>());
}

std::string ListProjectsResponseData::display() const
{
    return "ListProjectResponseData";
}

DownloadResponseData::DownloadResponseData(const std::shared_ptr<const FileData> &file)
{
    BitString str;
    std::string filename = file->getKey();
    std::string content = file->getContent();
    auto data = file->getCustomData<CustomFileData>();

    if (filename.size() > std::numeric_limits<std::uint8_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint8_t>(filename.size());
    str.insert_bits(filename);
    str.insert_bits<std::uint64_t>(file->getTimestamp());
    str.insert_bits<std::uint8_t>(data.isPublic(), 1);
    str.insert_bits<std::uint8_t>(data.isProject(), 1);
    str.insert_bits<std::uint8_t>(0, 3); // Unused
    if (content.size() >= Protocol::fragmented_size) {
        str.insert_bits<std::uint16_t>(Protocol::fragmented_size, 11);
        str.insert_bits<std::string>(FragmentedBlock::getBlocksString(content));
    } else {
        str.insert_bits<std::uint16_t>(content.size(), 11);
        str.insert_bits(content);
    }
    setData(str.extract_bits<std::string>());
}

std::string DownloadResponseData::display() const
{
    return "DownloadResponseData";
}

ListFileResponseData::ListFileResponseData(const std::shared_ptr<FileData> &file)
{
    BitString str;
    auto data = file->getCustomData<CustomFileData>();
    auto filename = file->getFilename();
    auto content = file->getContent();
    unsigned char *buff = MD5((const unsigned char *)content.c_str(), content.size(), nullptr);

    if (filename.size() > std::numeric_limits<std::uint8_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint8_t>(filename.size());
    str.insert_bits(filename);
    str.insert_bits<std::uint64_t>(file->getTimestamp());
    str.insert_bits<std::uint8_t>(data.isPublic(), 1);
    str.insert_bits<std::uint8_t>(data.isProject(), 1);
    str.insert_bits<std::uint8_t>(0, 6); // UNUSED
    str.insert_bytes((const char *)buff, MD5_DIGEST_LENGTH);
    setData(str.extract_bits<std::string>());
}

std::string ListFileResponseData::display() const
{
    return "ListFileResponseData";
}

ListUsersResponseData::ListUsersResponseData(const std::vector<std::shared_ptr<UserData>> &data)
{
    BitString str;

    if (data.size() > std::numeric_limits<std::uint32_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint32_t>(data.size());
    for (const auto &iter : data) {
        std::string pseudo = iter->getUsername();

        if (pseudo.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(pseudo.size());
        str.insert_bits(pseudo);
    }
    setData(str.extract_bits<std::string>());
}

std::string ListUsersResponseData::display() const
{
    return "ListUsersResponseData";
}

ListResponseData<WikiDatabase::languages_t>::ListResponseData(std::uint16_t version, const WikiDatabase::languages_t &values)
{
    BitString str;

    str.insert_bits<std::uint16_t>(version);
    str.insert_bits<std::uint8_t>(values.size());
    for (const auto &iter : values) {
        if (iter.first.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(iter.first.size());
        str.insert_bits(iter.first);
    }
    setData(str.extract_bits<std::string>());
}

std::string ListResponseData<WikiDatabase::languages_t>::display() const
{
    return "ListResponseData<WikiDatabase::languages_t>";
}

ListResponseData<WikiDatabase::categories_t>::ListResponseData(std::uint16_t version, const WikiDatabase::categories_t &values)
{
    BitString str;

    str.insert_bits<std::uint16_t>(version);
    str.insert_bits<std::uint8_t>(values.size());
    for (const auto &iter : values) {
        if (iter.first.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(iter.first.size());
        str.insert_bits(iter.first);
    }
    setData(str.extract_bits<std::string>());
}

std::string ListResponseData<WikiDatabase::categories_t>::display() const
{
    return "ListResponseData<WikiDatabase::categories_t>";
}

ListResponseData<WikiDatabase::functions_t>::ListResponseData(std::uint16_t version, const WikiDatabase::functions_t &values)
{
    BitString str;

    str.insert_bits<std::uint16_t>(version);
    str.insert_bits<std::uint8_t>(values.size());
    for (const auto &iter : values) {
        if (iter.first.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(iter.first.size());
        str.insert_bits(iter.first);
    }
    setData(str.extract_bits<std::string>());
}

std::string ListResponseData<WikiDatabase::functions_t>::display() const
{
    return "ListResponseData<WikiDatabase::functions_t>";
}

FunctionsResponseData::FunctionsResponseData(std::uint16_t version, const WikiDatabase::functions_t &values)
{
    BitString str;

    str.insert_bits<std::uint16_t>(version);
    str.insert_bits<std::uint8_t>(values.size());
    for (const auto &iter : values) {
        if (iter.second.size() > std::numeric_limits<std::uint16_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint16_t>(iter.second.size());
        str.insert_bits(iter.second);
    }
    setData(str.extract_bits<std::string>());
}

std::string FunctionsResponseData::display() const
{
    return "FunctionsResponseData";
}

FunctionResponseData::FunctionResponseData(std::uint16_t version, const std::string &fun)
{
    BitString str;

    str.insert_bits<std::uint16_t>(version);
    if (fun.size() > std::numeric_limits<std::uint16_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint16_t>(fun.size());
    str.insert_bits(fun);
    setData(str.extract_bits<std::string>());
}


std::string FunctionResponseData::display() const
{
    return "FunctionResponseData";
}
