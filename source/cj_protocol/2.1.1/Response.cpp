#include "cj_protocol/2.1.1/CJ_Protocol.hpp"
#include "cj_protocol/2.1.1/Response.hpp"

#include <sstream>

using namespace CJ_Protocol_2_1_1;

// TODO use BitString here ?

Response::Response(std::uint8_t status, std::uint8_t command, std::uint16_t size, const std::string &data, Response *nested) :
    _status_code(status),
    _command_code(command),
    _data_size(size),
    _data(data),
    _nested(nested)
{}

Response::Response(std::uint8_t status, std::uint8_t command, Response *nested) :
    _status_code(status),
    _command_code(command),
    _data_size(0),
    _data(""),
    _nested(nested)
{}

std::string Response::toString() const
{
    std::uint8_t status = _status_code;
    std::uint8_t code = _command_code;
    std::uint16_t size = _data_size;
    std::string res((char *)&status, sizeof(std::uint8_t));

    if (_data.size() > size) // Mean either we have more than 2047 data or something else went wrong
        throw "Err"; // TODO
    code = (code << 3) + (size >> 8);
    res += std::string((char *)&code, sizeof(std::uint8_t));
    code = (size << 8) >> 8;
    res += std::string(((char *)&code), sizeof(std::uint8_t));
    res += _data;
    if (_nested)
        res += _nested->toString();
    return res;
}

Response Response::fromString(std::stringstream &stream)
{
    unsigned char buff[3];
    std::uint16_t size;
    std::uint8_t code;
    std::uint8_t status;
    std::string data;

    stream.read((char *)buff, sizeof(buff));
    if (stream.gcount() != sizeof(buff))
        throw "Error"; // TODO
    status = buff[0];
    code = buff[1] >> 3;
    size = (std::uint8_t)(buff[1] << 5);
    size = (size << 3) + buff[2];
    if (size) {
        std::uint16_t _size = (size == Protocol::fragmented_size) ? 2 : size;
        char tmp[_size];

        stream.read(tmp, _size);
        if (stream.gcount() != _size)
            throw "Error"; // TODO
        data = std::string(tmp, _size);
    }
    return Response(status, code, size, data);
}

bool Response::operator==(const Response &o)
{
    return _status_code == o._status_code &&
        _command_code == o._command_code &&
        _data_size == o._data_size &&
        _data == o._data;
}
