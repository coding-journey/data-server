#include "cj_protocol/2.1.1/CommandData.hpp"
#include "cj_protocol/2.1.1/CJ_Protocol.hpp"
#include "cj_protocol/BitString.hpp"

#include <bits/stdc++.h>

using namespace CJ_Protocol_2_1_1;

bool ICommandData::isFragmented() const
{
    return _fragmented;
}

// TODO make a ILoginCommandData or whatever, and inherit from this class when a class need login
bool ICommandData::needLogin() const
{
    return _needLogin;
}

std::uint16_t ICommandData::getSize() const
{
    return _size;
}

// TODO why not remove data from here since it's unused?
ICommandData::ICommandData(std::uint16_t size, const std::string &data, bool unfragmented) :
    _size(size),
    _fragmented(false),
    _needLogin(true)
{
    if (unfragmented)
        return;
    if (_size == Protocol::fragmented_size) {
        BitString str(data);

        _fragmented = true;
        _size = str.extract_bits<std::uint16_t>();
    }
}

RequestVersionData::RequestVersionData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    _needLogin = false;
    if (!_fragmented) {
        BitString str(data);

        _version = str.extract_bits<std::uint16_t>();
        str.insert_bits(data, 16); // Reload to extract for each subversion
        _major = str.extract_bits<std::uint8_t>(3);
        _minor = str.extract_bits<std::uint8_t>(5);
        _patch = str.extract_bits<std::uint8_t>();
    }
}

std::uint16_t RequestVersionData::getVersion() const
{
    return _version;
}

std::uint8_t RequestVersionData::getMajor() const
{
    return _major;
}

std::uint8_t RequestVersionData::getMinor() const
{
    return _minor;
}

std::uint8_t RequestVersionData::getPatch() const
{
    return _patch;
}

LoginData::LoginData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    _needLogin = false;
    if (!_fragmented) {
        BitString str(data);
        std::string id = str.extract_bits<std::string>(Protocol::steam_id_size * 8);
        auto size = str.extract_bits<std::uint8_t>();

        std::strncpy(_steamid.data(), id.c_str(), Protocol::steam_id_size);
        _pseudo = str.extract_bits<std::string>(size * 8);
    }
}

const char *LoginData::getSteamID() const
{
    return _steamid.data();
}

std::string LoginData::getPseudo() const
{
    return _pseudo;
}

DownloadData::DownloadData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    if (!_fragmented) {
        BitString str(data);
        auto size = str.extract_bits<std::uint8_t>();

        _filename = str.extract_bits<std::string>(size * 8);
    }
}

std::string DownloadData::getFilename() const
{
    return _filename;
}

UploadData::UploadData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    if (!_fragmented) {
        BitString str(data);
        std::uint16_t size = str.extract_bits<std::uint8_t>();

        _filename = str.extract_bits<std::string>(size * 8);
        _timestamp = str.extract_bits<std::uint64_t>();
        _public = str.extract_bits<std::uint8_t>(1);
        str.extract_bits<std::uint8_t>(4); // UNUSED bits
        size = str.extract_bits<std::uint16_t>(11);
        if (size != Protocol::fragmented_size) {
            _content = str.extract_bits<std::string>(size * 8);
            _fragmentedContent = false;
        } else {
            _size = str.extract_bits<std::uint16_t>();
            _fragmentedContent = true;
        }
    }
}

void UploadData::unfragment(const std::string &data)
{
    _fragmentedContent = false;
    _content = data;
}

bool UploadData::hasFragmentedContent() const
{
    return _fragmentedContent;
}

std::string UploadData::getFilename() const
{
    return _filename;
}

std::uint64_t UploadData::getTimestamp() const
{
    return _timestamp;
}

std::string UploadData::getContent() const
{
    return _content;
}

bool UploadData::isPublic() const
{
    return _public;
}

FragmentedData::FragmentedData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    if (!_fragmented) {
        BitString str(data);
        std::uint16_t size;

        _blockID = str.extract_bits<std::uint16_t>();
        _mode = str.extract_bits<std::uint8_t>(1);
        str.extract_bits<std::uint8_t>(_mode ? 7 : 4); // UNUSED bits
        if (!_mode) {
            size = str.extract_bits<std::uint16_t>(11);
            _data = str.extract_bits<std::string>(size * 8);
        }
    }
}

bool FragmentedData::isModeGet() const
{
    return _mode;
}

std::uint16_t FragmentedData::getBlockID() const
{
    return _blockID;
}

std::string FragmentedData::getData() const
{
    return _data;
}

NoData::NoData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    _needLogin = false;
}

NoDataLogin::NoDataLogin(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{}

GetUserIdData::GetUserIdData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    if (!_fragmented) {
        BitString str(data);
        auto pseudo_size = str.extract_bits<std::uint8_t>();

        _pseudo = str.extract_bits<std::string>(pseudo_size * 8);
    }
    _needLogin = false;
}

std::string GetUserIdData::getPseudo() const
{
    return _pseudo;
}

ListPublicFilesData::ListPublicFilesData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    if (!_fragmented) {
        BitString str(data);
        std::string id = str.extract_bits<std::string>(Protocol::steam_id_size * 8);

        std::strncpy(_steamid.data(), id.c_str(), Protocol::steam_id_size);
    }
    _needLogin = false;
}

const std::string ListPublicFilesData::getSteamID() const
{
    return std::string(_steamid.data(), _steamid.size());
}

DownloadPublicFileData::DownloadPublicFileData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    if (!_fragmented) {
        BitString str(data);
        std::string id = str.extract_bits<std::string>(Protocol::steam_id_size * 8);
        auto name_size = str.extract_bits<std::uint8_t>();

        _filename = str.extract_bits<std::string>(name_size * 8);
        std::strncpy(_steamid.data(), id.c_str(), Protocol::steam_id_size);
    }
    _needLogin = false;
}

const std::string DownloadPublicFileData::getSteamID() const
{
    return std::string(_steamid.data(), _steamid.size());
}

const std::string DownloadPublicFileData::getFilename() const
{
    return _filename;
}


GetCategoriesData::GetCategoriesData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    _needLogin = false;
    if (!_fragmented) {
        BitString str(data);
        std::uint8_t len = str.extract_bits<std::uint8_t>();

        _lang = str.extract_bits<std::string>(len * 8);
    }
}

const std::string GetCategoriesData::getLanguage() const
{
    return _lang;
}

GetFunctionsData::GetFunctionsData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    _needLogin = false;
    if (!_fragmented) {
        BitString str(data);
        std::uint8_t len = str.extract_bits<std::uint8_t>();

        _lang = str.extract_bits<std::string>(len * 8);
        len = str.extract_bits<std::uint8_t>();
        _categ = str.extract_bits<std::string>(len * 8);
    }
}

const std::string GetFunctionsData::getLanguage() const
{
    return _lang;
}

const std::string GetFunctionsData::getCategory() const
{
    return _categ;
}

GetFunctionData::GetFunctionData(std::uint16_t size, const std::string &data, bool unfragmented):
    ICommandData(size, data, unfragmented)
{
    _needLogin = false;
    if (!_fragmented) {
        BitString str(data);
        std::uint8_t len = str.extract_bits<std::uint8_t>();

        _lang = str.extract_bits<std::string>(len * 8);
        len = str.extract_bits<std::uint8_t>();
        _name = str.extract_bits<std::string>(len * 8);
    }
}

const std::string GetFunctionData::getLanguage() const
{
    return _lang;
}

const std::string GetFunctionData::getName() const
{
    return _name;
}

ReportBugData::ReportBugData(std::uint16_t size, const std::string &data, bool unfragmented) :
    ICommandData(size, data, unfragmented)
{
    _needLogin = false;
    if (!_fragmented) {
        BitString str(data);
        bool soft;
        std::uint8_t os;
        float tmp;

        _stacktrace = str.extract_bits<std::string>(str.extract_bits<std::uint16_t>() * 8);
        _message = str.extract_bits<std::string>(str.extract_bits<std::uint16_t>() * 8);
        soft = str.extract_bits<std::uint8_t>(1);
        str.extract_bits<std::uint8_t>(5); // UNUSED
        os = str.extract_bits<std::uint8_t>(2);
        _ram = str.extract_bits<std::uint8_t>();
        tmp = str.extract_bits<std::uint8_t>();
        while (tmp > 1)
            tmp = tmp / 10;
        _ram += tmp;
        _soft = (soft ? SoftwareType::Editor : SoftwareType::Game);
        switch (os) {
            case 1:
                _os = OperatingSystem::Linux;
                break;
            case 2:
                _os = OperatingSystem::Windows;
                break;
            case 3:
                _os = OperatingSystem::Mac;
                break;
            default:
                _os = OperatingSystem::Other;
        }
        _uuid = str.extract_bits<std::string>(36 * 8);
    }
}

const std::string ReportBugData::getStacktrace() const
{
    return _stacktrace;
}

const std::string ReportBugData::getMessage() const
{
    return _message;
}

SoftwareType ReportBugData::getSoftware() const
{
    return _soft;
}

OperatingSystem ReportBugData::getOS() const
{
    return _os;
}

float ReportBugData::getRAM() const
{
    return _ram;
}

const std::string ReportBugData::getUUID() const
{
    return _uuid;
}
