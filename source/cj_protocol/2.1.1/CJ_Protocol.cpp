#include <algorithm>

#include "cj_protocol/2.1.1/CJ_Protocol.hpp"
#include "cj_protocol/BitString.hpp"
#include "cj_protocol/CJ_ProtocolManager.hpp"
#include "cj_protocol/CustomFileData.hpp"
#include "cj_protocol/TMP_file.hpp"

#include "database/ISerialisableData.hpp"

#include "logger/Logger.hpp"

using namespace CJ_Protocol_2_1_1;


const std::uint8_t Protocol::major_version = 1; // 0-7
const std::uint8_t Protocol::minor_version = 0; // 0-31
const std::uint8_t Protocol::patch_version = 0; // 0-255

const std::uint8_t Protocol::steam_id_size = 17;
const std::uint16_t Protocol::fragmented_size = 2047;

// TODO Remove this fonction and use the VersionResponseData class
static std::uint16_t convertVersion(std::uint8_t major, std::uint8_t minor, std::uint8_t patch)
{
    BitString str;

    str.insert_bits<std::uint8_t>(major, 3);
    str.insert_bits<std::uint8_t>(minor, 5);
    str.insert_bits<std::uint8_t>(patch, 8);
    return str.extract_bits<std::uint16_t>();
}

// TODO test units on this class (possible so do it)
Protocol::Protocol(CJ_ProtocolManager *m) :
    _version(convertVersion(major_version, minor_version, patch_version)),
    _m(m),
    _dbs(_m->getDBs())
{
    Logger::log() << "Using CJ Protocol Version " <<
        major_version + 1 << "." << minor_version + 1 << "." << patch_version + 1;
}

std::uint64_t Protocol::getVersion() const
{
    return _version;
}

template<typename T, class A>
static T *dynamic_cast_throw(A *ptr)
{
    T *res = dynamic_cast<T *>(ptr); // TODO replace with std::dynamic_pointer_cast

    if (res)
        return res;
    throw "Err"; // TODO
}

bool Protocol::set_fragmented_get(std::string data)
{
    std::unordered_map<std::uint16_t /*id*/, std::string /*content*/> blocks;
    std::uint16_t i = 1; // id 0 is reserved
    const std::uint16_t block_size = Protocol::fragmented_size - 10; // Min -5 to fit the rest of the response, can be less to be sure
    std::size_t size = data.size();

    if (_get.active)
        return false;
    _get.active = true;
    Logger::log() << "FRAGMENTED MODE GET ON";
    while (size) {
        size = block_size > size ? size : block_size;
        blocks[i] = data.substr(0, size);
        data = data.substr(size);
        size = data.size();
        i++;
    }
    if (blocks.size() > std::numeric_limits<std::uint16_t>::max())
        throw "Err"; // TODO
    _get.total = blocks.size();
    _get.blocks = blocks;
    return true;
}

bool Protocol::set_fragmented_put(Command &cmd)
{
    if (_put.active)
        return false;
    Logger::log() << "FRAGMENTED MODE PUT ON";
    _put.active = true;
    _put.blocks.clear();
    _put.total = cmd._data->getSize();
    _put.cmd = std::move(cmd);
    return true;
}

Response Protocol::create_response(const IResponseData &data, CommandCode code, StatusCode status)
{
    if (data.isFragmented()) {
        BitString buff;

        if (!set_fragmented_get(data.toString()))
            return Response(StatusCode::ERR_ALREADY_FRAGMENTED_MODE, code);
        buff.insert_bits<std::uint16_t>(_get.total);
        return Response(status, code, Protocol::fragmented_size, buff.extract_bits<std::string>());
    }
    return Response(status, code, data.getSize(), data.toString());
}

Response Protocol::exec_partial_fragmented(Command &cmd, const std::string &udata)
{
    switch (cmd._command_code) {
        case CommandCode::UPLOAD_FILE: {
            auto data = dynamic_cast_throw<UploadData>(cmd._data.get());

            data->unfragment(udata);
            return process_command(cmd);
        }
        default:
            return Response(StatusCode::ERR_PARAMETER_ERROR, cmd._command_code);
    }
}

// TODO split in sub functions
Response Protocol::process_command(Command &cmd)
{
    if (cmd._data == nullptr)
        return Response(StatusCode::ERR_UNKNOWN_COMMAND, cmd._command_code);
    if (_user.size() == 0 && cmd._data->needLogin())
        return Response(StatusCode::ERR_MISSING_LOGIN, cmd._command_code);
    if (cmd._data->isFragmented()) {
        if (!set_fragmented_put(cmd))
            return Response(StatusCode::ERR_ALREADY_FRAGMENTED_MODE, cmd._command_code);
        return Response(StatusCode::STATUS_OK, cmd._command_code);
    }
    switch (cmd._command_code) {
        case CommandCode::VERSION:
            return create_response(
                VersionResponseData(Protocol::major_version, Protocol::minor_version, Protocol::patch_version),
                CommandCode::VERSION);
        case CommandCode::REQUEST_SERVER_VERSION: {
            auto data = dynamic_cast_throw<RequestVersionData>(cmd._data.get());

            try {
                std::uint16_t version = _m->change_version(data->getMajor(), data->getMinor(), data->getPatch());

                return create_response(VersionResponseData(version), CommandCode::REQUEST_SERVER_VERSION);
            } catch (...) {
                return Response(StatusCode::ERR_VERSION, CommandCode::REQUEST_SERVER_VERSION);
            }
        }
        case CommandCode::LOGIN: {
            auto data = dynamic_cast_throw<LoginData>(cmd._data.get());

            if (_user.size() != 0)
                return Response(StatusCode::ERR_ALREADY_LOGIN, CommandCode::LOGIN);
            _user = std::string(data->getSteamID(), Protocol::steam_id_size);
            // _dbs.getUserDB()->insert(std::make_shared<PseudoData>(data->getPseudo(), _user));
            _db = _dbs.getDatabases()->createDatabase(_user);
            return Response(StatusCode::STATUS_OK, CommandCode::LOGIN);
        }
        case CommandCode::LIST_FILES: {
            auto files = _db->list_files();
            std::vector<std::shared_ptr<const FileData>> vec;

            vec.reserve(files.size());
            std::transform(files.begin(), files.end(), std::back_inserter(vec), [](const auto &p){return p.second;});
            return create_response(ListFilesResponseData(vec), CommandCode::LIST_FILES);
        }
        case CommandCode::DOWNLOAD_FILE: {
            auto data = dynamic_cast_throw<DownloadData>(cmd._data.get());
            auto file = _db->querry_file(data->getFilename());

            if (!file)
                return Response(StatusCode::ERR_NO_SUCH_FILE, CommandCode::DOWNLOAD_FILE);
            const std::string &content = file->getContent();

            if (content.size() >= Protocol::fragmented_size) {
                if (!set_fragmented_get(content))
                    return Response(StatusCode::ERR_ALREADY_FRAGMENTED_MODE, CommandCode::DOWNLOAD_FILE);
                return create_response(DownloadResponseData(file, _get.total), CommandCode::DOWNLOAD_FILE);
            }
            return create_response(DownloadResponseData(file), CommandCode::DOWNLOAD_FILE);
        }
        case CommandCode::UPLOAD_FILE: {
            auto data = dynamic_cast_throw<UploadData>(cmd._data.get());
            std::shared_ptr<FileData> file = std::make_shared<FileData>(
                data->getFilename(), data->getTimestamp(), data->getContent(), CustomFileData(data->isPublic(), false)
            );

            if (_db->size() > std::numeric_limits<std::uint16_t>::max())
                return Response(StatusCode::ERR_TOO_MANY_FILES, CommandCode::UPLOAD_FILE);
            if (data->hasFragmentedContent()) {
                if (!set_fragmented_put(cmd))
                    return Response(StatusCode::ERR_ALREADY_FRAGMENTED_MODE, CommandCode::UPLOAD_FILE);
                return Response(StatusCode::STATUS_OK, CommandCode::UPLOAD_FILE);
            }

            _db->insert(file);
            return Response(StatusCode::STATUS_OK, CommandCode::UPLOAD_FILE);
        }
        case CommandCode::FRAGMENTED_DATA: {
            auto data = dynamic_cast_throw<FragmentedData>(cmd._data.get());
            std::uint16_t id = data->getBlockID();

            if ((data->isModeGet() && !_get.active) || (!data->isModeGet() && !_put.active))
                return Response(StatusCode::ERR_NOT_FRAGMENTED_MODE, CommandCode::FRAGMENTED_DATA);
            if (data->isModeGet()) {
                if (id > _get.total)
                    return Response(StatusCode::ERR_UNKNOWN_BLOCK, CommandCode::FRAGMENTED_DATA);
                if (id == 0) {
                    Logger::log() << "FRAGMENTED MODE GET OFF";
                    _get.active = false;
                    _get.blocks.clear();
                    return Response(StatusCode::STATUS_OK, CommandCode::FRAGMENTED_DATA);
                }
                return create_response(FragmentedResponseData(id, _get.blocks[id]), CommandCode::FRAGMENTED_DATA);
            } else {
                if (id > _put.total)
                    return Response(StatusCode::ERR_UNKNOWN_BLOCK, CommandCode::FRAGMENTED_DATA);
                if (id == 0) {
                    Logger::log() << "FRAGMENTED MODE PUT OFF";
                    _put.active = false;
                    if (_put.blocks.size() != _put.total) {
                        std::string res = Response(StatusCode::ERR_MISSING_BLOCKS, _put.cmd._command_code).toString();

                        return Response(StatusCode::STATUS_OK, CommandCode::FRAGMENTED_DATA, res.size(), res);
                    }
                    std::string data;

                    for (auto &iter : _put.blocks) {
                        data += iter.second;
                    }
                    std::string res; // Will contain the result of the fragmented command

                    if (!_put.cmd._data->isFragmented()) { // If the command is not fragmented, then it's a parial frag
                        res = exec_partial_fragmented(_put.cmd, data).toString();
                    } else {
                        _put.cmd._rawData = data;
                        res = handle_command(_put.cmd.toString(), true);
                    }
                    std::stringstream ss(res);
                    // We return 2 responses in once
                    return Response(StatusCode::STATUS_OK, CommandCode::FRAGMENTED_DATA, new Response(Response::fromString(ss)));
                }
                _put.blocks[id] = data->getData();
                return Response(StatusCode::STATUS_OK, CommandCode::FRAGMENTED_DATA);
            }
        }
        case CommandCode::LIST_USERS: {
            auto users = _dbs.getUsersDB()->list_files();
            std::vector<std::shared_ptr<PseudoData>> vec;

            vec.reserve(users.size());
            std::transform(users.begin(), users.end(), std::back_inserter(vec), [](const auto &p){return std::make_shared<PseudoData>(p.second->getUsername(), "00000000000000000");});
            return create_response(ListUsersResponseData(vec), CommandCode::LIST_USERS);
        }
        case CommandCode::GET_USER_ID: {
            auto data = dynamic_cast_throw<GetUserIdData>(cmd._data.get());
            auto user = _dbs.getUsersDB()->querry_file([=](const std::shared_ptr<ISerialisableData> &item) -> bool {
                auto tmp = std::dynamic_pointer_cast<UserData>(item);

                if (!tmp)
                    return false;
                return data->getPseudo() == tmp->getUsername();
            });

            if (!user)
                return Response(StatusCode::ERR_UNKNOWN_PSEUDO, CommandCode::GET_USER_ID);
            return create_response(GetUserIdResponseData("00000000000000000"), CommandCode::GET_USER_ID);
        }
        case CommandCode::LIST_PUBLIC_FILES: {
            auto data = dynamic_cast_throw<ListPublicFilesData>(cmd._data.get());
            std::unordered_map<std::string, std::shared_ptr<FileData>> files = _dbs.getDatabases()->getDatabase(data->getSteamID())->list_files();
            std::vector<std::shared_ptr<const FileData>> vec;

            for (auto iter = files.begin(); iter != files.end();) {
                if (!iter->second->template getCustomData<CustomFileData>().isPublic())
                    iter = files.erase(iter);
                else
                    iter++;
            }
            vec.reserve(files.size());
            std::transform(files.begin(), files.end(), std::back_inserter(vec), [](const auto &p){return p.second;});
            return create_response(ListFilesResponseData(vec), CommandCode::LIST_PUBLIC_FILES);
        }
        case CommandCode::DOWNLOAD_PUBLIC_FILE: {
            auto data = dynamic_cast_throw<DownloadPublicFileData>(cmd._data.get());
            auto file = _dbs.getDatabases()->getDatabase(data->getSteamID())->querry_file(data->getFilename());

            if (!file || !file->template getCustomData<CustomFileData>().isPublic())
                return Response(StatusCode::ERR_NO_SUCH_FILE, CommandCode::DOWNLOAD_PUBLIC_FILE);
            const std::string &content = file->getContent();

            if (content.size() >= Protocol::fragmented_size) {
                if (!set_fragmented_get(content))
                    return Response(StatusCode::ERR_ALREADY_FRAGMENTED_MODE, CommandCode::DOWNLOAD_PUBLIC_FILE);
                return create_response(DownloadResponseData(file, _get.total), CommandCode::DOWNLOAD_PUBLIC_FILE);
            }
            return create_response(DownloadResponseData(file), CommandCode::DOWNLOAD_PUBLIC_FILE);
        }
        case CommandCode::GET_LANGUAGES: {
            auto &wiki_db = _dbs.getWikiDB();

            return create_response(ListResponseData<WikiDatabase::languages_t>(wiki_db->getVersion(), wiki_db->getLanguages()), CommandCode::GET_LANGUAGES);
        }
        case CommandCode::GET_CATEGORIES: {
            auto data = dynamic_cast_throw<GetCategoriesData>(cmd._data.get());
            auto &wiki_db = _dbs.getWikiDB();

            try {
                return create_response(ListResponseData<WikiDatabase::categories_t>(wiki_db->getVersion(), wiki_db->getCategories(data->getLanguage())), CommandCode::GET_CATEGORIES);
            } catch (LanguageNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_LANGUAGE, cmd._command_code);
            }
        }
        case CommandCode::GET_FUNCTIONS_NAMES: {
            auto data = dynamic_cast_throw<GetFunctionsData>(cmd._data.get());
            auto &wiki_db = _dbs.getWikiDB();

            try {
                return create_response(ListResponseData<WikiDatabase::functions_t>(wiki_db->getVersion(), wiki_db->getFunctions(data->getLanguage(), data->getCategory())), CommandCode::GET_FUNCTIONS_NAMES);
            } catch (LanguageNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_LANGUAGE, cmd._command_code);
            } catch (CategoryNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_CATEGORY, cmd._command_code);
            }
        }
        case CommandCode::GET_FUNCTIONS: {
            auto data = dynamic_cast_throw<GetFunctionsData>(cmd._data.get());
            auto &wiki_db = _dbs.getWikiDB();

            try {
                return create_response(FunctionsResponseData(wiki_db->getVersion(), wiki_db->getFunctions(data->getLanguage(), data->getCategory())), CommandCode::GET_FUNCTIONS);
            } catch (LanguageNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_LANGUAGE, cmd._command_code);
            } catch (CategoryNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_CATEGORY, cmd._command_code);
            }
        }
        case CommandCode::GET_FUNCTION: {
            auto data = dynamic_cast_throw<GetFunctionData>(cmd._data.get());
            auto &wiki_db = _dbs.getWikiDB();

            try {
            return create_response(FunctionResponseData(wiki_db->getVersion(), wiki_db->getFunction(data->getLanguage(), data->getName())), CommandCode::GET_FUNCTION);
            } catch (LanguageNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_LANGUAGE, cmd._command_code);
            } catch (FunctionNotFoundException &) {
                return Response(StatusCode::ERR_UNKNOWN_FUNCTION, cmd._command_code);
            }
        }
        case CommandCode::REPORT_BUG: {
            auto data = dynamic_cast_throw<ReportBugData>(cmd._data.get());

            _dbs.getCrashReportDB()->report_crash(data->getStacktrace(), data->getMessage(), std::make_shared<std::string>(nullptr), data->getSoftware(), data->getOS(), data->getRAM(), data->getUUID(), (_user.size() != 0 ? std::make_unique<std::string>(_user) : std::shared_ptr<std::string>(nullptr)));
            return Response(StatusCode::STATUS_OK, CommandCode::REPORT_BUG);
        }
        default:
            return Response(StatusCode::ERR_UNKNOWN_COMMAND, cmd._command_code);
    }
}

#include <bitset> // TODO remove

std::string Protocol::handle_command(const std::string &command, std::string &)
{
    return handle_command(command, false);
}

std::string Protocol::handle_command(const std::string &command, bool unfragmented)
{
    std::stringstream stream(command);
    std::string res;

    while (stream.good()) {
        Command cmd;

        try {
            cmd = Command::fromString(stream, unfragmented);

            std::cout << "Input: " << cmd._command_code << std::endl;
            try {
                auto tmp = process_command(cmd);

                std::cout << "Output: " << tmp._command_code << " " << tmp._status_code << std::endl;
                res += tmp.toString();
                Logger::log() << "Command ok";
            } catch (std::exception &e) {
                Logger::error() << "INTERNAL SERVER ERROR : " << e.what();
                res += Response(StatusCode::ERR_INTERNAL_SERVER_EROR, cmd._command_code).toString();
            } catch (...) { // TODO check exception type to log and maybe do thing with it ?
                Logger::error() << "INTERNAL SERVER ERROR";
                res += Response(StatusCode::ERR_INTERNAL_SERVER_EROR, cmd._command_code).toString();
            }
        } catch (const std::uint8_t &code) {
            Logger::error() << "PARAMETER ERROR";
            res += Response(StatusCode::ERR_PARAMETER_ERROR, code).toString();
        } catch (...) {
            Logger::error() << "PROTOCOL ERROR";
            res += Response(StatusCode::ERR_PROTOCOL_MISSMATCH, 0).toString();
        }
        stream.peek();
    }
    return res;
}
