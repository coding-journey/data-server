#include "cj_protocol/2.1.1/CJ_Protocol.hpp"
#include "cj_protocol/2.1.1/ResponseData.hpp"
#include "cj_protocol/BitString.hpp"
#include "cj_protocol/CustomFileData.hpp"

#include <openssl/md5.h>
static_assert(MD5_DIGEST_LENGTH == 16, "MD5_DIGEST is not 16 octets");

using namespace CJ_Protocol_2_1_1;

std::uint16_t IResponseData::getSize() const
{
    return _fragmented ? Protocol::fragmented_size : _data.size();
}

bool IResponseData::isFragmented() const
{
    return _fragmented;
}

std::string IResponseData::toString() const
{
    return _data;
}

void IResponseData::setData(const std::string &data)
{
    _data = data;
    _fragmented = _data.size() >= Protocol::fragmented_size;
}

VersionResponseData::VersionResponseData(std::uint16_t version)
{
    BitString str;

    str.insert_bits(version);
    setData(str.extract_bits<std::string>());
}

VersionResponseData::VersionResponseData(std::uint8_t major, std::uint8_t minor, std::uint8_t patch)
{
    BitString str;

    str.insert_bits<std::uint8_t>(major, 3);
    str.insert_bits<std::uint8_t>(minor, 5);
    str.insert_bits<std::uint8_t>(patch, 8);
    setData(str.extract_bits<std::string>());
}

ListFilesResponseData::ListFilesResponseData(const std::vector<std::shared_ptr<const FileData>> &files)
{
    BitString str;

    if (files.size() > std::numeric_limits<std::uint16_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint16_t>(files.size());
    for (const auto &iter : files) {
        std::string filename = iter->getKey();
        std::string content = iter->getContent();
        unsigned char *buff = MD5((const unsigned char *)content.c_str(), content.size(), nullptr);

        if (filename.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(filename.size());
        str.insert_bits(filename);
        str.insert_bits<std::uint64_t>(iter->getTimestamp());
        str.insert_bits((const char *)buff, MD5_DIGEST_LENGTH * 8);
    }
    setData(str.extract_bits<std::string>());
}

DownloadResponseData::DownloadResponseData(const std::shared_ptr<const FileData> &file)
{
    BitString str;
    std::string filename = file->getKey();
    std::string content = file->getContent();
    auto data = file->getCustomData<CustomFileData>();

    if (filename.size() > std::numeric_limits<std::uint8_t>::max())
        throw "Err"; // TODO
    if (content.size() > Protocol::fragmented_size)
        throw "Err"; // TODO
    str.insert_bits<std::uint8_t>(filename.size());
    str.insert_bits(filename);
    str.insert_bits<std::uint64_t>(file->getTimestamp());
    str.insert_bits<std::uint8_t>(data.isPublic(), 1);
    str.insert_bits<std::uint8_t>(0, 4); // Unused
    str.insert_bits<std::uint16_t>(content.size(), 11);
    str.insert_bits(content);
    setData(str.extract_bits<std::string>());
}

DownloadResponseData::DownloadResponseData(const std::shared_ptr<const FileData> &file, std::uint16_t blocks)
{
    BitString str;
    std::string filename = file->getKey();
    auto data = file->getCustomData<CustomFileData>();

    if (filename.size() > std::numeric_limits<std::uint8_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint8_t>(filename.size());
    str.insert_bits(filename);
    str.insert_bits<std::uint64_t>(file->getTimestamp());
    str.insert_bits<std::uint8_t>(data.isPublic(), 1);
    str.insert_bits<std::uint8_t>(0, 4); // Unused
    str.insert_bits<std::uint16_t>(Protocol::fragmented_size, 11);
    str.insert_bits<std::uint16_t>(blocks);
    setData(str.extract_bits<std::string>());
}

FragmentedResponseData::FragmentedResponseData(std::uint16_t blockID, const std::string &data)
{
    BitString str;

    if (data.size() > (std::size_t)(Protocol::fragmented_size - 4 - 1))
        throw "Err"; // TODO
    str.insert_bits<std::uint16_t>(blockID);
    str.insert_bits<std::uint8_t>(0, 5); // Unused
    str.insert_bits<std::uint16_t>(data.size(), 11);
    str.insert_bits(data);
    setData(str.extract_bits<std::string>());
}

ListUsersResponseData::ListUsersResponseData(const std::vector<std::shared_ptr<PseudoData>> &data)
{
    BitString str;

    if (data.size() > std::numeric_limits<std::uint32_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint32_t>(data.size());
    for (const auto &iter : data) {
        std::string pseudo = iter->getPseudo();
        std::string steam_id = iter->getSteamID();

        if (steam_id.size() != Protocol::steam_id_size)
            throw "Err"; // TODO
        str.insert_bits(steam_id);
        if (pseudo.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(pseudo.size());
        str.insert_bits(pseudo);
    }
    setData(str.extract_bits<std::string>());
}

GetUserIdResponseData::GetUserIdResponseData(const std::string &steam_id)
{
    BitString str(steam_id);

    if (steam_id.size() != Protocol::steam_id_size)
        throw "Err"; // TODO
    setData(str.extract_bits<std::string>());
}

ListResponseData<WikiDatabase::languages_t>::ListResponseData(std::uint16_t version, const WikiDatabase::languages_t &values)
{
    BitString str;

    str.insert_bits<std::uint16_t>(version);
    str.insert_bits<std::uint8_t>(values.size());
    for (const auto &iter : values) {
        if (iter.first.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(iter.first.size());
        str.insert_bits(iter.first);
    }
    setData(str.extract_bits<std::string>());
}

ListResponseData<WikiDatabase::categories_t>::ListResponseData(std::uint16_t version, const WikiDatabase::categories_t &values)
{
    BitString str;

    str.insert_bits<std::uint16_t>(version);
    str.insert_bits<std::uint8_t>(values.size());
    for (const auto &iter : values) {
        if (iter.first.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(iter.first.size());
        str.insert_bits(iter.first);
    }
    setData(str.extract_bits<std::string>());
}

ListResponseData<WikiDatabase::functions_t>::ListResponseData(std::uint16_t version, const WikiDatabase::functions_t &values)
{
    BitString str;

    str.insert_bits<std::uint16_t>(version);
    str.insert_bits<std::uint8_t>(values.size());
    for (const auto &iter : values) {
        if (iter.first.size() > std::numeric_limits<std::uint8_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint8_t>(iter.first.size());
        str.insert_bits(iter.first);
    }
    setData(str.extract_bits<std::string>());
}

FunctionsResponseData::FunctionsResponseData(std::uint16_t version, const WikiDatabase::functions_t &values)
{
    BitString str;

    str.insert_bits<std::uint16_t>(version);
    str.insert_bits<std::uint8_t>(values.size());
    for (const auto &iter : values) {
        if (iter.second.size() > std::numeric_limits<std::uint16_t>::max())
            throw "Err"; // TODO
        str.insert_bits<std::uint16_t>(iter.second.size());
        str.insert_bits(iter.second);
    }
    setData(str.extract_bits<std::string>());
}

FunctionResponseData::FunctionResponseData(std::uint16_t version, const std::string &fun)
{
    BitString str;

    str.insert_bits<std::uint16_t>(version);
    if (fun.size() > std::numeric_limits<std::uint16_t>::max())
        throw "Err"; // TODO
    str.insert_bits<std::uint16_t>(fun.size());
    str.insert_bits(fun);
    setData(str.extract_bits<std::string>());
}
