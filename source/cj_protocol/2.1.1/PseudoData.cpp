#include "cj_protocol/2.1.1/CJ_Protocol.hpp"
#include "cj_protocol/2.1.1/PseudoData.hpp"

#include "exceptions/ContextException.hpp"

using namespace CJ_Protocol_2_1_1;

PseudoData::PseudoData(std::string pseudo, std::string steamID) :
    _pseudo(std::move(pseudo)),
    _steam_id(std::move(steamID))
{}

std::string PseudoData::getKey() const
{
    return _pseudo;
}

bool PseudoData::isValid() const
{
    if (_pseudo.size() == 0)
        return false;
    if (_pseudo.size() > PseudoData::max_pseudo)
        return false;
    if (_steam_id.size() != Protocol::steam_id_size)
        return false;
    return true;
}

bool PseudoData::operator==(const ISerialisableData &other) const
{
    try {
        auto a = dynamic_cast<const PseudoData &>(other);

        return a == *this;
    } catch (const std::bad_cast &) {
        return false;
    }
}

bool PseudoData::operator==(const PseudoData &other) const
{
    return this->_pseudo == other._pseudo &&
        this->_steam_id == other._steam_id;
}

std::string PseudoData::serialize() const
{
    std::uint8_t size = _pseudo.size();
    std::string res;

    if (!isValid())
        throw NewContextException(getCantSerializeExceptionMessage());
    res += std::string((char *)&size, sizeof(std::uint8_t));
    res += _pseudo;
    res += _steam_id;
    return res;
}

void PseudoData::unserialize(std::istream &data)
{
    std::uint8_t size = 0;

    if (data.peek() == std::istream::traits_type::eof())
        throw NewContextException(getNotSerializedExceptionMessage());
    size += ISerialisableData::hton((std::uint8_t)data.get());
    _pseudo = extract_chars(data, size);
    _steam_id = extract_chars(data, Protocol::steam_id_size);
}

void PseudoData::migrate(std::uint8_t, std::istream &)
{
}
