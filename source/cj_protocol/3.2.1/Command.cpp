#include "cj_protocol/3.2.1/CJ_Protocol.hpp"
#include "cj_protocol/3.2.1/Command.hpp"
#include "cj_protocol/BitString.hpp"

// TODO remove usings
using namespace CJ_Protocol_3_2_1;

Command::Command(std::uint8_t code, std::uint16_t size, const std::string &data, bool unfragmented) :
    _command_code(code),
    _data_size(size),
    _rawData(data),
    _data(initData(size, data, unfragmented))
{}

// TODO is it possible to make a function pointer to constructor ?
ICommandData *Command::initData(std::uint16_t size, const std::string &data, bool unfragmented) const
{
    try {
        switch (_command_code) {
            case CommandCode::LIST_USERS:
            case CommandCode::LIST_PROJECTS:
            case CommandCode::GET_LANGUAGES:
            case CommandCode::VERSION:
                return new NoData(size, data, unfragmented);
            case CommandCode::REQUEST_SERVER_VERSION:
                return new RequestVersionData(size, data, unfragmented);
            case CommandCode::DELETE_ACCOUNT:
                return new DeleteAccountData(size, data, unfragmented);
            case CommandCode::REGISTER:
                return new RegisterData(size, data, unfragmented);
            case CommandCode::LOGIN:
                return new LoginData(size, data, unfragmented);
            case CommandCode::LOGOUT:
            case CommandCode::LIST_FILES:
                return new NoDataLogin(size, data, unfragmented);
            case CommandCode::DELETE_PROJECT:
            case CommandCode::DELETE_FILE:
            case CommandCode::LIST_FILE:
            case CommandCode::DOWNLOAD_FILE:
                return new DownloadData(size, data, unfragmented);
            case CommandCode::UPLOAD_FILE:
            case CommandCode::UPLOAD_PROJECT:
                return new UploadData(size, data, unfragmented);
            case CommandCode::FRAGMENTED_DATA:
                return new FragmentedData(size, data, unfragmented);
            case CommandCode::LIST_PUBLIC_FILES:
                return new ListPublicFilesData(size, data, unfragmented);
            case CommandCode::DOWNLOAD_PUBLIC_FILE:
                return new DownloadPublicFileData(size, data, unfragmented);
            case CommandCode::GET_CATEGORIES:
                return new GetCategoriesData(size, data, unfragmented);
            case CommandCode::GET_FUNCTIONS_NAMES:
            case CommandCode::GET_FUNCTIONS:
                return new GetFunctionsData(size, data, unfragmented);
            case CommandCode::GET_FUNCTION:
                return new GetFunctionData(size, data, unfragmented);
            case CommandCode::REPORT_CRASH:
                return new ReportCrashData(size, data, unfragmented);
            default:
                return nullptr;
        }
    } catch (...) {
        // TODO don't throw, find something else
        throw this->_command_code;
    }
}

Command &Command::operator=(Command &&other) noexcept
{
    this->_command_code = other._command_code;
    this->_data_size = other._data_size;
    this->_rawData = std::move(other._rawData);
    this->_data = std::move(other._data);
    return *this;
}

Command::Command(Command &&other) noexcept :
    _command_code(other._command_code),
    _data_size(other._data_size),
    _rawData(std::move(other._rawData)),
    _data(std::move(other._data))
{}

// TODO use BitString here ?
std::string Command::toString() const
{
    std::uint8_t code = _command_code;
    std::uint16_t size = _data_size;
    std::string res;

    code = (code << 3U) + (size >> 8U);
    res += std::string((char *)&code, sizeof(std::uint8_t));
    code = (size << 8U) >> 8U;
    res += std::string(((char *)&code), sizeof(std::uint8_t));
    res += _rawData;
    return res;
}

std::string Command::display() const
{
    std::stringstream res;

    res << "Code: " << std::to_string(_command_code) << std::endl;
    res << "Size: " << std::to_string(_data_size) << std::endl;
    // res << "RawData: '" << _rawData << "'" << std::endl;
    res << _data->display() << std::endl;
    return res.str();
}

Command Command::fromString(std::stringstream &stream, bool unfragmented)
{
    std::array<unsigned char, 2> buff = {};
    std::uint16_t size;
    std::uint8_t code;
    std::string data;
    BitString str;

    stream.read((char *)buff.data(), buff.size());
    if (stream.gcount() != (int)buff.size())
        throw "Error"; // TODO
    str.insert_bits(std::string((char *)buff.data(), 2), 2 * 8);
    code = str.extract_bits<std::uint8_t>(5);
    size = str.extract_bits<std::uint16_t>(11);
    if (size || unfragmented) {
        std::uint16_t _size = (unfragmented ? stream.str().size() - buff.size() :
                               ((size == Protocol::fragmented_size) ? 2 : size));
        std::vector<char> tmp(_size);

        stream.read(tmp.data(), _size);
        if (stream.gcount() != _size)
            throw "Error"; // TODO
        data = std::string(tmp.data(), _size);
    }
    return Command(code, size, data, unfragmented);
}

bool Command::operator==(const Command &o)
{
    return _command_code == o._command_code &&
        _data_size == o._data_size &&
        _rawData == o._rawData;
}
