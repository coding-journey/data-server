#include "cj_protocol/WikiDatabase.hpp"

#include "exceptions/ContextException.hpp"

#include <algorithm>
#include <fstream>

#include <iostream>

WikiDatabase::WikiDatabase(const std::string &db_dir) :
    m_root_dir(db_dir + "/wiki/")
{
    std::error_code ec;
    std::ifstream file;

    if (!std::filesystem::is_directory(m_root_dir, ec))
        throw NewContextException("Couldn't open wiki directory : " + ec.message());
    file.open(m_root_dir + "version.txt");

    if (!file.is_open())
        throw NewContextException("Couldn't open version file : " + ec.message());
    file >> m_version;

    for (const auto &iter : std::filesystem::directory_iterator(m_root_dir)) {
        if (!iter.is_directory())
            continue;
        loadLanguage(iter);
    }
}

void WikiDatabase::loadLanguage(const std::filesystem::directory_entry &e)
{
    std::string lang = e.path().filename();

    for (const auto &iter : std::filesystem::directory_iterator(e)) {
        if (!iter.is_directory())
            continue;
        loadCategory(lang, iter);
    }
}

void WikiDatabase::loadCategory(const std::string &lang, const std::filesystem::directory_entry &e)
{
    std::string categ = e.path().filename();
    auto &functions = m_functions[lang][categ];
    std::size_t nb_files = 0;

    for (const auto &iter : std::filesystem::directory_iterator(e)) {
        if (iter.is_regular_file())
            nb_files++;
    }
    functions.reserve(nb_files);
    for (const auto &iter : std::filesystem::directory_iterator(e)) {
        if (!iter.is_regular_file())
            continue;
        const std::filesystem::path &path = iter.path();
        std::ifstream file(path);
        std::string content((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

        functions.emplace_back(std::make_pair(path.filename(), content));
    }
}

std::uint16_t WikiDatabase::getVersion() const
{
    return m_version;
}


const WikiDatabase::languages_t &WikiDatabase::getLanguages() const
{
    return m_functions;
}

const WikiDatabase::categories_t &WikiDatabase::getCategories(const std::string &lang) const
{
    try {
        return m_functions.at(lang);
    } catch (std::out_of_range &) {
        throw LanguageNotFoundException(lang);
    }
}

const WikiDatabase::functions_t &WikiDatabase::getFunctions(const std::string &lang, const std::string &category) const
{
    try {
        return getCategories(lang).at(category);
    } catch (std::out_of_range &) {
        throw CategoryNotFoundException(category);
    }
}

std::string WikiDatabase::getFunction(const std::string &lang, const std::string &name) const
{
    const categories_t &c = getCategories(lang);

    for (const auto &iter : c) {
        for (const auto &func : iter.second) {
            if (func.first == name)
                return func.second;
        }
    }
    throw FunctionNotFoundException(name);
}

LanguageNotFoundException::LanguageNotFoundException(const std::string &name) :
    m_msg("Language ")
{
    m_msg += name;
    m_msg += " doesn't exists.";
}

const char *LanguageNotFoundException::what() const noexcept
{
    return m_msg.c_str();
}

CategoryNotFoundException::CategoryNotFoundException(const std::string &name) :
    m_msg("Category ")
{
    m_msg += name;
    m_msg += " doesn't exists.";
}

const char *CategoryNotFoundException::what() const noexcept
{
    return m_msg.c_str();
}

FunctionNotFoundException::FunctionNotFoundException(const std::string &name) :
    m_msg("Function ")
{
    m_msg += name;
    m_msg += " doesn't exists.";
}

const char *FunctionNotFoundException::what() const noexcept
{
    return m_msg.c_str();
}
