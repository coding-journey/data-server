#include "cj_protocol/CJ_ProtocolManager.hpp"
#include "cj_protocol/2.1.1/CJ_Protocol.hpp"
#include "cj_protocol/3.2.1/CJ_Protocol.hpp"
#include "cj_protocol/4.1.1/CJ_Protocol.hpp"
#include "cj_protocol/5.1.1/CJ_Protocol.hpp"

CJ_ProtocolManager::CJ_ProtocolManager(DatabasesManager &dbs) :
    _dbs(dbs),
    _actif(std::make_shared<CJ_Protocol_2_1_1::Protocol>(this))
{}

std::string CJ_ProtocolManager::handle_command(const std::string &command, std::string &remaining)
{
    return _actif->handle_command(command, remaining);
}

std::uint16_t CJ_ProtocolManager::change_version(std::uint8_t major, std::uint8_t minor, std::uint8_t patch)
{
    (void) minor;
    (void) patch;
    switch (major) {
        case 0:
        case 1:
            _actif = std::make_shared<CJ_Protocol_2_1_1::Protocol>(this);
            break;
        case 2:
            _actif = std::make_shared<CJ_Protocol_3_2_1::Protocol>(this);
            break;
        case 3:
            _actif = std::make_shared<CJ_Protocol_4_1_1::Protocol>(this);
            break;
        case 4:
            _actif = std::make_shared<CJ_Protocol_5_1_1::Protocol>(this);
            break;
        default:
            throw "Err"; // TODO
    }
    return _actif->getVersion();
}

DatabasesManager &CJ_ProtocolManager::getDBs() const
{
    return _dbs;
}

std::uint64_t CJ_ProtocolManager::getVersion() const
{
    return _actif->getVersion();
}
