#include "cj_protocol/CustomFileData.hpp"
#include "cj_protocol/BitString.hpp"

#include "exceptions/ContextException.hpp"

std::string CustomFileData::serialize() const
{
    BitString str;

    str.insert_bits(m_isPublic, 1);
    str.insert_bits(m_isProject, 1);
    str.insert_bits(0, 6); // Unused

    return str.extract_bytes<std::string>();
}

bool CustomFileData::operator==(const CustomFileData &other) const
{
    return other.m_isProject == this->m_isProject && other.m_isPublic == this->m_isPublic;
}

void CustomFileData::unserialize(std::istream &data)
{
    if (data.peek() == std::istream::traits_type::eof())
        throw NewContextException("Not a serialized CustomFileData");
    BitString str;

    str.insert_bytes<std::uint8_t>(data.get());
    m_isPublic = str.extract_bits<std::uint8_t>(1);
    m_isProject = str.extract_bits<std::uint8_t>(1);
    str.extract_bits<std::uint8_t>(6); // Unused
}
