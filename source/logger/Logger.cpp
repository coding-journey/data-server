#include "logger/Logger.hpp"
#include "exceptions/ContextException.hpp"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <unordered_map>

std::string Logger::log_dir;
const char *const Logger::default_logger = "log";
const char *const Logger::error_logger = "error";
const char *const Logger::default_stream = "cout";
const char *const Logger::error_stream = "cerr";

std::string Logger::time(const std::string &format)
{
    std::time_t t = std::time(nullptr);
    std::tm tm = *std::localtime(&t);
    std::ostringstream s;

    s << std::put_time(&tm, format.c_str());
    return s.str();
}

Logger::Formatter Logger::getLogger(const std::string &name, LogLevel level)
{
    return Logger::Formatter(name, level);
}

Logger::Formatter Logger::getLoggers(const std::vector<std::string> &names, LogLevel level)
{
    return Logger::Formatter(names, level);
}

Logger::Formatter Logger::log(LogLevel level)
{
    return getLoggers({default_logger, default_stream}, level);
}

Logger::Formatter Logger::info()
{
    return getLoggers({default_logger, default_stream}, LogLevel::INFO);
}

Logger::Formatter Logger::debug()
{
    return getLoggers({default_logger, default_stream}, LogLevel::DEBUG);
}

Logger::Formatter Logger::warn()
{
    return getLoggers({default_logger, default_stream}, LogLevel::WARN);
}

Logger::Formatter Logger::error()
{
    return getLoggers({default_logger, error_logger, error_stream}, LogLevel::ERROR);
}

std::ostream &Logger::get_log_stream(const std::string &name)
{
    static std::unordered_map<std::string, std::fstream> _files;
    std::error_code ec;

    if (Logger::log_dir == "")
        Logger::log_dir = "./logs";
    if (name == default_stream)
        return std::cout;
    if (name == error_stream)
        return std::cerr;
    if (_files.count(name) == 0) {
        std::string dir = std::string(Logger::log_dir) + "/";
        std::string log_file = name + Logger::time("-%d-%m-%Y_%H_%M_%S.log");
        std::string link_name = name + ".latest";

        if (!std::filesystem::is_directory(Logger::log_dir, ec) || ec) {
            if (!std::filesystem::create_directories(Logger::log_dir, ec) || ec)
                throw NewContextException("Failed to create log directory");
        }
        std::filesystem::remove(dir + link_name, ec);
        std::filesystem::create_symlink(log_file, dir + link_name, ec);
        _files[name].open(dir + log_file,
                          std::ios::out | std::ios::ate);
        if (!_files[name].is_open())
            throw NewContextException("Failed to create log file");
    }
    return _files[name];
}

Logger::Formatter::Formatter(const std::string &name, LogLevel level) :
    _level(level)
{
    addStream(name);
}

Logger::Formatter::Formatter(std::vector<std::string> names, LogLevel level) :
    _stream_names(std::move(names)),
    _level(level)
{}

Logger::Formatter &Logger::Formatter::addStream(const std::string &name)
{
    _stream_names.push_back(name);
    return *this;
}

std::string Logger::Formatter::formatLevel(LogLevel level)
{
    switch (level) {
        case Logger::INFO:
            return " [INFO] ";
        case Logger::DEBUG:
            return " [DEBUG] ";
        case Logger::WARN:
            return " [WARNING] ";
        case Logger::ERROR:
            return " [ERROR] ";
        default:
            return "";
    }
}

Logger::Formatter::~Formatter()
{
    std::string str = this->str();

    for (auto &name : _stream_names) {
        auto &stream = static_cast<std::ostream &>(Logger::get_log_stream(name));
        std::stringstream ss{str};

        for (std::string line; std::getline(ss, line); ) {
            stream << _time << formatLevel(_level) << line << std::endl;
        }
        stream.flush();
    }
}
