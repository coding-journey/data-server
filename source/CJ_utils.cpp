#include "CJ_utils.hpp"

#include <algorithm>
#include <random>
#include <limits>

//LCOV_EXCL_START
//LCOV_EXCL_LINE
//LCOV_EXCL_STOP

std::string CJ_utils::random_string(size_t length)
{
    const std::string charset =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    std::default_random_engine rng(std::random_device{}());
    std::uniform_int_distribution<> dist(0, charset.size()-1);
    auto randchar = [charset, &dist, &rng](){
        return charset[dist(rng)];
    };

    std::string str(length, 0);
    std::generate_n(str.begin(), length, randchar);
    return str;
}

std::string CJ_utils::random_raw_string(size_t length)
{
    std::default_random_engine rng(std::random_device{}());
    std::uniform_int_distribution<> dist(std::numeric_limits<char>::min(),
                                         std::numeric_limits<char>::max());
    auto randchar = [&dist, &rng](){
        return (char)dist(rng);
    };

    std::string str(length, 0);
    std::generate_n(str.begin(), length, randchar);
    return str;
}

std::uint64_t CJ_utils::random_number(std::uint64_t min, std::uint64_t max)
{
    std::default_random_engine rng(std::random_device{}());
    std::uniform_int_distribution<unsigned long long> dist(min, max);

    return dist(rng);
}
