#include "database/FileData.hpp"
#include "exceptions/ContextException.hpp"

#include <vector>

constexpr auto could_not_serialize =  "FileData: Can't serialize";
constexpr auto not_serialized = "Not a serialized FileData";

FileData::FileData(std::string filename, std::uint64_t timestamp, std::string content, std::string custom_data) :
    _filename(std::move(filename)),
    _content(std::move(content)),
    _custom_data(std::move(custom_data)),
    _timestamp(timestamp)
{
    if (!is_valid())
        throw NewContextException(could_not_serialize);
}

bool FileData::is_valid() const
{
    if (_filename.size() == 0)
        return false;
    if (_filename.size() > FileData::max_filename)
        return false;
    if (_content.size() > FileData::max_content)
        return false;
    return true;
}

void FileData::unserialize(std::istream &data)
{
    // Remember that if filename_size = 0, _filename.size() = 1
    std::uint16_t filename_size = 1;
    std::uint16_t custom_size;
    std::uint32_t content_size;
    std::uint8_t version;

    if (data.peek() == std::istream::traits_type::eof())
        throw NewContextException(not_serialized);
    version = ISerialisableData::hton((std::uint8_t)data.get());
    if (version != current_version)
        return migrate(version, data);
    filename_size += ISerialisableData::hton((std::uint8_t)data.get());
    _filename = extract_chars(data, filename_size);
    _timestamp = ISerialisableData::hton(*(std::uint64_t *)
                    extract_chars(data, sizeof(std::uint64_t)).c_str());
    custom_size = ISerialisableData::hton(*(std::uint16_t *)
                    extract_chars(data, sizeof(std::uint16_t)).c_str());
    _custom_data = extract_chars(data, custom_size);
    content_size = ISerialisableData::hton(*(std::uint32_t *)
                    extract_chars(data, sizeof(std::uint32_t)).c_str());
    _content = extract_chars(data, content_size);
}

std::string FileData::serialize() const
{
    // Remember that if filename_size = 0, _filename.size() = 1
    std::uint8_t filename_size = ISerialisableData::hton((std::uint8_t)(_filename.size() - 1));
    std::uint16_t custom_size = ISerialisableData::hton((std::uint16_t)_custom_data.size());
    std::uint32_t content_size = ISerialisableData::hton((std::uint32_t)_content.size());
    std::uint64_t tmp_timestamp = ISerialisableData::hton(_timestamp);
    std::string res;

    if (!is_valid())
        throw NewContextException(could_not_serialize);
    res += std::string((char *)&current_version, sizeof(std::uint8_t));
    res += std::string((char *)&filename_size, sizeof(std::uint8_t));
    res += _filename;
    res += std::string((char *)&tmp_timestamp, sizeof(std::uint64_t));
    res += std::string((char *)&custom_size, sizeof(std::uint16_t));
    res += _custom_data;
    res += std::string((char *)&content_size, sizeof(std::uint32_t));
    res += _content;
    return res;
}

const std::uint8_t FileData::current_version = 0;

void FileData::migrate(std::uint8_t version, std::istream &data)
{
    // Remember that if filename_size = 0, _filename.size() = 1
    std::uint16_t filename_size = 1;
    std::uint16_t custom_size;
    std::uint32_t content_size;

    switch (version) {
        default:
            filename_size += version;
            _filename = extract_chars(data, filename_size);
            _timestamp = ISerialisableData::hton(*(std::uint64_t *)
                extract_chars(data, sizeof(std::uint64_t)).c_str());
            custom_size = ISerialisableData::hton(*(std::uint16_t *)
                extract_chars(data, sizeof(std::uint16_t)).c_str());
            _custom_data = extract_chars(data, custom_size);
            content_size = ISerialisableData::hton(*(std::uint32_t *)
                extract_chars(data, sizeof(std::uint32_t)).c_str());
            _content = extract_chars(data, content_size);
//            throw NewContextException(getNotSerializedExceptionMessage());
    }
}

bool FileData::operator==(const ISerialisableData &other) const
{
    try {
        auto a = dynamic_cast<const FileData &>(other);

        return a == *this;
    } catch (const std::bad_cast &) {
        return false;
    }
}

bool FileData::operator==(const FileData &other) const
{
    return this->_filename == other._filename &&
        this->_timestamp == other._timestamp &&
        this->_content == other._content;
}

std::string FileData::getKey() const
{
    return _filename;
}

const std::string &FileData::getContent() const
{
    return _content;
}

std::uint64_t FileData::getTimestamp() const
{
    return _timestamp;
}

const std::string &FileData::getFilename() const
{
    return _filename;
}

//LCOV_EXCL_START
std::ostream& operator<<(std::ostream& os, const FileData& dt)
{
    return os << "FileData(" << dt._filename << ", " << dt._timestamp << ", "
              << (dt._content.size() < 256 ? dt._content : "CONTENT") << ")";
}
//LCOV_EXCL_STOP
