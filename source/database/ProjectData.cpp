#include "database/ProjectData.hpp"
#include "exceptions/ContextException.hpp"
#include "logger/Logger.hpp"

#include <vector>

constexpr auto could_not_serialize =  "ProjectData: Can't serialize";
constexpr auto not_serialized = "Not a serialized ProjectData";

ProjectData::ProjectData(const std::string &owner, const std::string &project_name, const std::string &meta_file, const std::string &file, std::uint64_t timestamp, bool isApproved, std::uint8_t game_version) :
    _owner(std::move(owner)),
    _project_name(std::move(project_name)),
    _meta_file(std::move(meta_file)),
    _file(std::move(file)),
    _timestamp(timestamp),
    _isApproved(isApproved),
    _game_version(game_version)
{
    if (!is_valid())
        throw NewContextException(could_not_serialize);
}

bool ProjectData::is_valid() const
{
    if (_owner.size() > ProjectData::max_string_size)
        return false;
    if (_project_name.size() > ProjectData::max_string_size)
        return false;
    if (_meta_file.size() > ProjectData::max_string_size)
        return false;
    if (_file.size() > ProjectData::max_string_size)
        return false;
    return true;
}

void ProjectData::unserialize(std::istream &data)
{
    std::uint8_t owner_size;
    std::uint8_t name_size;
    std::uint8_t meta_size;
    std::uint8_t file_size;
    std::uint8_t version;

    if (data.peek() == std::istream::traits_type::eof())
        throw NewContextException(not_serialized);
    version = ISerialisableData::hton((std::uint8_t)data.get());
    if (version != current_version)
        return migrate(version, data);
    owner_size = ISerialisableData::hton((std::uint8_t)data.get());
    _owner = extract_chars(data, owner_size);
    name_size = ISerialisableData::hton((std::uint8_t)data.get());
    _project_name = extract_chars(data, name_size);
    _timestamp = ISerialisableData::hton(*(std::uint64_t *)
                    extract_chars(data, sizeof(std::uint64_t)).c_str());
    meta_size = ISerialisableData::hton(*(std::uint16_t *)
                    extract_chars(data, sizeof(std::uint16_t)).c_str());
    _meta_file = extract_chars(data, meta_size);
    file_size = ISerialisableData::hton(*(std::uint32_t *)
                    extract_chars(data, sizeof(std::uint32_t)).c_str());
    _file = extract_chars(data, file_size);
    _game_version = ISerialisableData::hton((std::uint8_t)data.get());
    _isApproved = ISerialisableData::hton((std::uint8_t)data.get());
}

std::string ProjectData::serialize() const
{
    std::uint8_t owner_size = ISerialisableData::hton((std::uint8_t)(_owner.size()));
    std::uint8_t name_size = ISerialisableData::hton((std::uint8_t)(_project_name.size()));
    std::uint16_t meta_size = ISerialisableData::hton((std::uint16_t)_meta_file.size());
    std::uint32_t file_size = ISerialisableData::hton((std::uint32_t)_file.size());
    std::uint64_t tmp_timestamp = ISerialisableData::hton(_timestamp);
    std::string res;

    if (!is_valid())
        throw NewContextException(could_not_serialize);
    res += std::string((char *)&current_version, sizeof(std::uint8_t));
    res += std::string((char *)&owner_size, sizeof(std::uint8_t));
    res += _owner;
    res += std::string((char *)&name_size, sizeof(std::uint8_t));
    res += _project_name;
    res += std::string((char *)&tmp_timestamp, sizeof(std::uint64_t));
    res += std::string((char *)&meta_size, sizeof(std::uint16_t));
    res += _meta_file;
    res += std::string((char *)&file_size, sizeof(std::uint32_t));
    res += _file;
    res += std::string((char *)&_game_version, sizeof(std::uint8_t));
    res += (char)_isApproved;
    return res;
}

const std::uint8_t ProjectData::current_version = 1;

void ProjectData::migrate(std::uint8_t version, std::istream &data)
{
    std::uint8_t owner_size;
    std::uint8_t name_size;
    std::uint8_t meta_size;
    std::uint8_t file_size;

    Logger::warn() << "[ProjectData] MIGRATION...";
    switch (version) {
        case 0:
            owner_size = ISerialisableData::hton((std::uint8_t)data.get());
            _owner = extract_chars(data, owner_size);
            name_size = ISerialisableData::hton((std::uint8_t)data.get());
            _project_name = extract_chars(data, name_size);
            _timestamp = ISerialisableData::hton(*(std::uint64_t *)
                extract_chars(data, sizeof(std::uint64_t)).c_str());
            meta_size = ISerialisableData::hton(*(std::uint16_t *)
                extract_chars(data, sizeof(std::uint16_t)).c_str());
            _meta_file = extract_chars(data, meta_size);
            file_size = ISerialisableData::hton(*(std::uint32_t *)
                extract_chars(data, sizeof(std::uint32_t)).c_str());
            _file = extract_chars(data, file_size);
            _isApproved = ISerialisableData::hton((std::uint8_t)data.get());
            _game_version = 1;
            break;
        default:
            throw NewContextException(not_serialized);
    }
}

bool ProjectData::operator==(const ISerialisableData &other) const
{
    try {
        auto a = dynamic_cast<const ProjectData &>(other);

        return a == *this;
    } catch (const std::bad_cast &) {
        return false;
    }
}

bool ProjectData::operator==(const ProjectData &other) const
{
    return this->_owner == other._owner &&
        this->_timestamp == other._timestamp &&
        this->_file == other._file &&
        this->_meta_file == other._meta_file;
}

std::string ProjectData::getKey() const
{
    return _owner + "-" + _project_name;
}

std::uint64_t ProjectData::getTimestamp() const
{
    return _timestamp;
}

const std::string &ProjectData::getOwner() const
{
    return _owner;
}

const std::string &ProjectData::getProjectName() const
{
    return _project_name;
}

const std::string &ProjectData::getFilename() const
{
    return _file;
}

const std::string &ProjectData::getMetaFilename() const
{
    return _meta_file;
}

std::uint8_t ProjectData::getGameVersion() const
{
    return _game_version;
}

bool ProjectData::isApproved() const
{
    return _isApproved;
}

void ProjectData::setTimestamp(std::uint64_t timestamp)
{
    _timestamp = timestamp;
}

void ProjectData::setOwner(const std::string &owner)
{
    _owner = owner;
}

void ProjectData::setProjectName(const std::string &project_name)
{
    _project_name = project_name;
}

void ProjectData::setFilename(const std::string &filename)
{
    _file = filename;
}

void ProjectData::setGameVersion(std::uint8_t game_version)
{
    _game_version = game_version;
}

void ProjectData::setMetaFilename(const std::string &meta_filename)
{
    _meta_file = meta_filename;
}

void ProjectData::setApproved(bool approved)
{
    _isApproved = approved;
}

//LCOV_EXCL_START
std::ostream& operator<<(std::ostream& os, const ProjectData& dt)
{
    return os << "ProjectData(" << dt._owner << ", " << dt._project_name
              << ", " << dt._timestamp << ", "
              << dt._meta_file << ", " << dt._file << ")";
}
//LCOV_EXCL_STOP
