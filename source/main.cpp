#include "cj_protocol/CJ_Protocol.hpp"
#include "cj_protocol/CJ_ProtocolManager.hpp"
#include "cj_protocol/DatabasesManager.hpp"

#include "logger/Logger.hpp"

#include "ssl/TLS_error.hpp"
#include "ssl/TLS_server.hpp"

#include <cstring>
#include <iostream>

#include <signal.h>
// TODO create a class for generic command line parsing

#define DEFAULT_PORT 2369
#define DEFAULT_TIMEOUT 10 * 60  // 10min
#define DEFAULT_CERT "./fullchain.pem"
#define DEFAULT_PRIVATE_KEY "./privkey.pem"
#define DEFAULT_RSA_KEY "./rsa.key"

// TODO Add a config file option for easier deployment
struct prog_args_t {
    std::string cert_file = DEFAULT_CERT;
    std::string pkey_file = DEFAULT_PRIVATE_KEY;
    std::string rsa_file = DEFAULT_RSA_KEY;
    int port = DEFAULT_PORT;
    int timeout = DEFAULT_TIMEOUT;
    bool help = false;
};

int help(char *av, int error)
{
    #define OUT (error ? std::cerr : std::cout)

    OUT << "Usage: " << av << " [OPTIONS]" << std::endl;
    OUT << std::endl << "OPTIONS:" << std::endl;
    OUT << "\t-p,--port\t";
    OUT << "The port the server should listen on. Default to " << DEFAULT_PORT << ".";
    OUT << std::endl;
    OUT << "\t-c,--cert\t";
    OUT << "The certificate chain file to use. Default to " << DEFAULT_CERT << ".";
    OUT << std::endl;
    OUT << "\t-k,--pkey\t";
    OUT << "The private key file to use. Default to " << DEFAULT_PRIVATE_KEY << ".";
    OUT << std::endl;
    OUT << "\t-r,--rsa\t";
    OUT << "The RSA private key file to use. Default to " << DEFAULT_RSA_KEY << ".";
    OUT << std::endl;
    OUT << "\t-t,--timeout\t";
    OUT << "The time (in seconds) before disconecting an inactive client. Put to 0 to disable. Default to " << DEFAULT_TIMEOUT << "s.";
    OUT << std::endl;

    return error;
    #undef OUT
}

int error(std::string err)
{
    std::cerr << err << std::endl;
    std::cerr << "Retry with --help" << std::endl;
    return 1;
}

// TODO error check on args beforhand (file exists ?/....)
int parse_args(int ac, char **av, prog_args_t *args)
{
    for (int i = 1; i < ac; i++) {
        if (strcmp(av[i], "-h") == 0 || strcmp(av[i], "--help") == 0) {
            args->help = true;
            return help(av[0], 0);
        }
        if (!(i + 1 < ac))
            return error("Missing argument for option '" + std::string(av[i]) + "'");
        if (strcmp(av[i], "-p") == 0 || strcmp(av[i], "--port") == 0) {
            size_t end;
            std::string tmp = av[++i];

            try {
                args->port = std::stoi(tmp, &end);
                if (end != tmp.size())
                    return help(av[0], 1);
                continue;
            } catch (...) {
                return help(av[0], 1);
            }
        }
        if (strcmp(av[i], "-c") == 0 || strcmp(av[i], "--cert") == 0) {
            args->cert_file = av[++i];
            continue;
        }
        if (strcmp(av[i], "-t") == 0 || strcmp(av[i], "--timeout") == 0) {
            size_t end;
            std::string tmp = av[++i];

            try {
                args->timeout = std::stoi(tmp, &end);
                if (end != tmp.size())
                    return help(av[0], 1);
                continue;
            } catch (...) {
                return help(av[0], 1);
            }
            continue;
        }
        if (strcmp(av[i], "-k") == 0 || strcmp(av[i], "--pkey") == 0) {
            args->pkey_file = av[++i];
            continue;
        }
        if (strcmp(av[i], "-r") == 0 || strcmp(av[i], "--rsa") == 0) {
            // TODO add a check on rsa file permissions
            args->rsa_file = av[++i];
            continue;
        }
        // TODO find a way to put it earlier if single arg cmd
        return error("Error: Unknown option '" + std::string(av[i]) + "'");
    }
    return 0;
}

int start(const prog_args_t &args)
{
    struct sigaction handler;
    time_t timeout = args.timeout;
    DatabasesManager manager(args.rsa_file, "./user_data"); // TODO add a setting for that
    // CJ_ProtocolManager p_manager_b(manager);
    // CJ_ProtocolManager *p_manager = &p_manager_b;

    handler.sa_handler = [](int) {
        TLS_server<CJ_ProtocolManager, DatabasesManager>::instance()->stop();
    };
    sigemptyset(&handler.sa_mask);
    handler.sa_flags = 0;

    sigaction(SIGINT, &handler, nullptr);
    signal(SIGPIPE, SIG_IGN);
    try {
        TLS_server<CJ_ProtocolManager, DatabasesManager>::init(args.port, args.cert_file, args.pkey_file, manager, timeout);
        TLS_server<CJ_ProtocolManager, DatabasesManager> *server = TLS_server<CJ_ProtocolManager, DatabasesManager>::instance();

        server->run();
    } catch (TLS_error &e) {
        Logger::error() << e.what() << std::endl;
        TLS_server<CJ_ProtocolManager, DatabasesManager>::deinit();
        return 1;
    }
    TLS_server<CJ_ProtocolManager, DatabasesManager>::deinit();
    return 0;
}

int main(int ac, char **av)
{
    prog_args_t args;
    int res = parse_args(ac, av, &args);

    if (res != 0 || args.help)
        return res;
    Logger::log() << "ARGS:" << std::endl;
    Logger::log() << "\tport: " << args.port << std::endl;
    Logger::log() << "\tcert: " << args.cert_file << std::endl;
    Logger::log() << "\tkey: " << args.pkey_file << std::endl;
    Logger::log() << "\trsa: " << args.rsa_file << std::endl;
    Logger::log() << "\ttimeout: " << args.timeout << "s" << std::endl;
    Logger::log() << std::endl;
    res = start(args);
    return res;
}
