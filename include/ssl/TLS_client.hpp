#pragma once

#include "ssl/IProtocol.hpp"

#include <functional>
#include <memory>
#include <netinet/in.h>
#include <openssl/ssl.h>
#include <string>
#include <ctime>

#define BUFF_SIZE 4096

class TLS_client {
    public:
        TLS_client(int fd, SSL_CTX *ctx, const struct sockaddr_in &addr, std::unique_ptr<IProtocol> protocol, time_t timeout);
        ~TLS_client();

        std::string read();
        void write(const std::string &);

        time_t get_timeout_time() const;
        int get_fd() const;
        bool handle_commands();
    private:
        int _fd;
        std::unique_ptr<SSL, std::function<void(SSL *)>> _ssl;
        std::unique_ptr<X509, std::function<void(X509 *)>> _client_cert;
        bool _shutdown = true;
        struct sockaddr_in _addr;
        std::unique_ptr<IProtocol> _protocol;
        std::string _buff;
        time_t _timeout;
        time_t _timeout_time;

        void _deinit();
        void _handle_SSL_error(int e);
};
