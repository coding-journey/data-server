#pragma once

#include <exception>
#include <string>

class TLS_error : public std::exception {
    public:
        TLS_error(const std::string &error, bool use_errno = false);

        virtual const char* what() const noexcept override;
    private:
        std::string _err;
};
