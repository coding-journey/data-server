#pragma once

#include <string>

class IProtocol {
    public:
        virtual ~IProtocol() = default;

        virtual std::string handle_command(const std::string &command, std::string &remaining) = 0;
        [[nodiscard]]
        virtual std::uint64_t getVersion() const = 0;
};
