#pragma once

#include "ssl/TLS_client.hpp"

#include <openssl/ssl.h>
#include <string>
#include <vector>
#include <memory>
#include <functional>

// TODO refactor this class to be usabled as a general TLS_server
template<class T, class UserData>
class TLS_server {
    public:
        ~TLS_server();

        void run();
        void stop();
        static void init(int port, const std::string &cert, const std::string &pkey, UserData &user_data, time_t timeout);
        static TLS_server *instance();
        static void deinit();
    private:
        void _init(int port);
        void _deinit();
        void _accept();
        TLS_server(int port, const std::string &cert, const std::string &pkey, UserData &user_data, time_t timeout);

        static TLS_server *_this;
        const SSL_METHOD *_method;
        std::unique_ptr<SSL_CTX, std::function<void(SSL_CTX *)>> _ctx;
        UserData &_user_data;
        int _socket;
        bool _running;
        fd_set _read_set;
        time_t _timeout;
        std::vector<std::unique_ptr<TLS_client>> _clients;
};

template<class T, class UserData>
TLS_server<T, UserData> *TLS_server<T, UserData>::_this = nullptr;

// TODO remove that (pimpl idiom)
#include "logger/Logger.hpp"
#include "ssl/TLS_error.hpp"

#include <arpa/inet.h>
#include <csignal>
#include <netinet/in.h>
#include <strings.h>
#include <sys/socket.h>
#include <unistd.h>

#include <openssl/err.h>

template<class T, class UserData>
void TLS_server<T, UserData>::init(int port, const std::string &cert, const std::string &pkey, UserData &user_data, time_t timeout)
{
    SSL_load_error_strings();
    SSL_library_init();
    OpenSSL_add_all_algorithms();
    _this = new TLS_server(port, cert, pkey, user_data, timeout);
}

template<class T, class UserData>
void TLS_server<T, UserData>::deinit()
{
    ERR_free_strings();
    EVP_cleanup();
    delete _this;
    _this = nullptr;
}

template<class T, class UserData>
TLS_server<T, UserData> *TLS_server<T, UserData>::instance()
{
    if (_this == nullptr)
        throw TLS_error("TLS_server not initialized !");
    return _this;
}

static int verify_callback(int preverif_ok, X509_STORE_CTX *ctx)
{
    Logger::log() << "preverif : " << preverif_ok << std::endl;
    (void)ctx;
    // TODO check client certificate
    return 1;
}

template<class T, class UserData>
TLS_server<T, UserData>::TLS_server(int port, const std::string &cert, const std::string &pkey, UserData &user_data, time_t timeout) :
    _method(TLS_server_method()),
    _ctx(SSL_CTX_new(_method), SSL_CTX_free),
    _user_data(user_data),
    _socket(socket(AF_INET, SOCK_STREAM, AF_UNSPEC)),
    _running(false),
    _timeout(timeout)
{
    try {
        if (_socket < 0)
            throw TLS_error("Failed to create socket", true);
        // Recomanded security setting
        SSL_CTX_set_options(_ctx.get(), SSL_OP_SINGLE_DH_USE);
        // SSL_VERIFY_PEER ask the client for a certificate if he has one
        SSL_CTX_set_verify(_ctx.get(), SSL_VERIFY_PEER, &verify_callback);
        if (SSL_CTX_use_certificate_chain_file(_ctx.get(), cert.c_str()) <= 0) {
            ERR_print_errors_fp(stderr);
            throw TLS_error("Error with the certificate file");
        }
        if (SSL_CTX_use_PrivateKey_file(_ctx.get(), pkey.c_str(), SSL_FILETYPE_PEM) <= 0) {
            ERR_print_errors_fp(stderr);
            throw TLS_error("Error with the private key file");
        }
        if (!SSL_CTX_check_private_key(_ctx.get()))
            throw TLS_error("Private key does not match the certificate public key");
        _init(port);
    } catch (TLS_error &e) {
        _deinit();
        throw e;
    }
}

template<class T, class UserData>
TLS_server<T, UserData>::~TLS_server()
{
    Logger::log() << "Server shutting down..." << std::endl;
    _deinit();
}

template<class T, class UserData>
void TLS_server<T, UserData>::_init(int port)
{
    struct sockaddr_in addr;
    int err = 1;

    // SO_REUSEADDR is handy but not mandatory
    // TODO add a setting to disable it ?
    if (setsockopt(_socket, SOL_SOCKET, SO_REUSEADDR, &err, sizeof(int)) < 0)
        Logger::warn() << "Failed to set SO_REUSEADDR on socket ! (Skipping it...)" << std::endl;
    bzero((char *) &addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY; // TODO add a setting ?
    addr.sin_port = htons(port);
    err = bind(_socket, (struct sockaddr *) &addr, sizeof(addr));
    if (err < 0)
        throw TLS_error("Failed to bind", true);
    err = listen(_socket, 5); // TODO maybe change this limit ?
    if (err < 0)
        throw TLS_error("Failed to listen", true);
    Logger::log() << "Server listening on port " << port << "." << std::endl;
}

template<class T, class UserData>
void TLS_server<T, UserData>::_deinit()
{
    close(_socket);
}

template<class T, class UserData>
void TLS_server<T, UserData>::stop()
{
    _running = false;
}

template<class T, class UserData>
void TLS_server<T, UserData>::_accept()
{
    struct sockaddr_in sa_cli;
    socklen_t client_len = sizeof(sa_cli);
    int fd = accept(_socket, (struct sockaddr*)&sa_cli, &client_len);
    char str[INET_ADDRSTRLEN];

    inet_ntop(AF_INET, &sa_cli.sin_addr, str, INET_ADDRSTRLEN);
    Logger::log() << "Connection from " << str << ", port " << sa_cli.sin_port << std::endl;
    if (fd < 0)
        throw TLS_error("Failed to accept", true);
    try {
        std::unique_ptr<TLS_client> client(new TLS_client(fd, _ctx.get(), sa_cli, std::make_unique<T>(_user_data), _timeout));

        _clients.push_back(std::move(client));
        // Add the client fd to the monitored fd list
        // If we don't do that, we won't know when the client send data
        FD_SET(fd, &_read_set);
    } catch (TLS_error &e) {
        Logger::error() << "Failed to accept connection: " << e.what() << std::endl;
        close(fd);
    }
}

template<class T, class UserData>
void TLS_server<T, UserData>::run()
{
    fd_set tmp;
    struct timespec timeout = {0, 0};
    sigset_t emptyset;
    sigset_t blockset;
    int ret;

    // Prepare blockset to block SIGINT
    sigemptyset(&blockset);
    sigaddset(&blockset, SIGINT);
    timeout.tv_sec = _timeout;
    FD_ZERO(&_read_set);
    // Add the server socket to be alerted when new connections are available
    FD_SET(_socket, &_read_set);
    _running = true;
    while (_running) {
        // Block SIGINT
        sigprocmask(SIG_BLOCK, &blockset, NULL);
        // Reset the dirty fd set (was modified by select(), so we reset it)
        tmp = _read_set;
        sigemptyset(&emptyset);
        // TODO if network change, connection might be lost without us noticing, do smth for that
        ret = pselect(FD_SETSIZE, &tmp, nullptr, nullptr, (_timeout > 0 ? &timeout : nullptr), &emptyset);
        // If pselect is interrupted because the server is not running anymore, just quit,
        // else throw error.
        if (!_running)
            return;
        if (ret < 0)
            throw TLS_error("Error with select", true);
        // If the server socket have pending connections, accept them
        if (FD_ISSET(_socket, &tmp))
            _accept();
        for (auto iter = _clients.begin(); iter != _clients.end(); ) {
            // if the client didn't send data, skip it
            if (!FD_ISSET((*iter)->get_fd(), &tmp)) {
                // if the client hasn't send anything for a while and timeout is set
                // we kill it. Otherwise we just skip it
                if ((*iter)->get_timeout_time() <= time(NULL) && _timeout > 0) {
                    Logger::info() << "Client timeout";
                    FD_CLR((*iter)->get_fd(), &_read_set);
                    iter = _clients.erase(iter);
                } else {
                    iter++;
                }
                continue;
            }
            // process the client request. If there is an error (or the client close connection),
            // remove the client and force close connection.
            if (!(*iter)->handle_commands()) {
                FD_CLR((*iter)->get_fd(), &_read_set);
                iter = _clients.erase(iter);
                continue;
            }
            iter++;
        }
    }
}
