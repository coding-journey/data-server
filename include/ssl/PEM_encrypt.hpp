#pragma once

#include <string>
#include <fstream>
#include <memory>
#include <functional>

#include <openssl/pem.h>

class PEM_encrypt {
    public:
        PEM_encrypt(const std::string &pkey_file);

        bool encrypt_file(std::fstream &src, std::fstream &dst);
        bool decrypt_file(std::fstream &src, std::fstream &dst);
        size_t get_in_size() const;
        size_t get_out_size() const;
    private:
        std::unique_ptr<EVP_PKEY, std::function<void(EVP_PKEY *)>> _pkey;
        size_t _in_block_size;
        size_t _out_block_size;
};
