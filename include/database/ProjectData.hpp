#pragma once

#include "database/ISerialisableData.hpp"
#include "exceptions/ContextException.hpp"

#include <limits>

class ProjectData : public ISerialisableData {
    public:
        ProjectData() = default;
        ProjectData(const std::string &owner, const std::string &project_name, const std::string &meta_file, const std::string &file, std::uint64_t timestamp, bool isApproved, std::uint8_t game_version);
        virtual ~ProjectData() = default;

        using ISerialisableData::unserialize;

        bool operator==(const ISerialisableData &other) const override;
        bool operator==(const ProjectData &other) const;

        void unserialize(std::istream &data) override;
        [[nodiscard]]
        std::string serialize() const override;
        [[nodiscard]]
        std::string getKey() const override;

        [[nodiscard]]
        const std::string &getOwner() const;
        [[nodiscard]]
        const std::string &getProjectName() const;
        [[nodiscard]]
        const std::string &getFilename() const;
        [[nodiscard]]
        const std::string &getMetaFilename() const;
        [[nodiscard]]
        std::uint64_t getTimestamp() const;
        [[nodiscard]]
        std::uint8_t getGameVersion() const;
        [[nodiscard]]
        bool isApproved() const;

        void setOwner(const std::string &owner);
        void setProjectName(const std::string &project_name);
        void setFilename(const std::string &filename);
        void setMetaFilename(const std::string &meta_filename);
        void setTimestamp(std::uint64_t timestamp);
        void setGameVersion(std::uint8_t game_version);
        void setApproved(bool approved);

        [[nodiscard]]
        const std::string getClassName() const override {
            return "ProjectData";
        };

        static const std::uint8_t current_version;
        static const std::uint8_t max_string_size = std::numeric_limits<std::uint8_t>::max();

        friend std::ostream& operator<<(std::ostream& os, const ProjectData& dt);
    private:
        std::string _owner;
        std::string _project_name;
        std::string _meta_file;
        std::string _file;
        std::uint64_t _timestamp = 0;
        bool _isApproved = false;
        std::uint8_t _game_version = 1;

        void migrate(std::uint8_t version, std::istream &data) override;

        [[nodiscard]]
        bool is_valid() const;
};
