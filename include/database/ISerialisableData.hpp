#pragma once

#include "exceptions/ContextException.hpp"

#include <istream>
#include <sstream>
#include <string>
#include <vector>

class ISerialisable {
    public:
        [[nodiscard]]
        virtual std::string serialize() const = 0;
        virtual void unserialize(std::istream &data) = 0;

        void unserialize(const std::string &data)
        {
            std::stringstream ss(data);

            unserialize(ss);
        }

        [[nodiscard]]
        virtual const std::string getClassName() const = 0;

    protected:
        std::string extract_chars(std::istream &data, std::uint32_t nb)
        {
            std::vector<char> buff(nb);

            data.read(buff.data(), nb);
            if (data.gcount() != nb)
                throw NewContextException(getNotSerializedExceptionMessage());
            return std::string(buff.data(), nb);
        }

        [[nodiscard]]
        const std::string getNotSerializedExceptionMessage() const
        {
            return "Not a serialized " + getClassName();
        }

        [[nodiscard]]
        const std::string getCantSerializeExceptionMessage() const
        {
            return getClassName() + " : Can't serialize";
        }
};

class ISerialisableData : public ISerialisable {
    public:
        [[nodiscard]]
        virtual std::string getKey() const = 0;
        virtual bool operator==(const ISerialisableData &other) const = 0;

        template <typename T>
        static T hton(T in)
        {
            if (isBigEndian())
                return in; // LCOV_EXCL_LINE
            return reverse_endianess<T>(in);
        }

        template <typename T>
        static T reverse_endianess(T in)
        {
            T out = 0;

            for (unsigned long i = 0; i < sizeof(T); i++) {
                out = (out << 8) + ((in >> (8 * i)) & 0xFF);
            }
            return out;
        }

        static bool isBigEndian()
        {
            constexpr static int i = 1;

            return !*((char *)&i);
        }

    protected:
        virtual void migrate(std::uint8_t version, std::istream &input) = 0;
};
