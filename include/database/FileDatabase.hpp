#pragma once

#include "database/IDatabase.hpp"
#include "exceptions/ContextException.hpp"
#include "logger/Logger.hpp"

#include <filesystem>
#include <fstream>
#include <functional>

// TODO add DB versions somewhere, check it, and if version outdated, update file
template <class Data,
          std::enable_if_t<std::is_base_of<ISerialisableData, Data>::value>* = nullptr>
class FileDatabase : public IDatabase {
    public:
        FileDatabase(const std::string &databases_dir) :
            _databases_dir(databases_dir)
        {
            std::error_code ec;

            std::filesystem::create_directories(databases_dir, ec);
            if (ec)
                throw NewContextException("Failed to create database directory : " + ec.message());
        };

        virtual ~FileDatabase() {
            cleanup();
        };

        FileDatabase(const FileDatabase &) = delete;
        FileDatabase(FileDatabase &&) = delete;

        FileDatabase &operator=(const FileDatabase &) = delete;
        FileDatabase &operator=(FileDatabase &&) = delete;

        void select_database(const std::string &database_name) override {
            std::fstream db;

            cleanup();
            _filename = _databases_dir + "/" + database_name + ".db";
            db = _openFun(_filename, std::ios::in | std::ios::binary);
            if (db.is_open()) {
                try {
                    read_database(db);
                } catch (const std::exception &e) {
                    std::string back = _filename;

                    Logger::error() << "Failed to read database " << database_name <<  " : " << e.what() << std::endl;
                    _filename = ""; // Avoid writting to the db during cleanup
                    cleanup();
                    _filename = back;
                    std::filesystem::rename(_filename, _filename + ".back");
                }
            } else if (std::filesystem::exists(_filename)) {
                Logger::error() << "Failed to open database " << database_name <<  " : " << "Decryption error" << std::endl;
                std::filesystem::rename(_filename, _filename + ".back");
            }
        };

        const std::unordered_map<std::string, std::shared_ptr<ISerialisableData>> list() const override {
            std::unordered_map<std::string, std::shared_ptr<ISerialisableData>> vec;

            if (_filename.empty())
                throw NewContextException("No db selected !");
            std::transform(_table.begin(), _table.end(), std::inserter(vec, vec.end()), [](const auto &p) {
                return std::pair(p.first, p.second);
            });
            return vec;
        };

        const std::unordered_map<std::string, std::shared_ptr<Data>> &list_files() const {
            if (_filename.empty())
                throw NewContextException("No db selected !");
            return _table;
        };

        void insert(std::shared_ptr<ISerialisableData> data, bool overide = true) override {
            std::string id = data->getKey();
            Data *ptr = dynamic_cast<Data *>(data.get());

            if (_filename.empty())
                throw NewContextException("No db selected !");
            if (!ptr)
                throw NewContextException("Dynamic cast to FileDatabase's templated type failed");
            if (_table.count(id) != 0 && !overide)
                return;
            _table[id] = std::shared_ptr<Data>(ptr);
        };

        bool remove(const std::string &key) override {
            if (_filename.empty())
                throw NewContextException("No db selected !");
            return _table.erase(key) != 0;
        };

        void insert(std::shared_ptr<Data> data, bool overide = true) {
            std::string id = data->getKey();

            if (_filename.empty())
                throw NewContextException("No db selected !");
            if (_table.count(id) != 0 && !overide)
                return;
            _table[id] = data;
        };

        std::shared_ptr<ISerialisableData> querry(const std::string &id) const override {
            if (_filename.empty())
                throw NewContextException("No db selected !");
            try {
                return _table.at(id);
            } catch (std::out_of_range &) {
                return nullptr;
            }
        };

        std::shared_ptr<ISerialisableData> querry(const std::function<bool (const std::shared_ptr<ISerialisableData> &)> &compare) const override {
            if (_filename.empty())
                throw NewContextException("No db selected !");
            for (const auto &iter : _table) {
                if (compare(iter.second))
                    return iter.second;
            }
            return nullptr;
        };

        std::shared_ptr<Data> querry_file(const std::string &id) const {
            if (_filename.empty())
                throw NewContextException("No db selected !");
            try {
                return _table.at(id);
            } catch (std::out_of_range &) {
                return nullptr;
            }
        };

        std::shared_ptr<Data> querry_file(const std::function<bool (const std::shared_ptr<ISerialisableData> &)> &compare) const {
            if (_filename.empty())
                throw NewContextException("No db selected !");
            for (const auto &iter : _table) {
                if (compare(iter.second))
                    return iter.second;
            }
            return nullptr;
        };

        std::uint64_t size() const override {
            if (_filename.empty())
                throw NewContextException("No db selected !");
            return _table.size();
        }

        void drop_database() override
        {
            if (_filename.empty())
                throw NewContextException("No db selected !");
            _table.clear();
            cleanup();
            std::filesystem::remove(_filename);
        }

        void setOpenCallback(const std::function<std::fstream (const std::string &filename, std::ios::openmode mode)> &callback) override {
            _openFun = callback;
        };

        void setCloseCallback(const std::function<void (const std::string &filename, std::fstream &stream)> &callback) override {
            _closeFun = callback;
        };

    private:
        std::unordered_map<std::string, std::shared_ptr<Data>> _table;
        std::string _databases_dir;
        std::string _filename;
        static const std::uint16_t _magic = 0x6942;

        // Called twice : before reading the db, and before writting, check the mode
        // to see if it's in or out;
        std::function<std::fstream (const std::string &filename, std::ios::openmode mode)>
        _openFun = [] (const std::string &filename, std::ios::openmode mode) -> std::fstream {
            return std::fstream(filename, mode);
        };

        std::function<void(const std::string &filename, std::fstream &stream)>
        _closeFun = [] (const std::string &, std::fstream &db) {
            db.close();
        };

        void cleanup() {
            if (!_filename.empty()) {
                std::fstream db;
                std::uint16_t magic = ISerialisableData::hton(_magic);

                db.close();
                db.clear();
                db = _openFun(_filename, std::ios::out | std::ios::trunc | std::ios::binary);
                if (!db.is_open())
                    throw NewContextException("Failed to open db file for writing : " + _filename);
                for (auto &iter : _table) {
                    try {
                        if (iter.second) {
                            std::string data = iter.second->serialize(); // May throw

                            db.write((char *)&magic, 2);
                            db.write(data.c_str(), data.size());
                        }
                    } catch (const std::exception &e) {
                        Logger::error() << "Failed to save a data row : " << e.what() << std::endl;
                    }
                }
                _closeFun(_filename, db);
            }
            _table.clear();
        };

        void read_database(std::fstream &db) {
            db.clear();
            db.seekg(0);
            db.peek(); // Update the stream to trigger EOF if the file is empty
            while (db.good()) { // Check EOF or error
                std::array<char, 2> buff = {};
                std::shared_ptr<Data> data;

                db.read(buff.data(), 2);
                if (db.gcount() != 2 || ISerialisableData::hton(*(std::uint16_t *)buff.data()) != _magic)
                    throw NewContextException("Malformed db file: " + _filename);
                data.reset(new Data());
                try {
                    data->unserialize(db);
                } catch (const std::exception &e) {
                    throw NewContextException("Malformed db entry: " + _filename);
                }
                _table[data->getKey()] = data;
                db.peek(); // Update the stream to trigger EOF if the file is empty
            }
        };
};
