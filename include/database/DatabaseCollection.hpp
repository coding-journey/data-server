#pragma once

#include "database/IDatabase.hpp"

#include <functional>
#include <memory>

template<class T,
         std::enable_if_t<std::is_base_of<IDatabase, T>::value>* = nullptr>
class DatabaseCollection {
    public:
        DatabaseCollection(std::string db_dir):
            _db_dir(std::move(db_dir))
        {}

        std::shared_ptr<T> createDatabase(const std::string &name) {
            std::weak_ptr<T> &item = _dbs[name];

            if (!item.lock()) {
                auto db = std::make_shared<T>(_db_dir);

                if (_openFun)
                    db->setOpenCallback(_openFun);
                if (_closeFun)
                    db->setCloseCallback(_closeFun);
                db->select_database(name);
                item = db;
                return db;
            }
            return std::shared_ptr(item);
        };

        std::shared_ptr<const T> getDatabase(const std::string &name) {
            return createDatabase(name);
        };

        void setOpenCallback(const std::function<std::fstream (const std::string &filename, std::ios::openmode mode)> &callback) {
            _openFun = callback;
        };

        void setCloseCallback(const std::function<void (const std::string &filename, std::fstream &stream)> &callback) {
            _closeFun = callback;
        };

    private:
        std::unordered_map<std::string, std::weak_ptr<T>> _dbs;
        std::string _db_dir;
        std::function<std::fstream (const std::string &filename, std::ios::openmode mode)>
        _openFun;
        std::function<void(const std::string &filename, std::fstream &stream)>
        _closeFun;
};
