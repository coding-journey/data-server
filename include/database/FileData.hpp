#pragma once

#include "database/ISerialisableData.hpp"
#include "exceptions/ContextException.hpp"

#include <limits>
#include <sstream>

class FileData : public ISerialisableData {
    public:
        FileData() = default;
        template<typename CustomDataType,
                  std::enable_if_t<std::is_base_of<ISerialisable, CustomDataType>::value>* = nullptr>
        FileData(const std::string &filename, std::uint64_t timestamp, const std::string &content, const CustomDataType &custom_data) :
            FileData(filename, timestamp, content, custom_data.serialize()) {}
        FileData(std::string filename, std::uint64_t timestamp, std::string content, std::string custom_data = "");
        virtual ~FileData() = default;

        static const std::uint8_t current_version;

        bool operator==(const ISerialisableData &other) const override;
        bool operator==(const FileData &other) const;

        void unserialize(std::istream &data) override;
        [[nodiscard]]
        std::string serialize() const override;
        [[nodiscard]]
        std::string getKey() const override;
        void migrate(std::uint8_t version, std::istream &data) override;

        [[nodiscard]]
        std::uint64_t getTimestamp() const;
        [[nodiscard]]
        const std::string &getContent() const;
        [[nodiscard]]
        const std::string &getFilename() const;

        template<typename CustomDataType,
                  std::enable_if_t<std::is_base_of<ISerialisable, CustomDataType>::value>* = nullptr>
        CustomDataType getCustomData() const {
              CustomDataType res;
              std::stringstream ss(_custom_data);

              res.unserialize(ss);
              return res;
        }

        [[nodiscard]]
        const std::string getClassName() const override {
            return "FileData";
        };

        static const std::uint16_t max_filename = std::numeric_limits<std::uint8_t>::max() + 1;
        static const std::uint32_t max_content = std::numeric_limits<std::uint32_t>::max();

        friend std::ostream& operator<<(std::ostream& os, const FileData& dt);
    private:
        std::string _filename;
        std::string _content;
        std::string _custom_data;
        std::uint64_t _timestamp = 0;

        [[nodiscard]]
        bool is_valid() const;
};
