#pragma once

#include "database/ISerialisableData.hpp"

#include <functional>
#include <memory>
#include <string>
#include <type_traits>
#include <unordered_map> // TODO reserve space in ordered map when using std::transform

class IDatabase {
    public:
        virtual void select_database(const std::string &database_name) = 0;
        virtual void drop_database() = 0; // Trhow if no db selected

        [[nodiscard]]
        virtual std::uint64_t size() const = 0; // Throw if no db selected
        [[nodiscard]]
        virtual const std::unordered_map<std::string, std::shared_ptr<ISerialisableData>> list() const = 0; // Throw if no db selected

        virtual bool remove(const std::string &key) = 0; // Throw if no db selected
        virtual void insert(std::shared_ptr<ISerialisableData> data, bool overide = true) = 0; // Throw if no db selected

        [[nodiscard]]
        virtual std::shared_ptr<ISerialisableData> querry(const std::string &id) const = 0; // Throw if no db selected
        [[nodiscard]]
        virtual std::shared_ptr<ISerialisableData> querry(const std::function<bool (const std::shared_ptr<ISerialisableData> &)> &compare) const = 0; // Throw if no db selected

        virtual void setOpenCallback(const std::function<std::fstream (const std::string &filename, std::ios::openmode mode)> &callback) = 0;
        virtual void setCloseCallback(const std::function<void (const std::string &filename, std::fstream &stream)> &callback) = 0;
};
