#pragma once

#include <limits>

#include "database/ISerialisableData.hpp"

class UserData : public ISerialisableData {
    public:
        UserData() = default;
        UserData(std::string email, std::string username, std::string password_hash, std::string token);
        virtual ~UserData() = default;

        using ISerialisableData::unserialize;
        static const std::uint8_t current_version;

        [[nodiscard]]
        std::string getKey() const override;
        bool operator==(const ISerialisableData &other) const override;
        bool operator==(const UserData &other) const;

        [[nodiscard]]
        std::string serialize() const override;
        void unserialize(std::istream &data) override;
        void migrate(std::uint8_t version, std::istream &input) override;

        static bool checkPassword(const std::string &password);
        static std::string generateToken();
        static std::string hashPassword(const std::string &password);
        static bool verifyPassword(const std::string &password, const std::string &hash);
        bool verifyPassword(const std::string &password);

        static const std::uint8_t max_username = std::numeric_limits<std::uint8_t>::max();
        static const std::uint8_t max_password = std::numeric_limits<std::uint8_t>::max();
        static const std::uint8_t min_password = 8;

    private:
        std::string _email;
        std::string _username;
        std::string _password_hash;
        std::string _token;

        [[nodiscard]]
        bool isValid() const;

        // Hidding theses static utility functions
        using ISerialisableData::hton;
        using ISerialisableData::reverse_endianess;
        using ISerialisableData::isBigEndian;

    public:
        [[nodiscard]]
        const std::string getClassName() const override {
            return "UserData";
        }

        [[nodiscard]]
        std::string getUsername() const {
            return _username;
        }

        [[nodiscard]]
        std::string getEmail() const {
            return _email;
        }

        [[nodiscard]]
        std::string getHashedPassword() const {
            return _password_hash;
        }

        [[nodiscard]]
        std::string getToken() const {
            return _token;
        }

        void setToken(const std::string &token) {
            _token = token;
        }
};
