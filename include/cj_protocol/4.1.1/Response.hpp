#pragma once

#include <memory>
#include <string>

namespace CJ_Protocol_4_1_1 {
    struct Response {
        public:
            Response(std::uint8_t status, std::uint8_t command, std::uint16_t size, std::string data);
            Response(std::uint8_t status, std::uint8_t command);
            virtual ~Response() = default;

            [[nodiscard]]
            std::string toString() const;

            [[nodiscard]]
            virtual std::string display() const;

            bool operator==(const Response &o);

        public:
            std::uint8_t _status_code;
            std::uint8_t _command_code;
            std::uint16_t _data_size;
            std::string _data;
    };
} // namespace CJ_Protocol
