#pragma once

#include "sqlite_orm/database_schema.hpp"

#include <array>
#include <string>

namespace CJ_Protocol_4_1_1 {
    class ICommandData {
        public:
            virtual ~ICommandData() = default;

            [[nodiscard]]
            bool needLogin() const;
            [[nodiscard]]
            std::uint16_t getSize() const;

            [[nodiscard]]
            virtual std::string display() const = 0;

        protected:
            ICommandData(std::uint16_t size, const std::string &data);

            void setNeedLogin(bool value);
            void setSize(std::uint16_t value);

        private:
            std::uint16_t _size;
            bool _needLogin;
    };

    class NoData : public ICommandData {
        public:
            NoData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;
    };

    class NoDataLogin : public ICommandData {
        public:
            NoDataLogin(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;
    };

    class RequestVersionData : public NoData {
        public:
            RequestVersionData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::uint16_t getVersion() const noexcept;
            [[nodiscard]]
            std::uint8_t getMajor() const noexcept;
            [[nodiscard]]
            std::uint8_t getMinor() const noexcept;
            [[nodiscard]]
            std::uint8_t getPatch() const noexcept;
        private:
            std::uint16_t _version;
            std::uint8_t _major;
            std::uint8_t _minor;
            std::uint8_t _patch;
    };

    class DeleteAccountData : public ICommandData {
        public:
            DeleteAccountData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::string getToken() const;
        private:
            std::string _token;
    };

    class RegisterData : public NoData {
        public:
            RegisterData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::string getUsername() const noexcept;
            [[nodiscard]]
            std::string getPassword() const noexcept;
            [[nodiscard]]
            std::string getEmail() const noexcept;
        private:
            std::string _email;
            std::string _username;
            std::string _password;
    };

    class LoginData : public NoData {
        public:
            LoginData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::string getToken() const noexcept;
            [[nodiscard]]
            std::string getUsername() const noexcept;
            [[nodiscard]]
            std::string getPassword() const noexcept;
            [[nodiscard]]
            bool isToken() const noexcept;
        private:
            std::string _token;
            std::string _username;
            std::string _password;
            bool _is_token = false;
    };

    class DownloadData : public ICommandData {
        public:
            DownloadData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::string getFilename() const;
        private:
            std::string _filename;
    };

    class UploadData : public ICommandData {
        public:
            UploadData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::string getFilename() const;
            [[nodiscard]]
            std::uint64_t getTimestamp() const;
            [[nodiscard]]
            std::string getContent() const;
            [[nodiscard]]
            bool isPublic() const;

            void unfragment(const std::string &data);
        private:
            std::string _filename;
            std::uint64_t _timestamp;
            std::string _content;
            bool _public;
    };

    class ListPublicFilesData : public NoData {
        public:
            ListPublicFilesData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getUsername() const;
        private:
            std::string _username;
    };

    class DownloadPublicFileData : public NoData {
        public:
            DownloadPublicFileData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getUsername() const;
            [[nodiscard]]
            const std::string getFilename() const;
        private:
            std::string _username;
            std::string _filename;
    };

    class GetCategoriesData : public NoData {
        public:
            GetCategoriesData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getLanguage() const;

        private:
            std::string _lang;
    };

    class GetFunctionsData : public NoData {
        public:
            GetFunctionsData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getLanguage() const;
            [[nodiscard]]
            const std::string getCategory() const;

        private:
            std::string _lang;
            std::string _categ;
    };

    class GetFunctionData : public NoData {
        public:
            GetFunctionData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getLanguage() const;
            [[nodiscard]]
            const std::string getName() const;

        private:
            std::string _lang;
            std::string _name;
    };

    class ReportCrashData : public NoData {
        public:
            ReportCrashData(std::uint16_t size, const std::string &data);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getStacktrace() const;
            [[nodiscard]]
            const std::string getMessage() const;
            [[nodiscard]]
            const std::string getSceneName() const;
            [[nodiscard]]
            SoftwareType getSoftware() const;
            [[nodiscard]]
            OperatingSystem getOS() const;
            [[nodiscard]]
            float getRAM() const;
            [[nodiscard]]
            const std::string getUUID() const;

        private:
            std::string _stacktrace;
            std::string _message;
            std::string _scene_name;
            SoftwareType _soft;
            OperatingSystem _os;
            float _ram;
            std::string _uuid;
    };
}
