#include "exceptions/ContextException.hpp"

class CJProtocolException : public ContextException {
    protected:
        CJProtocolException(const std::string &file, int line, const std::string function, const std::string &msg, std::exception_ptr nested_ex = std::current_exception());
        virtual ~CJProtocolException() = default;
};

class CJProtocolMismatch : public CJProtocolException {
    public:
        CJProtocolMismatch(const std::string &file, int line, const std::string function, const std::string &msg, std::exception_ptr nested_ex = std::current_exceptio\
n());
        virtual ~CJProtocolMismatch() = default;
};

class CJProtocolMissingBytes : public CJProtocolException {
    public:
        CJProtocolMissingBytes(const std::string &file, int line, const std::string function, const std::string &msg, std::exception_ptr nested_ex = std::current_exceptio\
n());
        virtual ~CJProtocolMissingBytes() = default;
};
