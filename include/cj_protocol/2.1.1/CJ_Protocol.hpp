#pragma once

#include "cj_protocol/2.1.1/Command.hpp"
#include "cj_protocol/2.1.1/PseudoData.hpp"
#include "cj_protocol/2.1.1/Response.hpp"
#include "cj_protocol/2.1.1/ResponseData.hpp"
#include "cj_protocol/DatabasesManager.hpp"

#include "database/DatabaseCollection.hpp"
#include "database/FileData.hpp"
#include "database/FileDatabase.hpp"

#include "ssl/IProtocol.hpp"

class CJ_ProtocolManager;

namespace CJ_Protocol_2_1_1 {
    enum CommandCode {
        VERSION                     = 0x00,
        REQUEST_SERVER_VERSION      = 0x01,
        LOGIN                       = 0x04,
        LIST_FILES                  = 0x06,
        DOWNLOAD_FILE               = 0x07,
        UPLOAD_FILE                 = 0x08,
        // TODO DELETE_FILE
        FRAGMENTED_DATA             = 0x0F,
        LIST_USERS                  = 0x10,
        GET_USER_ID                 = 0x11,
        LIST_PUBLIC_FILES           = 0x14,
        DOWNLOAD_PUBLIC_FILE        = 0x15,
        GET_LANGUAGES               = 0x17,
        GET_CATEGORIES              = 0x18,
        GET_FUNCTIONS_NAMES         = 0x19,
        GET_FUNCTIONS               = 0x1A,
        GET_FUNCTION                = 0x1B,
        REPORT_BUG                  = 0x1E,
    }; // 0-31

    enum StatusCode {
        STATUS_OK                       = 0x00,
        ERR_PARAMETER_ERROR             = 0x01,
        ERR_UNKNOWN_COMMAND             = 0x02,
        ERR_VERSION                     = 0x04,
        ERR_ALREADY_LOGIN               = 0x06,
        ERR_MISSING_LOGIN               = 0x07,
        ERR_NO_SUCH_FILE                = 0x0A,
        ERR_TOO_MANY_FILES              = 0x0C,
        ERR_ALREADY_FRAGMENTED_MODE     = 0x10,
        ERR_NOT_FRAGMENTED_MODE         = 0x11,
        ERR_MISSING_BLOCKS              = 0x13,
        ERR_UNKNOWN_BLOCK               = 0x14,
        ERR_UNKNOWN_PSEUDO              = 0x20,
        ERR_UNKNOWN_STEAM_ID            = 0x21, // TODO
        ERR_UNKNOWN_LANGUAGE            = 0x30,
        ERR_UNKNOWN_CATEGORY            = 0x31,
        ERR_UNKNOWN_FUNCTION            = 0x32,
        ERR_PROTOCOL_MISSMATCH          = 0xFE,
        ERR_INTERNAL_SERVER_EROR        = 0xFF,
    }; // 0-255

    class Protocol : public IProtocol {
        public:
            Protocol(CJ_ProtocolManager *m);

            std::string handle_command(const std::string &command, std::string &remaining) override;
            [[nodiscard]]
            std::uint64_t getVersion() const override;
            std::string handle_command(const std::string &command, bool unfragmented);

            static const std::uint8_t major_version; // 0-7
            static const std::uint8_t minor_version; // 0-31
            static const std::uint8_t patch_version; // 0-255

            static const std::uint8_t steam_id_size;
            static const std::uint16_t fragmented_size;

        private:
            struct Fragmented {
                std::unordered_map<std::uint16_t /*id*/, std::string /*content*/> blocks;
                Command cmd;
                std::uint16_t total;
                bool active = false;
            };

            Response process_command(Command &cmd);
            Response create_response(const IResponseData &data, CommandCode code, StatusCode status = StatusCode::STATUS_OK);
            Response exec_partial_fragmented(Command &cmd, const std::string &data);
            bool set_fragmented_get(std::string data);
            bool set_fragmented_put(Command &cmd);

            const std::uint16_t _version;

            CJ_ProtocolManager *_m;
            DatabasesManager &_dbs;
            std::shared_ptr<FileDatabase<FileData>> _db;
            std::string _user;
            Fragmented _get;
            Fragmented _put;
    };
} // CJ_Protocol
