#pragma once

#include "sqlite_orm/database_schema.hpp"

#include <array>
#include <string>

namespace CJ_Protocol_2_1_1 {
    class ICommandData {
        public:
            virtual ~ICommandData() = default;

            [[nodiscard]]
            bool needLogin() const;
            [[nodiscard]]
            bool isFragmented() const;
            [[nodiscard]]
            std::uint16_t getSize() const;

        protected:
            ICommandData(std::uint16_t size, const std::string &data, bool unfragmented);

            std::uint16_t _size;
            bool _fragmented;
            bool _needLogin;
    };

    class NoData : public ICommandData {
        public:
            NoData(std::uint16_t size, const std::string &data, bool unfragmented);
    };

    class NoDataLogin : public ICommandData {
        public:
            NoDataLogin(std::uint16_t size, const std::string &data, bool unfragmented);
    };

    class RequestVersionData : public ICommandData {
        public:
            RequestVersionData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::uint16_t getVersion() const;
            [[nodiscard]]
            std::uint8_t getMajor() const;
            [[nodiscard]]
            std::uint8_t getMinor() const;
            [[nodiscard]]
            std::uint8_t getPatch() const;
        private:
            std::uint16_t _version;
            std::uint8_t _major;
            std::uint8_t _minor;
            std::uint8_t _patch;
    };

    class LoginData : public ICommandData {
        public:
            LoginData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            const char *getSteamID() const;
            [[nodiscard]]
            std::string getPseudo() const;
        private:
            std::array<char, 17> _steamid = {};
            std::string _pseudo;
    };

    class DownloadData : public ICommandData {
        public:
            DownloadData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string getFilename() const;
        private:
            std::string _filename;
    };

    class UploadData : public ICommandData {
        public:
            UploadData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string getFilename() const;
            [[nodiscard]]
            std::uint64_t getTimestamp() const;
            [[nodiscard]]
            std::string getContent() const;
            [[nodiscard]]
            bool hasFragmentedContent() const;
            [[nodiscard]]
            bool isPublic() const;

            void unfragment(const std::string &data);
        private:
            std::string _filename;
            std::uint64_t _timestamp;
            std::string _content;
            bool _fragmentedContent;
            bool _public;
    };

    class FragmentedData : public ICommandData {
        public:
            FragmentedData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::uint16_t getBlockID() const;
            [[nodiscard]]
            bool isModeGet() const;
            [[nodiscard]]
            std::string getData() const;
        private:
            std::uint16_t _blockID;
            bool _mode;
            std::string _data;
    };

    class GetUserIdData : public ICommandData {
        public:
            GetUserIdData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string getPseudo() const;
        private:
            std::string _pseudo;
    };

    class ListPublicFilesData : public ICommandData {
        public:
            ListPublicFilesData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            const std::string getSteamID() const;
        private:
            std::array<char, 17> _steamid = {};
    };

    class DownloadPublicFileData : public ICommandData {
        public:
            DownloadPublicFileData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            const std::string getSteamID() const;
            [[nodiscard]]
            const std::string getFilename() const;
        private:
            std::array<char, 17> _steamid = {};
            std::string _filename;
    };

    class GetCategoriesData : public ICommandData {
        public:
            GetCategoriesData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            const std::string getLanguage() const;

        private:
            std::string _lang;
    };

    class GetFunctionsData : public ICommandData {
        public:
            GetFunctionsData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            const std::string getLanguage() const;
            [[nodiscard]]
            const std::string getCategory() const;

        private:
            std::string _lang;
            std::string _categ;
    };

    class GetFunctionData : public ICommandData {
        public:
            GetFunctionData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            const std::string getLanguage() const;
            [[nodiscard]]
            const std::string getName() const;

        private:
            std::string _lang;
            std::string _name;
    };

    class ReportBugData : public ICommandData {
        public:
            ReportBugData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            const std::string getStacktrace() const;
            [[nodiscard]]
            const std::string getMessage() const;
            [[nodiscard]]
            SoftwareType getSoftware() const;
            [[nodiscard]]
            OperatingSystem getOS() const;
            [[nodiscard]]
            float getRAM() const;
            [[nodiscard]]
            const std::string getUUID() const;

        private:
            std::string _stacktrace;
            std::string _message;
            SoftwareType _soft;
            OperatingSystem _os;
            float _ram;
            std::string _uuid = {};
    };
}
