#pragma once

#include <memory>
#include <string>
#include <vector>

#include "cj_protocol/2.1.1/PseudoData.hpp"
#include "cj_protocol/WikiDatabase.hpp"

#include "database/FileData.hpp"

namespace CJ_Protocol_2_1_1 {
    class IResponseData {
        public:
            virtual ~IResponseData() = default;

            [[nodiscard]]
            std::string toString() const;
            void setData(const std::string &str);
            [[nodiscard]]
            std::uint16_t getSize() const;
            [[nodiscard]]
            bool isFragmented() const;

        private:
            std::string _data;
            bool _fragmented = false;
    };

    class VersionResponseData : public IResponseData {
        public:
            VersionResponseData(std::uint16_t version);
            VersionResponseData(std::uint8_t major, std::uint8_t minor, std::uint8_t patch);
    };

    class ListFilesResponseData : public IResponseData {
        public:
            ListFilesResponseData(const std::vector<std::shared_ptr<const FileData>> &files);
    };

    class DownloadResponseData : public IResponseData {
        public:
            DownloadResponseData(const std::shared_ptr<const FileData> &file);
            DownloadResponseData(const std::shared_ptr<const FileData> &file, std::uint16_t blocks);
    };

    class FragmentedResponseData : public IResponseData {
        public:
            FragmentedResponseData(std::uint16_t blockID, const std::string &data);
    };

    class ListUsersResponseData : public IResponseData {
        public:
            ListUsersResponseData(const std::vector<std::shared_ptr<PseudoData>> &data);
    };

    class GetUserIdResponseData : public IResponseData {
        public:
            GetUserIdResponseData(const std::string &steam_id);
    };

    template <class T>
    class ListResponseData : public IResponseData {
        public:
            ListResponseData(std::uint16_t version, const T &values);
    };

    template <>
    class ListResponseData<WikiDatabase::languages_t> : public IResponseData {
        public:
            ListResponseData(std::uint16_t version, const WikiDatabase::languages_t &languages);
    };

    template <>
    class ListResponseData<WikiDatabase::categories_t> : public IResponseData {
        public:
            ListResponseData(std::uint16_t version, const WikiDatabase::categories_t &categories);
    };

    template <>
    class ListResponseData<WikiDatabase::functions_t> : public IResponseData {
        public:
            ListResponseData(std::uint16_t version, const WikiDatabase::functions_t &functions);
    };

    class FunctionsResponseData : public IResponseData {
        public:
            FunctionsResponseData(std::uint16_t version, const WikiDatabase::functions_t &functions);
    };

    class FunctionResponseData : public IResponseData {
        public:
            FunctionResponseData(std::uint16_t version, const std::string &fun);
    };
}
