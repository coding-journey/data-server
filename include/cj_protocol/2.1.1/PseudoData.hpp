#pragma once

#include <limits>

#include "database/ISerialisableData.hpp"

namespace CJ_Protocol_2_1_1 {
    class PseudoData : public ISerialisableData {
        public:
            PseudoData() = default;
            PseudoData(std::string pseudo, std::string steam_id);
            virtual ~PseudoData() = default;

            [[nodiscard]]
            std::string getKey() const override;
            bool operator==(const ISerialisableData &other) const override;
            bool operator==(const PseudoData &other) const;

            [[nodiscard]]
            std::string serialize() const override;
            void unserialize(std::istream &data) override;
            void migrate(std::uint8_t version, std::istream &data) override;

            [[nodiscard]]
            const std::string getClassName() const override {
                return "PseudoData";
            };

            [[nodiscard]]
            std::string getPseudo() const {
                return _pseudo;
            };

            [[nodiscard]]
            std::string getSteamID() const {
                return _steam_id;
            };

            static const std::uint8_t max_pseudo = std::numeric_limits<std::uint8_t>::max();

        private:
            std::string _pseudo;
            std::string _steam_id;

            [[nodiscard]]
            bool isValid() const;
    };
}
