#pragma once
// TODO move this file in it's own namespace / folder

#include <fstream>

class TMP_file : public std::fstream {
    public:
        // If del == true, we will open then delete the file.
        // The file will only be accessible through this object.
        // Only set del to true if you don't want to reuse the file afterwards
        // NOTE: The file will still be deleted when the object is destroyed if you don't call keep();
        TMP_file(const std::string &filename = TMP_file::temp_filename(), bool del = true);
        virtual ~TMP_file();

        const std::string &get_filename() const;

        // Go back to the begining of the file
        void rewind();
        // Set if we have to delete the file or not when we delete this object
        void keep(bool state = true);
        // Rename this tmp file to a real file. This will set default permissions on the new file
        bool rename(const std::string &name);

        // Generate a filename for a temp file
        static std::string temp_filename();
    private:
        std::string _filename;
        bool _del;
};
