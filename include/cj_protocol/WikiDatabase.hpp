#pragma once

#include <filesystem>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

class WikiDatabase {
    public:
        WikiDatabase(const std::string &db_dir);

        using functions_t = std::vector<std::pair<std::string, std::string>>;
        using categories_t = std::unordered_map<std::string, functions_t>;
        using languages_t = std::unordered_map<std::string, categories_t>;

        [[nodiscard]]
        const languages_t &getLanguages() const;
        [[nodiscard]]
        const categories_t &getCategories(const std::string &lang) const;
        [[nodiscard]]
        const functions_t &getFunctions(const std::string &lang, const std::string &category) const;
        [[nodiscard]]
        std::string getFunction(const std::string &lang, const std::string &name) const;
        [[nodiscard]]
        std::uint16_t getVersion() const;

    private:
        std::string m_root_dir;
        languages_t m_functions;
        std::uint16_t m_version = 0;

        void loadLanguage(const std::filesystem::directory_entry &e);
        void loadCategory(const std::string &lang, const std::filesystem::directory_entry &e);
};

class LanguageNotFoundException : std::exception {
    public:
        LanguageNotFoundException(const std::string &name);

        const char *what() const noexcept override;
    private:
        std::string m_msg;
};

class CategoryNotFoundException : std::exception {
    public:
        CategoryNotFoundException(const std::string &name);

        const char *what() const noexcept override;
    private:
        std::string m_msg;
};


class FunctionNotFoundException : std::exception {
    public:
        FunctionNotFoundException(const std::string &name);

        const char *what() const noexcept override;
    private:
        std::string m_msg;
};
