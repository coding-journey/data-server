#pragma once

#include "cj_protocol/UserData.hpp"
#include "cj_protocol/WikiDatabase.hpp"

#include "sqlite_orm/database_schema.hpp"

#include "database/DatabaseCollection.hpp"
#include "database/FileData.hpp"
#include "database/ProjectData.hpp"
#include "database/FileDatabase.hpp"

class DatabasesManager {
    public:
        DatabasesManager(std::string rsa, const std::string &db_dir);

        std::unique_ptr<FileDatabase<UserData>> &getUsersDB();
        std::unique_ptr<FileDatabase<ProjectData>> &getProjectsDB();
        std::unique_ptr<DatabaseCollection<FileDatabase<FileData>>> &getDatabases();
        std::unique_ptr<SQLite_database> &getCrashReportDB();
        std::unique_ptr<WikiDatabase> &getWikiDB();

        static constexpr const char * const user_db_name = "PlayerPseudoCache";
        static constexpr const char * const project_db_name = "GameProjectsCache";
        static constexpr const char * const crashreport_db_name = "CrashReports";
    private:
        std::unique_ptr<FileDatabase<ProjectData>> _projects_db;
        std::unique_ptr<FileDatabase<UserData>> _users_db;
        std::unique_ptr<DatabaseCollection<FileDatabase<FileData>>> _dbs;
        std::function<std::fstream (const std::string &filename, std::ios::openmode mode)> _openFun;
        std::function<void(const std::string &filename, std::fstream &stream)> _closeFun;
        std::string _rsa;
        std::unique_ptr<SQLite_database> _crash_report_db;
        std::unique_ptr<WikiDatabase> _wiki_db;
        std::shared_ptr<FileDatabase<FileData>> _cj_db;
};
