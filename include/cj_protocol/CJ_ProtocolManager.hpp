#pragma once

#include "cj_protocol/DatabasesManager.hpp"
#include "cj_protocol/UserData.hpp"

#include "ssl/IProtocol.hpp"

#include <memory>

class CJ_ProtocolManager : public IProtocol {
    public:
        CJ_ProtocolManager(DatabasesManager &dbs);

        std::string handle_command(const std::string &command, std::string &remaining) override;
        std::uint16_t change_version(std::uint8_t major, std::uint8_t minor, std::uint8_t patch);
        [[nodiscard]]
        DatabasesManager &getDBs() const;
        [[nodiscard]]
        std::uint64_t getVersion() const override;

    private:
        DatabasesManager &_dbs;
        std::shared_ptr<IProtocol> _actif;
};
