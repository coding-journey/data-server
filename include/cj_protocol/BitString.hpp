#pragma once
// TODO move this file in it's own namespace / folder

#include <cmath>
#include <string>

// TODO check endianess

// TODO use this class in Command & Response everywhere

// TODO add satic_asserts to restrict templates type

#include <bitset>
#include <iostream>

// TODO more unit tests
class BitString {
    public:
        BitString() = default;
        BitString(const std::string &str) :
            BitString(str, str.size() * 8)
        {}
        BitString(const std::string &str, std::uint64_t nb_bits);

        template<typename T>
        T extract_bytes(std::uint64_t nb) {
            return extract_bits<T>(nb * 8);
        }

        template <typename T>
        T extract_bytes() {
            return extract_bits<T>(sizeof(T) * 8);
        }

        template <typename T>
        T extract_bits(std::uint64_t nb);

        template <typename T>
        T extract_bits() {
            return extract_bits<T>(sizeof(T) * 8);
        }

        template <typename T>
        void insert_bytes(const T &bits) {
            insert_bits<T>(bits, sizeof(T) * 8);
        }

        template <typename T>
        void insert_bytes(const T &bits, std::uint64_t nb) {
            insert_bits<T>(bits, nb * 8);
        }

        template <typename T>
        void insert_bits(const T &bits) {
            insert_bits<T>(bits, sizeof(T) * 8);
        }

        template <typename T>
        void insert_bits(const T &bits, std::uint64_t nb);

        [[nodiscard]]
        std::uint64_t size() const noexcept {
            return _nb_bits;
        }

        void clear() {
            _buff.clear();
            _nb_bits = 0;
        }

        // TODO: replace all insert/extract bits with bytes when possible
    private:
        std::string _buff;
        std::uint64_t _nb_bits = 0;

        template <typename T>
        void extract_bit(T &res, std::uint64_t &nb);

        template <typename T>
        void insert_bit(const T &bits, std::uint64_t &nb);
};

template <>
std::string BitString::extract_bits<std::string>();
template <>
std::string BitString::extract_bits<std::string>(std::uint64_t nb);

template <>
std::string BitString::extract_bytes<std::string>();
template <>
std::string BitString::extract_bytes<std::string>(std::uint64_t nb);

template <>
void BitString::insert_bytes<std::string>(const std::string &bits);
template <>
void BitString::insert_bytes<std::string>(const std::string &bits, std::uint64_t nb);

template <>
void BitString::insert_bits<std::string>(const std::string &bits);
template <>
void BitString::insert_bits<std::string>(const std::string &bits, std::uint64_t nb);

template <>
void BitString::insert_bytes<char *>(char * const &bits);
template <>
void BitString::insert_bytes<char *>(char * const &bits, std::uint64_t nb);
template <>
void BitString::insert_bytes<const char *>(const char * const &bits);
template <>
void BitString::insert_bytes<const char *>(const char * const &bits, std::uint64_t nb);

template <>
void BitString::insert_bits<char *>(char * const &bits);
template <>
void BitString::insert_bits<char *>(char * const &bits, std::uint64_t nb);
template <>
void BitString::insert_bits<const char *>(const char * const &bits);
template <>
void BitString::insert_bits<const char *>(const char * const &bits, std::uint64_t nb);


template<typename T>
T BitString::extract_bits(std::uint64_t nb)  {
    T res = 0;

    if (_nb_bits < nb)
        throw "Err"; // TODO
    if (nb > sizeof(T) * 8)
        throw "Err"; // TODO
    while (_nb_bits % 8 != 0 && nb > 0) {
        extract_bit(res, nb);
    }
    while (nb >= 8) {
        res = (res << 8) + (unsigned char)_buff.front();
        _buff.erase(_buff.begin());
        _nb_bits -= 8;
        nb -= 8;
    }
    while (nb > 0) {
        extract_bit(res, nb);
    }
    return res;
};

template <typename T>
void BitString::insert_bits(const T &bits, std::uint64_t nb) {
    // typedef std::make_unsigned_t<T> unsigned_T;

    if (nb > sizeof(T) * 8)
        throw "Err"; // TODO
    while (_nb_bits % 8 != 0 && nb > 0) {
        insert_bit(bits, nb);
    }
    while (nb >= 8 && false) { // TODO FIXME
        _buff += (char)(bits & 0xFF);
        //bits = (unsigned_T)((unsigned_T)bits >> 8); // TODO FIXME wtf is that even working ?
        _nb_bits += 8;
        nb -= 8;
    }
    while (nb > 0) {
        insert_bit(bits, nb);
    }
};

template <typename T>
void BitString::extract_bit(T &res, std::uint64_t &nb) {
    static_assert(!std::is_const_v<T>, "Cannot be const"); // TODO Replace with enable if
    std::uint8_t mod = _nb_bits % 8;
    std::uint8_t mask = (std::uint8_t)std::pow(2, (mod == 0 ? 8 : mod) - 1);

    res = (res << 1) + (bool)(_buff.front() & mask);
    nb--;
    _nb_bits--;
    if (_nb_bits % 8 == 0)
        _buff.erase(_buff.begin());
};

template <typename T>
void BitString::insert_bit(const T &bits, std::uint64_t &nb) {
    std::uint8_t mod = nb % (sizeof(T) * 8);
    auto mask = (std::uint64_t)std::pow(2, (mod == 0 ? (sizeof(T) * 8) : mod) - 1);

    if (_nb_bits % 8 == 0)
        _buff.push_back((char)(bool)(bits & mask));
    else
        _buff.back() = (_buff.back() << 1) + (char)(bool)(bits & mask);
    nb--;
    _nb_bits++;
};
