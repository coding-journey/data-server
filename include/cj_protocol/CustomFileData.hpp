#pragma once

#include "database/ISerialisableData.hpp"

class CustomFileData : public ISerialisable {
    public:
        CustomFileData() = default;
        explicit CustomFileData(bool isPublic, bool isProject) : m_isPublic(isPublic), m_isProject(isProject) {};

        using ISerialisable::unserialize;

        [[nodiscard]]
        bool operator==(const CustomFileData &other) const;

        [[nodiscard]]
        std::string serialize() const override;
        void unserialize(std::istream &data) override;

        [[nodiscard]]
        const std::string getClassName() const override {
            return "CustomFileData";
        };

        [[nodiscard]]
        bool isPublic() const {
            return m_isPublic;
        };

        [[nodiscard]]
        bool isProject() const {
            return m_isProject;
        };
    private:
        bool m_isPublic = false;
        bool m_isProject = false;
};
