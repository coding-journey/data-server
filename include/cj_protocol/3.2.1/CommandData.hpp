#pragma once

#include "sqlite_orm/database_schema.hpp"

#include <array>
#include <string>

namespace CJ_Protocol_3_2_1 {
    class ICommandData {
        public:
            virtual ~ICommandData() = default;

            [[nodiscard]]
            bool needLogin() const;
            [[nodiscard]]
            bool isFragmented() const;
            [[nodiscard]]
            std::uint16_t getSize() const;

            [[nodiscard]]
            virtual std::string display() const = 0;

        protected:
            ICommandData(std::uint16_t size, const std::string &data, bool unfragmented);

            void setNeedLogin(bool value);
            void setSize(std::uint16_t value);

        private:
            std::uint16_t _size;
            bool _fragmented;
            bool _needLogin;
    };

    class NoData : public ICommandData {
        public:
            NoData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;
    };

    class NoDataLogin : public ICommandData {
        public:
            NoDataLogin(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;
};

    class RequestVersionData : public ICommandData {
        public:
            RequestVersionData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::uint16_t getVersion() const noexcept;
            [[nodiscard]]
            std::uint8_t getMajor() const noexcept;
            [[nodiscard]]
            std::uint8_t getMinor() const noexcept;
            [[nodiscard]]
            std::uint8_t getPatch() const noexcept;
        private:
            std::uint16_t _version;
            std::uint8_t _major;
            std::uint8_t _minor;
            std::uint8_t _patch;
    };

    class DeleteAccountData : public ICommandData {
        public:
            DeleteAccountData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::string getToken() const;
        private:
            std::string _token;
    };

    class RegisterData : public ICommandData {
        public:
            RegisterData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::string getUsername() const noexcept;
            [[nodiscard]]
            std::string getPassword() const noexcept;
            [[nodiscard]]
            std::string getEmail() const noexcept;
        private:
            std::string _email;
            std::string _username;
            std::string _password;
    };

    class LoginData : public ICommandData {
        public:
            LoginData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::string getToken() const noexcept;
            [[nodiscard]]
            std::string getUsername() const noexcept;
            [[nodiscard]]
            std::string getPassword() const noexcept;
            [[nodiscard]]
            bool isToken() const noexcept;
        private:
            std::string _token;
            std::string _username;
            std::string _password;
            bool _is_token = false;
    };

    class DownloadData : public ICommandData {
        public:
            DownloadData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::string getFilename() const;
        private:
            std::string _filename;
    };

    class UploadData : public ICommandData {
        public:
            UploadData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::string getFilename() const;
            [[nodiscard]]
            std::uint64_t getTimestamp() const;
            [[nodiscard]]
            std::string getContent() const;
            [[nodiscard]]
            bool hasFragmentedContent() const;
            [[nodiscard]]
            bool isPublic() const;

            void unfragment(const std::string &data);
        private:
            std::string _filename;
            std::uint64_t _timestamp;
            std::string _content;
            bool _fragmentedContent;
            bool _public;
    };

    class FragmentedData : public ICommandData {
        public:
            FragmentedData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            std::uint16_t getBlockID() const;
            [[nodiscard]]
            bool isModeGet() const;
            [[nodiscard]]
            std::string getData() const;
        private:
            std::uint16_t _blockID;
            bool _mode;
            std::string _data;
    };

    class ListPublicFilesData : public ICommandData {
        public:
            ListPublicFilesData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getUsername() const;
        private:
            std::string _username;
    };

    class DownloadPublicFileData : public ICommandData {
        public:
            DownloadPublicFileData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getUsername() const;
            [[nodiscard]]
            const std::string getFilename() const;
        private:
            std::string _username;
            std::string _filename;
    };

    class GetCategoriesData : public ICommandData {
        public:
            GetCategoriesData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getLanguage() const;

        private:
            std::string _lang;
    };

    class GetFunctionsData : public ICommandData {
        public:
            GetFunctionsData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getLanguage() const;
            [[nodiscard]]
            const std::string getCategory() const;

        private:
            std::string _lang;
            std::string _categ;
    };

    class GetFunctionData : public ICommandData {
        public:
            GetFunctionData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getLanguage() const;
            [[nodiscard]]
            const std::string getName() const;

        private:
            std::string _lang;
            std::string _name;
    };

    class ReportCrashData : public ICommandData {
        public:
            ReportCrashData(std::uint16_t size, const std::string &data, bool unfragmented);

            [[nodiscard]]
            std::string display() const override;

            [[nodiscard]]
            const std::string getStacktrace() const;
            [[nodiscard]]
            const std::string getMessage() const;
            [[nodiscard]]
            const std::string getSceneName() const;
            [[nodiscard]]
            SoftwareType getSoftware() const;
            [[nodiscard]]
            OperatingSystem getOS() const;
            [[nodiscard]]
            float getRAM() const;
            [[nodiscard]]
            const std::string getUUID() const;

        private:
            std::string _stacktrace;
            std::string _message;
            std::string _scene_name;
            SoftwareType _soft;
            OperatingSystem _os;
            float _ram;
            std::string _uuid;
    };
}
