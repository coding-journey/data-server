#pragma once

#include "cj_protocol/3.2.1/CommandData.hpp"

#include <memory>
#include <sstream>

namespace CJ_Protocol_3_2_1 {
    struct Command {
        public:
            Command() = default;
            Command(std::uint8_t code, std::uint16_t size, const std::string &data, bool unfragmented = false);
            Command(Command &&other) noexcept;
            Command(const Command &other) = delete;

            ~Command() = default;

            Command &operator=(Command &&other) noexcept;
            Command &operator=(const Command &other) = delete;

            [[nodiscard]]
            std::string toString() const;
            static Command fromString(std::stringstream &stream, bool unfragmented = false);

            [[nodiscard]]
            virtual std::string display() const;

            bool operator==(const Command &o);
        private:
            [[nodiscard]]
            ICommandData *initData(std::uint16_t size, const std::string &data, bool unfragmented) const;

        public:
            std::uint8_t _command_code;
            std::uint16_t _data_size;
            std::string _rawData;
            std::unique_ptr<ICommandData> _data;

    };
}
