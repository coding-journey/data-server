#pragma once

#include <memory>
#include <string>
#include <vector>

#include "cj_protocol/UserData.hpp"
#include "cj_protocol/WikiDatabase.hpp"

#include "database/FileData.hpp"
#include "database/ProjectData.hpp"

namespace CJ_Protocol_5_1_1 {
    class IResponseData {
        public:
            virtual ~IResponseData() = default;

            [[nodiscard]]
            std::string toString() const;
            void setData(const std::string &str);
            [[nodiscard]]
            std::uint16_t getSize() const;

            [[nodiscard]]
            virtual std::string display() const = 0;

        private:
            std::string _data;
            bool _fragmented;
    };

    class VersionResponseData : public IResponseData {
        public:
            VersionResponseData(std::uint16_t version);
            VersionResponseData(std::uint8_t major, std::uint8_t minor, std::uint8_t patch);

            [[nodiscard]]
            std::string display() const override;
    };

    class DeleteAccountResponseData : public IResponseData {
        public:
            DeleteAccountResponseData(const std::string &token);

            [[nodiscard]]
            std::string display() const override;
    };

    class LoginResponseData : public IResponseData {
        public:
            LoginResponseData(const std::string &token, const std::string &username, const std::string &email);
            LoginResponseData(const UserData &user);

            [[nodiscard]]
            std::string display() const override;
    };

    class ListFilesResponseData : public IResponseData {
        public:
            ListFilesResponseData(const std::vector<std::shared_ptr<const FileData>> &files);

            [[nodiscard]]
            std::string display() const override;
    };

    class ListProjectsResponseData : public IResponseData {
        public:
            ListProjectsResponseData(const std::vector<std::shared_ptr<const ProjectData>> &files);

            [[nodiscard]]
            std::string display() const override;
    };

    class DownloadResponseData : public IResponseData {
        public:
            DownloadResponseData(const std::shared_ptr<const FileData> &file);

            [[nodiscard]]
            std::string display() const override;
    };

    class ListFileResponseData : public IResponseData {
        public:
            ListFileResponseData(const std::shared_ptr<FileData> &file);

            [[nodiscard]]
            std::string display() const override;
    };

    class ListUsersResponseData : public IResponseData {
        public:
            ListUsersResponseData(const std::vector<std::shared_ptr<UserData>> &data);

            [[nodiscard]]
            std::string display() const override;
    };

    template <class T>
    class ListResponseData : public IResponseData {
        public:
            ListResponseData(std::uint16_t version, const T &values);

            [[nodiscard]]
            std::string display() const override;
    };

    template <>
    class ListResponseData<WikiDatabase::languages_t> : public IResponseData {
        public:
            ListResponseData(std::uint16_t version, const WikiDatabase::languages_t &languages);

            [[nodiscard]]
            std::string display() const override;
    };

    template <>
    class ListResponseData<WikiDatabase::categories_t> : public IResponseData {
        public:
            ListResponseData(std::uint16_t version, const WikiDatabase::categories_t &categories);

            [[nodiscard]]
            std::string display() const override;
    };

    template <>
    class ListResponseData<WikiDatabase::functions_t> : public IResponseData {
        public:
            ListResponseData(std::uint16_t version, const WikiDatabase::functions_t &functions);

            [[nodiscard]]
            std::string display() const override;
    };

    class FunctionsResponseData : public IResponseData {
        public:
            FunctionsResponseData(std::uint16_t version, const WikiDatabase::functions_t &functions);

            [[nodiscard]]
            std::string display() const override;
    };

    class FunctionResponseData : public IResponseData {
        public:
            FunctionResponseData(std::uint16_t version, const std::string &fun);

            [[nodiscard]]
            std::string display() const override;
    };
}
