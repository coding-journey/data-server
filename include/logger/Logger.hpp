#pragma once

#ifdef DEBUG
#undef DEBUG
#endif

#include <ostream>
#include <sstream>
#include <vector>

class Logger {
    public:
        enum LogLevel {
            INFO,
            DEBUG,
            WARN,
            ERROR
        };

    private:
        class Formatter : public std::stringstream {
            public:
                Formatter(const std::string &name, LogLevel level);
                Formatter(std::vector<std::string> names, LogLevel level);
                ~Formatter() override;

                Formatter(const Formatter &) = delete;
                Formatter(Formatter &&) = delete;
                Formatter &operator=(const Formatter &) = delete;
                Formatter &operator=(Formatter &&) = delete;

                Formatter &addStream(const std::string &name);
            private:
                std::vector<std::string> _stream_names;
                LogLevel _level;
                std::string _time = Logger::time();

                std::string formatLevel(LogLevel level);
        };

    public:
        static Logger::Formatter log(LogLevel level = INFO);
        static Logger::Formatter info();
        static Logger::Formatter debug();
        static Logger::Formatter warn();
        static Logger::Formatter error();
        static Logger::Formatter getLogger(const std::string &name, LogLevel level = INFO);
        static Logger::Formatter getLoggers(const std::vector<std::string> &names, LogLevel level = INFO);
        static std::ostream &get_log_stream(const std::string &name);

        // TODO add a setting to change this
        static std::string log_dir;
        static const char *const default_logger;
        static const char *const error_logger;
        static const char *const default_stream;
        static const char *const error_stream;

        // Return a string representation of the actual date/time.
        // To be used when logging messages
        static std::string time(const std::string &format = "[%d/%m/%Y %H:%M:%S]");
};
