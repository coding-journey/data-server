#pragma once

#include "exceptions/NestedException.hpp"

class ContextException : public NestedException {
    public:
        ContextException() = delete;

        // Will never be implemented. This function is used to describe the valid usages
        // of the macro. The macro will replace it with the full version of the function
        ContextException(const std::string &msg, std::exception_ptr nested_ex = std::current_exception()) = delete;

        ContextException(const std::string &file, int line, const std::string function, const std::string &msg, std::exception_ptr nested_ex = std::current_exception());

        const std::string &getFile() const;
        int getLine() const;

        const std::string getFormattedMessage() const override;
        const std::string getClass() const override;

    private:
        const std::string _file;
        const std::string _function;
        const int _line;
};


#ifndef __FUNCTION_NAME__
    #ifdef WIN32   //WINDOWS
        #define __FUNCTION_NAME__   __FUNCTION__
    #else          //*NIX
        #define __FUNCTION_NAME__   __func__
    #endif
#endif

// Confort macro, to include __FILE__, __LINE__ and __FUNCTION__ automaticly
#define NewContextException(...) ContextException(__FILE__, __LINE__, __FUNCTION_NAME__, __VA_ARGS__);
#define ChildContextException(child, ...) child(__FILE__, __LINE__, __FUNCTION_NAME__, __VA_ARGS__);
