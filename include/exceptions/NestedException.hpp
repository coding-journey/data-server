#pragma once

#include <exception>
#include <string>

class NestedException : public std::exception {
    public:
        NestedException() = delete;
        NestedException(const std::string &msg, std::exception_ptr nested_ex = std::current_exception());

        const std::string getMessage() const;
        const std::exception_ptr getNestedException() const;

        virtual const std::string getFormattedMessage() const;
        virtual const std::string getClass() const;

        const char *what() const noexcept override;

    protected:
        const std::string _msg;
        std::exception_ptr _nested;
};
