#pragma once

#include <any>
#include <memory>
#include <string>

enum class OperatingSystem {
    Other,
    Linux,
    Windows,
    Mac
};

enum class SoftwareType {
    Editor,
    Game
};

struct CrashReport {
    int id;
    int issue_id;
    std::string stacktrace;
    std::string message;
    std::shared_ptr<std::string> scene_name;
    SoftwareType software;
    OperatingSystem operating_system;
    float ram_usage;
    std::string date;
    std::shared_ptr<std::string> player_id;
    std::string uuid;
    int count;
};

struct Issue {
    int id;
    std::unique_ptr<int> original_report_id;
    int total_count;
};

struct db_holder;

class SQLite_database {
    public:
        SQLite_database(const std::string &path);
        ~SQLite_database();

        void report_crash(const std::string &stacktrace, const std::string &message, std::shared_ptr<std::string> scene_name, SoftwareType software, OperatingSystem os, float ram, const std::string &uuid, std::shared_ptr<std::string> playerid);

    private:
        db_holder *_db;
};
