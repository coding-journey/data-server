#pragma once

#include <string>

class CJ_utils {
    public:
        // Return an alphanumerical string of length characters.
        static std::string random_string(size_t length);

        // Return a string composed of random raw values.
        static std::string random_raw_string(size_t length);

        // Return a random number between min and max.
        static std::uint64_t random_number(std::uint64_t min, std::uint64_t max);
};
