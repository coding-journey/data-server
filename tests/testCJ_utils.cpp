#include <criterion/criterion.h>

#include "CJ_utils.hpp"

Test(CJ_Utils, Random_Number)
{
    int a = CJ_utils::random_number(0, 0);
    int b = CJ_utils::random_number(0, 1);

    cr_assert(a == 0);
    cr_assert(b == 0 || b == 1);
}
