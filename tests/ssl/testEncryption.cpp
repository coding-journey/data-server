#include <criterion/criterion.h>
#include <unistd.h>

#include "ssl/PEM_encrypt.hpp"
#include "cj_protocol/TMP_file.hpp"
#include "CJ_utils.hpp"

#include <algorithm>
#include <iostream>

#define NB_ROUNDS 10

bool files_same(std::fstream& f1, std::fstream& f2)
{
    // Reset EOF flags if any
    f1.clear();
    f2.clear();

    if (f1.fail() || f2.fail()) {
        return false; //file problem
    }

    f1.seekg(0, std::ifstream::end);
    f2.seekg(0, std::ifstream::end);

    if (f1.tellg() != f2.tellg()) {
        return false; //size mismatch
    }

    //seek back to beginning and use std::equal to compare contents
    f1.seekg(0, std::ifstream::beg);
    f2.seekg(0, std::ifstream::beg);
    return std::equal(std::istreambuf_iterator<char>(f1.rdbuf()),
                      std::istreambuf_iterator<char>(),
                      std::istreambuf_iterator<char>(f2.rdbuf()));
}

namespace CJ {
    enum mode_t {
        SMALL,
        NORMAL,
        DOUBLE,
        LARGE,
        VERRY_BIG,
        RANDOM,
    };
}

size_t get_size_from_mode(size_t in_size, size_t out_size, CJ::mode_t mode)
{
    switch (mode) {
        case CJ::SMALL:
            return in_size / 2;
        case CJ::NORMAL:
            return in_size;
        case CJ::DOUBLE:
            return in_size * 2;
        case CJ::LARGE:
            return in_size * 3 + 123;
        case CJ::VERRY_BIG:
            return in_size * 100 + 234;
        case CJ::RANDOM:
            return CJ_utils::random_number(in_size / 2, out_size * 10);
        default:
            return in_size;
    }
}

void run_encryption_test(CJ::mode_t mode)
{
    PEM_encrypt crypt("./aa.key"); // TODO change
    std::string tmp_name1 = TMP_file::temp_filename();
    TMP_file src;
    TMP_file tmp;
    TMP_file out;
    size_t size = get_size_from_mode(crypt.get_in_size(), crypt.get_out_size(), mode);
    std::string text = CJ_utils::random_string(size);

    src.write(text.c_str(), text.size());
    src.rewind();
    try {
        cr_assert(crypt.encrypt_file(src, tmp), "Failed to encrypt file");
        tmp.rewind();
        cr_assert(crypt.decrypt_file(tmp, out), "Failed to decrypt file");
        cr_assert(files_same(src, out), "The files are not the same");
    } catch (const std::string &e) {
        std::cerr << "Exception: " << e << std::endl;
        cr_assert_fail();
    } catch (const std::exception &e) {
        std::cerr << "Exception: " << e.what() << std::endl;
        cr_assert_fail();
    }
}

Test(Encryption_Decryption, Small_file)
{
    for (int i = 0; i < NB_ROUNDS; i++)
        run_encryption_test(CJ::SMALL);
}

Test(Encryption_Decryption, Medium_file)
{
    for (int i = 0; i < NB_ROUNDS; i++)
        run_encryption_test(CJ::NORMAL);
}

Test(Encryption_Decryption, Big_file)
{
    for (int i = 0; i < NB_ROUNDS; i++)
        run_encryption_test(CJ::DOUBLE);
}

Test(Encryption_Decryption, Large_file)
{
    for (int i = 0; i < NB_ROUNDS; i++)
        run_encryption_test(CJ::LARGE);
}

Test(Encryption_Decryption, Verry_Big_file)
{
    for (int i = 0; i < NB_ROUNDS / 2; i++)
        run_encryption_test(CJ::VERRY_BIG);
}

Test(Encryption_Decryption, Random_Size_file)
{
    for (int i = 0; i < NB_ROUNDS; i++)
        run_encryption_test(CJ::RANDOM);
}
