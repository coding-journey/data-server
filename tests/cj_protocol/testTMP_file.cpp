#include <criterion/criterion.h>

#include "cj_protocol/TMP_file.hpp"

Test(TMP_files, Getters)
{
    std::string name1 = TMP_file::temp_filename();
    TMP_file t1(name1);

    cr_assert_eq(name1, t1.get_filename(), "Filenames are not the same");
}
