#include <criterion/criterion.h>

#include "cj_protocol/CustomFileData.hpp"

static void test_consistency(const CustomFileData &in)
{
    CustomFileData out;

    out.unserialize(in.serialize());
    cr_assert(in == out);
}

Test(CustomFileData, test_Consistency)
{
    CustomFileData data1(true, true);
    CustomFileData data2(false, false);
    CustomFileData data3(true, false);
    CustomFileData data4(false, true);

    cr_assert(data1.isPublic() && data1.isProject());
    cr_assert(!data2.isPublic() && !data2.isProject());
    cr_assert(data3.isPublic() && !data3.isProject());
    cr_assert(!data4.isPublic() && data4.isProject());
    test_consistency(data1);
    test_consistency(data2);
    test_consistency(data3);
    test_consistency(data4);
}
