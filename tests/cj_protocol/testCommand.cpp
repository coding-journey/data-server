#include <criterion/criterion.h>

#include "cj_protocol/3.2.1/Command.hpp"
#include "cj_protocol/2.1.1/Command.hpp"

Test(CJ_Protocol2_1_1_Command, test_Serialisation_Deserialisation)
{
    CJ_Protocol_2_1_1::Command command(1, 23, "\x31\x32\x33\x31\x32\x33\x31\x32\x33\x31\x32\x33\x31\x32\x33\x31\x32\x05\x48\x65\x6C\x6C\x6F");
    std::string str = command.toString();
    std::stringstream s(str);
    CJ_Protocol_2_1_1::Command test = CJ_Protocol_2_1_1::Command::fromString(s);
    CJ_Protocol_2_1_1::Command a;

    cr_assert(test == command);
}

Test(CJ_Protocol3_2_1_Command, test_Serialisation_Deserialisation)
{
    CJ_Protocol_3_2_1::Command command(1, 23, "\x31\x32\x33\x31\x32\x33\x31\x32\x33\x31\x32\x33\x31\x32\x33\x31\x32\x05\x48\x65\x6C\x6C\x6F");
    std::string str = command.toString();
    std::stringstream s(str);
    CJ_Protocol_3_2_1::Command test = CJ_Protocol_3_2_1::Command::fromString(s);
    CJ_Protocol_3_2_1::Command a;

    cr_assert(test == command);
}
