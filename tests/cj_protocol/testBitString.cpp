#include <criterion/criterion.h>
#include "cj_protocol/BitString.hpp"

Test(BitString, Insert_Extract_equivalence_uint8)
{
    BitString bits("abc");
    std::uint8_t first = bits.extract_bits<std::uint8_t>(3);
    std::uint8_t second = bits.extract_bits<std::uint8_t>(5);
    BitString res;
    char a = 0;

    cr_assert(bits.size() == 2 * 8);
    res.insert_bits(first, 3);
    res.insert_bits(second, 5);
    cr_assert(res.size() == 8);

    a = res.extract_bits<char>();
    cr_assert(res.size() == 0);
    cr_assert(a == 'a');
}

Test(BitString, Insert_Exctract_equivalence_uint64)
{
    std::uint64_t a;
    std::uint64_t b = 123456;
    std::uint64_t c;
    BitString bits;

    a = 1234;
    cr_assert(bits.size() == 0);
    bits.insert_bits(a);
    cr_assert(bits.size() == sizeof(std::uint64_t) * 8);
    c = bits.extract_bits<std::uint64_t>();
    cr_assert(a == c);

    a = 0xCD45;
    cr_assert(bits.size() == 0);
    bits.insert_bits(a);
    cr_assert(bits.size() == sizeof(std::uint64_t) * 8);
    c = bits.extract_bits<std::uint64_t>();
    cr_assert(a == c);

    bits.insert_bits<std::uint32_t>(b);
    a = bits.extract_bits<std::uint64_t>(9);
    c = bits.extract_bits<std::uint64_t>(23);
    cr_assert(bits.size() == 0);
    bits.insert_bits(a, 9);
    bits.insert_bits(c, 23);
    a = bits.extract_bits<std::uint64_t>(32);
    cr_assert(a == b);
}

Test(BitString, Insert_Extract_equivalence_string)
{
    std::string input = "abc";
    BitString bits;
    BitString res;
    std::string tmp;

    bits.insert_bits(input, 16);
    cr_assert(bits.size() == 8 * 2);
    tmp = bits.extract_bits<std::string>();
    cr_assert(tmp.size() == 2);
    cr_assert(tmp.compare(0, 2, input, 0, 2) == 0);
    res.insert_bits(tmp, 5);
    cr_assert(res.size() == 5);
    char a = res.extract_bits<char>(5);

    cr_assert(0x01 == a);
}

Test(BitString, clear)
{
    BitString bits("blabla");

    cr_assert(bits.size() == 6 * 8);
    bits.clear();
    cr_assert(bits.size() == 0);
}

Test(BitString, errors)
{
    BitString bits;

    bits.insert_bits<std::uint16_t>(1234);
    cr_assert(bits.size() == 16);
    cr_assert_throw(bits.extract_bits<std::uint64_t>(), const char *);
    bits.insert_bits<std::uint64_t>(1234);
    cr_assert_throw(bits.extract_bits<std::uint16_t>(64), const char *);
    cr_assert_throw(bits.insert_bits<std::uint16_t>(0, 64), const char *);
}
