#include <criterion/criterion.h>

#include "database/FileData.hpp"
#include "CJ_utils.hpp"

#include <sstream>

Test(FileData, operator_equal)
{
    FileData d("file.txt", 123123, "hello world");
    ISerialisableData &ref = d;

    cr_assert(d == d);
    cr_assert(ref == ref);
    cr_assert(d == ref);
}

void run_serialize_test(std::uint16_t filename_size, std::uint16_t content_size, std::uint64_t timestamp)
{
    std::string filename = CJ_utils::random_string(filename_size);
    std::string content = CJ_utils::random_raw_string(content_size);
    FileData data(filename, timestamp, content);

    std::string str = data.serialize();
    std::stringstream a(str);

    FileData other;
    other.unserialize(a);

    cr_assert(data == other, "Serialisation not reversible");
}

Test(FileData, serialize_deserialize)
{
    const std::uint16_t min = std::numeric_limits<std::uint16_t>::min();

    // We are testing with random inputs, so let's do this 100 times !
    for (int i = 0; i < 100; i++) {
        std::uint16_t filename_size = CJ_utils::random_number(min + 1, FileData::max_filename);
        std::uint16_t content_size = CJ_utils::random_number(min, FileData::max_content);
        std::uint64_t timestamp = CJ_utils::random_number(
            std::numeric_limits<std::uint64_t>::min(),
            std::numeric_limits<std::uint64_t>::max()
        );

        run_serialize_test(filename_size, content_size, timestamp);
    }
}
