#include <criterion/criterion.h>

#include "database/FileDatabase.hpp"
#include "database/FileData.hpp"

std::string getDirectory()
{
    return std::filesystem::temp_directory_path().string() + "/.db/";
}

void clear(void)
{
   std::filesystem::remove_all(getDirectory());
}

TestSuite(FileDatabase, .fini = clear);

Test(FileDatabase, easy_tests)
{
    std::string db_name = "toto";
    // FileDatabase<FileData> db("aaa", "/tmp");
    FileDatabase<FileData> db(getDirectory());
    std::string id = "_id";
    std::shared_ptr<FileData> data = std::make_shared<FileData>(id, 2, "blabla");

    db.select_database(db_name);
    cr_assert(db.list().size() == 0, "The db is not empty");

    db.insert(data);
    cr_assert(db.list().size() == 1);
    cr_assert(db.querry(id) != nullptr, "The data was not found");

    db.select_database("tata");
    cr_assert(db.list().size() == 0, "The db is not empty");

    cr_assert(std::filesystem::exists(getDirectory() + "/" + db_name + ".db"), "The db file was not found");
    std::filesystem::copy(getDirectory() + "/" + db_name + ".db", "/tmp/aaa", std::filesystem::copy_options::overwrite_existing);

    db.select_database(db_name);
    cr_assert(db.list().size() != 0, "The db was empty");
    cr_assert(*db.querry(id) == *data, "The data did not match");
}
