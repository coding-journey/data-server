#include <criterion/criterion.h>

#include "exceptions/NestedException.hpp"

Test(NestedException, nested_exceptions)
{
    std::string err_1 = "Bug";
    std::string err_2 = "Error";

    try {
        try {
            throw NestedException(err_1);
        } catch (const NestedException &e) {
            throw NestedException(err_2);
        }
    } catch (const std::exception &e) {
        const NestedException *ex = dynamic_cast<const NestedException *>(&e);

        cr_assert(ex);
        cr_assert(ex->getMessage() == err_2);
        cr_assert(ex->getNestedException());
        try {
            std::rethrow_exception(ex->getNestedException());
        } catch (const std::exception &e) {
            const NestedException *ex = dynamic_cast<const NestedException *>(&e);

            cr_assert(ex);
            cr_assert(ex->getMessage() == err_1);
            cr_assert(ex->getNestedException() == nullptr);
        }
    }
}
