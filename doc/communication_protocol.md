# Communication Protocol (Data server)

## Préambule

Ce document va décrire la manière dont le jeu et le server de données vont 
communiquer.

Dans la suite de ce document, certains termes seront utilisés, nous allons les 
définir ici :
    - SERVEUR : Le SERVEUR est le programme qui va recevoir les données des 
    CLIENTS et qui va les stocker.
    - CLIENT : Les CLIENTS sont les joueurs, ou plus précisément leur instance de
    CodingJourney actuellement en cours d'execution sur leur ordinateur.
    
    
## Informations Générales

Le CLIENT et le SERVEUR vont tous les deux stocker les données du joueur. Ces
données servont encryptés de chaque coté pour éviter qu'un joueur les modifie 
par exemple. 
L'encryption ne fera pas partie de ce document, les CLIENTS et le 
SERVEUR peuvent crypter les données de la manière dont ils le souhaitent. La 
seule règle a respecter est que les données doivent être décryptés avant d'être
envoyées.

La communication se fera par TCP suivant le protocole TLS pour l'encryption de
la communication. Le SERVEUR doit possèder un certificat certifiant son identitée 
évitant ainsi les attaques MITM (man in the middle).

Le protocole de communication décrit par ce document est un protocole binaire, 
et respecte le **network byte order** (Big Endian pour la représentation 
des nombres). Comme dit précédement, il est construit sur une couche TLS.

La communication se fera sur le port 2369;

A la moindre erreur de protocole *fatale* faite par le CLIENT, le SERVEUR enverra une 
erreur puis terminera la connection. Si le SERVEUR commet une erreur *fatale* de protocole,
le CLIENT n'a pas besoin d'envoyer de code d'erreur et peut directement fermer la
connection.



## Syntaxe des commandes

Tous les messages envoyés par le CLIENT sont en fait des commandes que le SERVEUR 
interprétera. Pour chaque commande reçue, le SERVEUR renverra une réponse indiquant 
si la commande à réussi ou non.

*Note* : Dans ce document, les différentes parties des packets seront nommées
pour simplifier la compréhension. Ces noms ne sont aucunement utilisés dans le protocole.
Ils peuvent néanmoins servir a des fins d'affichage de débug.

*Note* : Dans ce document, les exemples de représentation hexadécimales et binaires
ne sont que des représentations pour aider à la compréhension. Les espaces sont ajoutés
pour aider visuellement à associer chaque nombre avec le champ correspondant.
Dans la réalitée, il n'y a pas d'espace entre les champs, les bits s'enchainent sans 
discontinuitée. De plus, les bits sont normalement organisés par octets. Le protocole
suit cette règle, mais cela ne sera pas montré dans les exemples. 

La syntaxe des commandes est la suivante : 

`[COMMAND_CODE][DATA_SIZE][DATA]`
- Le champ `[COMMAND_CODE]` est le code de la commande à executer. 
Il est de taille fixe : 5 bits (0-31).
- Le champ `[DATA_SIZE]` contient le nombre d'octets du champ `[DATA]`. 
Il est de taille fixe : 11 bits (0-2047).
- Le champ `[DATA]` contient les paramètres de la commande, son contenu et 
organisation interne varie en fonction de la commande. 
Sa taille est variable et est indiquée dans le champ `[DATA_SIZE]`. 
Si le champ `[DATA_SIZE]` contient la valeur 0, alors il n'y aura pas de champ `[DATA]`.

Cela est fait de manière à ce que les 2 premiers champs ensembles occupent 2 octets,
et que chaqun d'eux possède assez d'espace pour stocker leur information.

Actuellement il y a de la place pour encoder 31 commandes (ce qui est largement 
plus que ce qui est actuellement utilisé) et la taille des données peut aller en 
théorie jusqu'à 2047, ce qui est suffisant pour la plupart des commandes.
La valeur 2047 est réservée par le protocole, donc la taille des données ne peut
excéder 2046 en pratique.

Dans le cas où la limite de 2046 risque de poser problème, le champ 
`[DATA_SIZE]` doit être mis à la valeur spéciale de 2047 (tous les bits
du champ sont à 1). Dans ce cas les données seront fragmentés en block de taille 
inférieure ou égale à 65535 caractères puis envoyés à la suite du champ `[DATA]`.
Dans ce cas, le champ `[DATA]` aura une taille de 2 octets et contiendra le 
nombre de blocks à récupérer. Chaque block commencera par la taille du block sur 
2 octets (0 - 65535) suivit par les données.


## Syntaxe des réponses

Pour chaque commande envoyée par le CLIENT, le SERVEUR va envoyer une réponse.
La syntaxe des réponses est la même si la commande s'est exécutée avec succès ou non.

La syntaxe des réponses est la suivante :

`[STATUS_CODE][COMMAND_CODE][DATA_SIZE][DATA]`
- Le champ `[STATUS_CODE]` contient le statut de la commande. Si il est à 0, c'est que 
tout c'est bien passé. Sinon, il contient la valeur du code d'erreur correspondant.
Il est de taille fixe : 8 bits (0-255).
- Le champ `[COMMAND_CODE]` est le code de la commande qui a été executée.
Il est de taille fixe : 5 bits (0-31).
- Le champ `[DATA_SIZE]` contient le nombre d'octets du champ `[DATA]`. 
Il est de taille fixe : 11 bits (0-2047).
- Le champ `[DATA]` contient les données du résultat de la commande, son contenu et 
organisation interne varie en fonction de la commande. 
Sa taille est variable et est indiquée dans le champ `[DATA_SIZE]`. 
Si le champ `[DATA_SIZE]` contient la valeur 0, alors il n'y aura pas de champ `[DATA]`.
Si le champ `[DATA_SIZE]` contient la valeur spéciale de 2047 (tous les bits
du champ sont à 1), les données seront fragmentés en block de taille 
inférieure ou égale à 65535 caractères puis envoyés à la suite du champ `[DATA]`.
Dans ce cas, le champ `[DATA]` aura une taille de 2 octets et contiendra le 
nombre de blocks à récupérer. Chaque block commencera par la taille du block sur 
2 octets (0 - 65535) suivit par les données.

## Codes de status

Les réponses du SERVEUR contiendront toujours un code représentant l'état d'achèvement
de la commande. 
Cette section détaille la signification de chaqun d'entre eux.

Certaines erreurs sont spécifiques à certaines commandes, et ne peuvent se produire
que lors de l'utilisation de l'une d'entre elles.
Les autres erreurs peuvent se produire pour toutes les commandes sauf si spécifié 
autrement.

Comme les codes de status sont écrits sur 1 octet, il est très simple de les représenter 
sous la forme hexadécimale suivante : `0x08`.
Comme leurs valeurs peuvent aller de 0 (`0x00` en hexadécimal) à 255 (`0xFF` en hexadécimal),
cette représentation est parfaite.

### STATUS_OK

Name : `STATUS_OK`

Hex code : `0x00`

Signification : 
    Ce code signifie que tout s'est bien passé. Le champ `[DATA]`, si présent,
    contient le résultat de la commande. Il peut être vide si la commande ne 
    renvoit rien. Se référer à la commande pour plus d'informations.


### ERR_PARAMETER_ERROR

Name : `ERR_PARAMETER_ERROR`

Hex code : `0x01`

Signification : 
    Ce code signifie que le CLIENT n'a pas envoyé les bons paramètres à la fonction,
    ou que leur taille n'est pas correcte.
    Cela peut venir du fait que la version du protocole du CLIENT et du SERVER
    diverge.

### ERR_UNKONW_COMMAND

Name : `ERR_UNKONW_COMMAND`

Hex code : `0x02`

Signification : 
    Ce code signifie que le SERVER ne reconnais pas le code de fonction envoyé 
    par le CLIENT.
    Cela peut venir du fait que la version du protocole du CLIENT et du SERVER
    diverge.


### ERR_VERSION

Name : `ERR_VERSION`

Hex code : `0x04`

Cette erreur est spécifique à la fonction `REQUEST_SERVER_VERSION`

Signification : 
    Cette erreur signifie que le SERVEUR n'est pas capable de fournir la
    version demandée. Le client devrait rééssayer en changeant la version majeure.
    A noter que le SERVEUR ne regarde pas la version de patch demandée, et prendra la
    plus élevée qu'il a à disposition. 
    De plus, le SERVEUR est autorisé à prendre une version mineure plus grande
    que celle demandée si disponible, ou plus petite si il ne peut pas faire
    autrement, et donc ne retournera pas forcément d'erreur dans ces cas là.

### ERR_MISSING_LOGIN

Name : `ERR_MISSING_LOGIN`

Hex code : `0x07`

Signification : 
    Cette erreur signifie que le CLIENT n'a pas encore envoyé la commande
    `LOGIN` et qu'il utilise une commande qui requiert une autentification.

### ERR_INVALID_LOGIN

Name : `ERR_INVALID_LOGIN`

Hex code : `0x09`

Cette erreur est spécifique à la fonction `LOGIN`

Signification : 
    Cette erreur signifie que le CLIENT à essayé de se login avec des 
    informations erronées. Le SERVER a donc rejeté cette demande de login, 
    et le CLIENT doit envoyer de nouvelles informations à jour si il veut
    pouvoir se login.

### ERR_EMAIL_TAKEN

Name : `ERR_EMAIL_TAKEN`

Hex code : `0x0A`

Cette erreur est spécifique à la fonction `REGISTER`

Signification : 
    Cette erreur signifie que le CLIENT a essayé de créer un compte avec un 
    email déjà utilisé. Le CLIENT devrait refaire la commande avec un nouveau
    email.

### ERR_USERNAME_TAKEN

Name : `ERR_USERNAME_TAKEN`

Hex code : `0x0B`

Cette erreur est spécifique à la fonction `REGISTER`

Signification : 
    Cette erreur signifie que le CLIENT a essayé de créer un compte avec un 
    username déjà utilisé. Le CLIENT devrait refaire la commande avec un 
    nouveau username.

### ERR_INVALID_PASSWORD

Name : `ERR_INVALID_PASSWORD`

Hex code : `0x0C`

Cette erreur est spécifique à la fonction `REGISTER`

Signification : 
    Cette erreur signifie que le CLIENT a essayé de se register avec un mot
    de passe qui n'es pas suffisament sécurisé ou trop court, ou trop long. 
    Le CLIENT devrait choisir un mot de passe plus sécurisé et réessayer.
    La taille maximum d'un mot de passe est 255 caractères, la taille minimum
    est 8 caractères et il doit comprendre au moins 3 des 4 éléments suivants :
    - Lettre minuscule
    - Lettre majuscule
    - Chiffre
    - Symbols


### ERR_NO_SUCH_FILE

Name : `ERR_NO_SUCH_FILE`

Hex code : `0x0D`

Cette erreur est spécifique aux fonction `DOWNLOAD_FILE` et `DOWNLOAD_PUBLIC_FILE`

Signification : 
    Cette erreur signifie que le fichier demandé par le CLIENT n'existe pas, et 
    ne peut donc pas être téléchagé.

### ERR_TOO_MANY_FILES

Name : `ERR_TOO_MANY_FILES`

Hex code : `0x0E`

Cette erreur est spécifique à la fonction `UPLOAD_FILES`

Signification : 
    Cette erreur signifie que le CLIENT a atteint le nombre maximum de fichiers
    différents sur le SERVEUR et ne peut donc pas en envoyer de nouveaux.

### ERR_NO_SUCH_PROJECT

Name : `ERR_NO_SUCH_PROJECT`

Hex code : `0x0F`

Cette erreur est spécifique à la fonction `DELETE_PROJECT`

Signification : 
    Cette erreur signifie que le CLIENT essaye de supprimer un project qui 
    n'existe pas ou dont il n'est pas le propriétaire.

### ERR_UNKNOWN_USERNAME

Name : `ERR_UNKNOWN_USERNAME`

Hex code : `0x21`

Cette erreur est spécifique aux fonctions `LIST_PUBLIC_FILES` and `DOWNLOAD_PUBLIC`

Signification : 
    Cette erreur signifie que le SERVER ne connait pas de joueur avec le username donné,
    et que par conséquent il lui est impossible de procéder.

### ERR_UNKNOWN_LANGUAGE

Name : `ERR_UNKNOWN_LANGUAGE`

Hex code : `0x30`

Cette erreur est spécifique aux fonctions `GET_CATEGORIES` `GET_FUNCTIONS_NAMES`
and `GET_FUNCTIONS`

Signification : 
    Cette erreur signifie que le SERVER ne connait pas le language demandé
    et que par conséquent il lui est impossible de retourner une réponse valide.

### ERR_UNKNOWN_CATEGORY

Name : `ERR_UNKNOWN_CATEGORY`

Hex code : `0x31`

Cette erreur est spécifique aux fonctions `GET_FUNCTIONS_NAMES` and `GET_FUNCTIONS`

Signification : 
    Cette erreur signifie que le SERVER ne connait pas la catégorie demandée
    et que par conséquent il lui est impossible de retourner une réponse valide.

### ERR_UNKNOWN_FUNCTION

Name : `ERR_UNKNOWN_FUNCTION`

Hex code : `0x32`

Cette erreur est spécifique a la fonction `GET_FUNCTION`

Signification : 
    Cette erreur signifie que le SERVER ne connait pas la fonction demandée
    et que par conséquent il lui est impossible de retourner une réponse valide.

### ERR_PROTOCOL_MISSMATCH

Name : `ERR_PROTOCOL_MISSMATCH`

Hex code : `0xFE`

Signification : 
    Cette erreur signifie que le SERVEUR n'est pas en mesure de traiter
    la commande du CLIENT a cause d'une erreur de la part du CLIENT.
    Cette erreur est généralement renvoyée si le CLIENT à commis une erreur
    qui n'est pas indiquée par l'un des autres codes d'erreur et que le SERVEUR
    n'a pas rencontré d'erreur.
    Cette erreur peut être envoyée si le CLIENT n'a pas envoyé un message pouvant
    être interprété en tant que commande. A cause de cela, le server risque de renvoyer
    un numéro de commande erroné (certainement le numéro 0) car il n'a pas été en mesure
    de lire le code envoyé par le CLIENT a cause de la malformation du message.
    Quand le CLIENT recoit cette erreur, il ne peut donc pas se baser sur le
    `COMMAND_CODE` retourné.

### ERR_INTERNAL_SERVER_ERROR

Name : `ERR_INTERNAL_SERVER_ERROR`

Hex code : `0xFF`

Signification : 
    Cette erreur signifie que le SERVEUR a rencontré une erreur inconnue,
    et se retrouve dans l'incapacitée de répondre au CLIENT. Cela indique 
    généralement un malfonctionnement du SERVEUR.


## Commandes

Cette section liste les commande utilisables par le client, avec leurs paramètres
ainsi que le contenu de leur réponse et des erreurs qu'elles pourraient renvoyer.

Les paramètres des commandes doivent être envoyés les uns à la suite des autres 
dans le champ `[DATA]`. La taille des paramètres sera indiqué dans la description 
de la commande. Si le paramètres possède une taille variable, il sera précédé de 
sa taille, sur un nombre de bits variable selon le paramètre.

Comme les commandes sont représentés sur 5 bits, leurs codes seront compris entre
`0x00` et `0x1F` (0 et 31 en décimal).

Dans les exemples, pour simplifier l'affichage, certains paramètres seront affichés
sous forme de texte, encadrés par des guillemets : "exemple". Cela permet de gagner
de la place, et de simplifier les exemples. Il faut comprendre que chaque caractère 
du texte occupera un octet et sera converti selon la table ASCII.
Exemple : 
    le texte "exemple" est représenté `0x65 0x78 0x65 0x6D 0x70 0x6C 0x65`
    en hexadécimal d'après la table ASCII. On voit le gain en lisibilitée
    et simplicitée de laisser la forme textuelle dans ce document.



### VERSION

Name : `VERSION`

Hex code : `0x00`

Description : 
    Cette commande permet au CLIENT de demander la version du protocole 
    utilisé par le SERVEUR.
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 
    Il est recommandé de l'envoyer directement après avoir établi une connection
    pour s'assurer que la version est supportée par le client.
    Les numéros de versions sont spéarés en 3 partie : 
    - La verison majeure : Un changement de version majeure indique de gros changements,
    incompatibles avec d'anciennes versions.
    - La verison mineure : Elle représente des ajouts / changements minimes
    qui ne devrait pas poser de problèmes avec d'anciennes verisons. Ce sont généralement
    des nouvelles fonctions, donc une différence de version implique juste que certaines 
    commandes ne seront pas disponibles.
    - La version de patch : Elle représente des corrections de bugs / changements
    internes qui n'impactent normalement pas la communication.

Parameters : Aucun

Response Data :  `[VERSION]`
- Le champ `[VERSION]` contient la version du server. 
Il est de taille fixe : 2 octets. 
La version est encodée comme suit : 
    Les 3 premiers bits servent à encoder la version majeure (0-7).
    Les 5 bits suivants servent à encoder la version mineure (0-31).
    Les 8 bits suivants servent à encoder la version de patch (0-255).
A noter qu'il faut ajouter 1 au numéro des versions (ainsi, la version 
minimale possible est 1.1.1 et la version maximale possible est 8.32.256)
car la version 0.0.0 n'existe pas et donc les versions sont encodés avec un
décalage de 1 pour utiliser les 2 octets en entier.
    
Errors : Aucune

Exemple :
Code :     `[VERSION][DATA_SIZE]`
Hex  :     `0x00     0x00       `
Bin  :     `00000    00000000000`
Response Data : `VERSION` = 2.4.17
Code :     `[VERSION]        `
Hex  :     `0x44 0x11        `
Bin  :     `01000100 00010001`


### REQUEST_SERVER_VERSION

Name : `REQUEST_SERVER_VERSION`

Hex code : `0x01`

Description : 
    Cette commande permet au CLIENT de demander au server d'utiliser une autre version
    du protocole. Cette commande à de grandes chances d'échouer car rien ne garenti 
    que le server est capable de gérer plusieurs versions et que la version demandée
    sera disponible. Les CLIENTS ne doivent donc pas se reposer sur son utilisation.
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 
---
    Le SERVEUR essayera de trouver la version la plus proche de celle demandée, 
    en mettant la priorité sur la version majeure, puis mineure, puis patch.
    A noter que le SERVEUR ne regarde pas la version de patch demandée, et prendra la
    plus élevée qu'il a à disposition. De plus, le SERVEUR est autorisé à prendre une
    version mineure plus grande que celle demandée si disponible, ou plus petite si il 
    ne peut pas faire autrement. De toute facon le SERVEUR renverra la version qu'il 
    utilisera, donc le client pourra connaitre la version choisie par le SERVEUR.
---
    Attention : Après avoir utilisé cette commande, la session sera réinitialisée
    comme si le CLIENT vennait d'ouvrir la connection au SERVER. Il devra donc 
    réutiliser la commande LOGIN.

Parameters : `[VERSION]`
- Le champ `[VERSION]` contient la version demandée.
Il est de taille fixe : 2 octets. 
Pour plus de détails sur ce champ, se référer à la commande `VERSION`.
    
Errors : `ERR_VERSION`

Response Data :  `[VERSION]`
- Le champ `[VERSION]` contient la version choisie par le server.
Il est de taille fixe : 2 octets. 
Pour plus de détails sur ce champ, se référer à la commande `VERSION`.

Exemple : `VERSION` = 2.6.11
Code :     `[REQUEST_SERVER_VERSION][DATA_SIZE] [VERSION]        `
Hex  :     `0x00                    0x02        0x46 0x0B        `
Bin  :     `00000                   00000000010 01000110 00001011`
Response Data : `VERSION` = 2.4.17
Code :     `[VERSION]        `
Hex  :     `0x44 0x11        `
Bin  :     `01000100 00010001`


### DELETE_ACCOUNT

Name : `DELETE_ACCOUNT`

Hex code : `0x02`

Description : 
    Cette commande permet au CLIENT de supprimer son compte et toutes les données
    associés avec celui-ci. Attention : cette action est IRREVERSIBLE. Une fois 
    un compte supprimmé, il n'y a aucun moyen de le récupérer.
    Pour éviter une suppression par erreur, la suppression se fait en 2 étapes:
    - Il faut d'abbord appeler la commande `DELETE_ACCOUNT` avec un token, peu 
      importe lequel (il peut être vide et avoir une taille de 0). 
      La commande `DELETE_ACCOUNT` va alors générer un token de suppression de 
      compte aléatoire et le retourner.
    - Le CLIENT doit appeler la commande une 2e fois avec le bon token de 
      suppression de compte. Si le token est le même, le compte est supprimé et
      le SERVER renverra un token vide (de taille 0) pour confirmer la suppression 
      du compte. Si le SERVER renvoie un token non-vide, cela veut dire que le 
      token de suppression de compte est invalide.
      Le SERVEUR va aussi réinitialiser la connection, ce qui deconnectera le CLIENT.

Parameters : `DELETE_TOKEN_SIZE` `DELETE_TOKEN`
    - Les paramètres `DELTE_TOKEN_SIZE` et `DELTE_TOKEN` correspondent respectivement 
    à la taille du token de suppression de compte, et au token lui même.
    `DELTE_TOKEN_SIZE` est de taille fixe : 1 octet (0-255).
    `DELTE_TOKEN` est de taille variable, sa taille est indiquée dans `DELTE_TOKEN_SIZE`.

Errors : `ERR_MISSING_LOGIN`

Response Data : `DELETE_TOKEN_SIZE` `DELETE_TOKEN`
    - `DELTE_TOKEN_SIZE` et `DELTE_TOKEN` correspondent respectivement 
    à la taille du token de suppression de compte, et au token lui même.
    `DELTE_TOKEN_SIZE` est de taille fixe : 1 octet (0-255).
    `DELTE_TOKEN` est de taille variable, sa taille est indiquée dans `DELTE_TOKEN_SIZE`.


### REGISTER

Name : `REGISTER`

Hex code : `0x03`

Description : 
    Cette commande permet au CLIENT de créer un compte sur le SERVER et de 
    pouvoir par la suite utiliser toutes les commandes qui nécéssitent d'être
    connecté. Cette commande (en cas de succès) va autentifier le CLIENT comme
    si il avait fait `LOGIN`, ce qui évite au CLIENT d'utiliser la commande 
    `LOGIN` qui serait un peu redondante.
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 

Parameters : `EMAIL_SIZE` `EMAIL` `USERNAME_SIZE` `USERNAME` `PASSWORD_SIZE` `PASSWORD`
    - Les paramètres `EMAIL_SIZE` et `EMAIL` correspondent respectivement 
    à la taille de l'email et a l'email. 
    `EMAIL_SIZE` est de taille fixe : 1 octet (0-255).
    `EMAIL` est de taille variable, sa taille est indiquée dans `EMAIL_SIZE`.
    - Les paramètres `UNSERNAME_SIZE` et `UNSERNAME` correspondent respectivement 
    à la taille du username et au username. 
    `USERNAME_SIZE` est de taille fixe : 1 octet (0-255).
    `USERNAME` est de taille variable, sa taille est indiquée dans `USERNAME_SIZE`.
    - Les paramètres `PASSWORD_SIZE` et `PASSWORD` correspondent respectivement 
    à la taille du password et au password.
    `PASSWORD_SIZE` est de taille fixe : 1 octet (0-255).
    `PASSWORD` est de taille variable, sa taille est indiquée dans `PASSWORD_SIZE`.

Errors : `ERR_EMAIL_TAKEN` `ERR_USERNAME_TAKEN` `ERR_INVALID_PASSWORD`

Response Data : `TOKEN` `USERNAME_SIZE` `USERNAME` `EMAIL_SIZE` `EMAIL`
    - Le champ `TOKEN` correspond au token que le CLIENT peut utiliser pour 
    se connecter la prochaine fois. 
    ATTENTION : Ce token est valide jusqu'à ce qu'il soit invalidé par un LOGOUT.
    Un LOGOUT va donc invalider *tous* les CLIENT qui utilisaint ce token, et ils
    devront se reconnecter.
    Ce paramètre est de taille fixe, 36 octets.
    - Les paramètres `UNSERNAME_SIZE` et `UNSERNAME` correspondent respectivement 
    à la taille du username et au username. 
    `USERNAME_SIZE` est de taille fixe : 1 octet (0-255).
    `USERNAME` est de taille variable, sa taille est indiquée dans `USERNAME_SIZE`.
    - Les paramètres `EMAIL_SIZE` et `EMAIL` correspondent respectivement 
    à la taille de l'email et à l'email. 
    `EMAIL_SIZE` est de taille fixe : 1 octet (0-255).
    `EMAIL` est de taille variable, sa taille est indiquée dans `EMAIl_SIZE`.

### LOGIN

Name : `LOGIN`

Hex code : `0x04`

Description : 
    Cette commande permet au CLIENT de s'autentifier auprès du SERVER. Cette 
    commande doit être envoyée avant toute autre commande qui nécéssite d'être 
    connecté.
    L'utilisateur peut utiliser son email à la place de son username pour se 
    login.
    Le SERVER va renvoyer le token même si le CLIENT vient d'utiliser le token 
    pour se login. Le CLIENT devrait vérifier si le SERVER lui renvoit un token
    différent que celui qu'il vient d'utiliser et mettre a jour le token qu'il
    a sauvegardé dans le cas où le SERVEUR l'aurait changé.

Parameters : `IS_TOKEN` `UNUSED` `TOKEN` `USERNAME_SIZE` `USERNAME` `PASSWORD_SIZE` `PASSWORD`
    - Le paramètre `IS_TOKEN` indique si le CLIENT se connecte a l'aide d'un 
    token ou non.
    Ce paramètre est de taille fixe, 1 bit.
    - Le paramètre `UNUSED` est actuellement inutilisé, il sert juste à combler 
    le vide laissé par le champ `IS_TOKEN`.
    Ce paramètre est de taille fixe, 7 bit.
    - Le paramètre `TOKEN` correspond au token que le CLIENT veut utiliser pour 
    se connecter. Il est envoyé seulement si `IS_TOKEN` est égal à 1.
    Ce paramètre est de taille fixe, 36 octets.
    - Les paramètres `UNSERNAME_SIZE` et `UNSERNAME` correspondent respectivement 
    à la taille du username et au username. 
    Il sont envoyé seulement si `IS_TOKEN` est égal à 0.
    `USERNAME_SIZE` est de taille fixe : 1 octet (0-255).
    `USERNAME` est de taille variable, sa taille est indiquée dans `USERNAME_SIZE`.
    - Les paramètres `PASSWORD_SIZE` et `PASSWORD` correspondent respectivement 
    à la taille du password et au password.
    Il sont envoyé seulement si `IS_TOKEN` est égal à 0.
    `PASSWORD_SIZE` est de taille fixe : 1 octet (0-255).
    `PASSWORD` est de taille variable, sa taille est indiquée dans `PASSWORD_SIZE`.

Errors : `ERR_PARAMETER_ERROR` `ERR_INVALID_LOGIN`

Response Data : `TOKEN` `USERNAME_SIZE` `USERNAME` `EMAIL_SIZE` `EMAIL`
    - Le champ `TOKEN` correspond au token que le CLIENT peut utiliser pour 
    se connecter la prochaine fois. 
    ATTENTION : Ce token est valide jusqu'à ce qu'il soit invalidé par un LOGOUT.
    Un LOGOUT va donc invalider *tous* les CLIENT qui utilisaint ce token, et ils
    devront se reconnecter.
    Ce paramètre est de taille fixe, 36 octets.
    - Les paramètres `UNSERNAME_SIZE` et `UNSERNAME` correspondent respectivement 
    à la taille du username et au username. 
    `USERNAME_SIZE` est de taille fixe : 1 octet (0-255).
    `USERNAME` est de taille variable, sa taille est indiquée dans `USERNAME_SIZE`.
    - Les paramètres `EMAIL_SIZE` et `EMAIL` correspondent respectivement 
    à la taille de l'email et à l'email. 
    `EMAIL_SIZE` est de taille fixe : 1 octet (0-255).
    `EMAIL` est de taille variable, sa taille est indiquée dans `EMAIl_SIZE`.


### LOGOUT

Name : `LOGOUT`

Hex code : `0x05`

Description : 
    Cette commande permet au CLIENT de réinitialiser son token. Cela a pour 
    conséquence de le déconnecter de tous ses apareils. Il devra resaisir ses
    identifiants pour se reconnecter la prochaine fois. Ceci permet de sécuriser
    le compte si on pense que le token à été compromis, ou qu'un apareil sur 
    lequel l'utilisateur était connecté à été piraté/volé.

Parameters : Aucun

Errors : `ERR_MISSING_LOGIN`

Response Data : Aucun



### LIST_FILES

Name : `LIST_FILES`

Hex code : `0x06`

Description : 
    Cette commande permet au CLIENT de lister les fichiers disponibles sur le SERVEUR.

Parameters : Aucuns

Errors : `ERR_MISSING_LOGIN`

Response Data : `NB_FILES`[`FILENAME_SIZE` `FILENAME` `TIMESTAMP` `PUBLIC` `PROJECT` `UNUSED` `MD5`]
    - Le champ `NB_FILES` contient le nombre de fichiers actuellement syncronisés 
    sur le server. De plus, cela indique le nombre de champs restant dans la réponse.
    Ce paramètre est de taille fixe : 2 octet (0-65535).
    Cette commande est particulière et renvoie une liste. Chaque élément de la liste
    possède son propore `FILENAME_SIZE` `FILENAME` `TIMESTAMP` et `MD5`.
    Donc les paramètres suivants peuvent aparaitre zéro ou plus d'une fois, toujours en 
    respectant l'ordre donné ci dessus. Chaque élément est **forcément** composé
    de **tous** les champs.
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du nom de fichier.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` contient le nom du fichier.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.
    - Le champ `TIMESTAMP` contient le timestamp de la dernière modification du fichier.
    Ce paramètre est de taille fixe : 8 octets (taille POSIX pour éviter bug de l'an 2038)
    - Le champ `PUBLIC` est un booleen permettant de savoir si le fichier est accessible
    publiquement. Si il est à 1, alors le fichier est public, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `PROJECT` est un booleen permettant de savoir si le fichier est un fichier
    de project. Si il est à 1, alors le fichier est un fichier de projet, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `UNUSED` est actuellement inutilisé, il sert juste à combler le 
    vide laissé par `PUBLIC`.
    Ce paramètre est de taille fixe : 6 bits.
    - Le champ `MD5` contient la somme md5 du contenu du fichier. Le client peut 
    l'utiliser pour comparer avec la sienne pour voir si il y a des différences 
    dans le contenu du fichier.
    Ce paramètre est de taille fixe : 16 octets (taille d'un checksum md5).

Exemple : 2 fichiers : `file.txt` et `image.png`
Code :     `[LIST_FILES][DATA_SIZE]`
Hex  :     `0x04        0x00       `
Bin  :     `00100       00000000000`
Exemple Response : 
Code :     `[NB_FILES][FILENAME_SIZE][FILENAME] [TIMESTAMP] [MD5] [FILENAME_SIZE][FILENAME] [TIMESTAMP] [MD5]`
Hex  :     `0x02      0x08           "file.txt" [TIMESTAMP] [MD5] 0x09           "image.png" [TIMESTAMP] [MD5]`
Bin  :     `00000010  00001000       "file.txt" [TIMESTAMP] [MD5] 00001001       "image.png" [TIMESTAMP] [MD5]`


### DOWNLOAD_FILE

Name : `DOWNLOAD_FILE`

Hex code : `0x07`

Description : 
    Cette commande permet au CLIENT de télécharger un fichier du SERVEUR.
    Particularité : 
    Si `CONTENT_SIZE` est mis à la valeur spéciale 2047, `CONTENT` aura une taille de 
    2 octets et contiendra le nombre de blocks à récupérer pour former le vrai `CONTENT`.
    Ces blocks seront envoyés a la suite du champ `CONTENT` et seront tous précédés par 
    leur taille sur 2 ocets.

Parameters : `FILENAME_SIZE` `FILENAME`
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du fichier.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` correspond au nom du fichier que le client veut téléchager.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.

Errors : `ERR_NO_SUCH_FILE` `ERR_PARAMETER_ERROR` `ERR_MISSING_LOGIN`

Response Data : `FILENAME_SIZE` `FILENAME` `TIMESTAMP` `PUBLIC` `PROJECT` `UNUSED` `CONTENT_SIZE` `CONTENT`
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du nom de fichier.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` contient le nom du fichier. Ceci est juste un rapel du 
    nom du fichier demandé.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.
    - Le champ `TIMESTAMP` contient le timestamp de la dernière modification du fichier.
    Ce paramètre est de taille fixe : 8 octets (taille POSIX pour éviter bug de l'an 2038)
    - Le champ `PUBLIC` est un booleen permettant de savoir si le fichier est accessible
    publiquement. Si il est à 1, alors le fichier est public, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `PROJECT` est un booleen permettant de savoir si le fichier est un fichier
    de project. Si il est à 1, alors le fichier est un fichier de projet, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `UNUSED` est actuellement inutilisé, il sert juste à combler le 
    vide et compléter l'octet utilisé par `CONTENT_SIZE`.
    Ce paramètre est de taille fixe : 3 bits.
    - Les champs `CONTENT_SIZE` et `CONTENT` correspondent respectivement à la taille 
    du contenu du fichier et le contenu lui même.
    `CONTENT_SIZE` est de taille fixe : 11 bits (0-2047)
    `CONTENT` est de taille variable, sa taille est indiquée dans `CONTENT_SIZE`.

Exemple : `FILENAME`= "file.txt"
Code :     `[DOWNLOAD_FILE][DATA_SIZE] [FILENAME_SIZE][FILENAME]`
Hex  :     `0x07           0x09        0x08           "file.txt"`
Bin  :     `00111          00000001001 00001000       "file.txt"`
Exemple Response : 
Code :     `[FILENAME_SIZE][FILENAME] [TIMESTAMP] [PUBLIC][UNUSED][CONTENT_SIZE][CONTENT]    `
Hex  :     `0x08           "file.txt" [TIMESTAMP] 0x0     0x0     0x0B          "Hello world"`
Bin  :     `00001000       "file.txt" [TIMESTAMP] 0       0000    00000001011   "Hello world"`


### UPLOAD_FILE

Name : `UPLOAD_FILE`

Hex code : `0x08`

Description : 
    Cette commande permet au CLIENT d'envoyer un fichier sur le SERVEUR.
    Attention : Le CLIENT peut au maximum avoir 65535 fichiers différents sur
    le SERVEUR. (limité par la taille du champ `NB_FILES` de la command `LIST_FILES`)
    Particularité : 
    Si `CONTENT_SIZE` est mis à la valeur spéciale 2047, `CONTENT` aura une taille de 
    2 octets et contiendra le nombre de blocks à récupérer pour former le vrai `CONTENT`.
    Ces blocks seront envoyés a la suite du champ `CONTENT` et seront tous précédés par 
    leur taille sur 2 ocets.

Parameters : `FILENAME_SIZE` `FILENAME` `TIMESTAMP` `PUBLIC` `UNUSED` `CONTENT_SIZE` `CONTENT`
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du fichier.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` correspond au nom du fichier que le client veut téléchager.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.
    - Le champ `TIMESTAMP` contient le timestamp de la dernière modification du fichier.
    Ce paramètre est de taille fixe : 8 octets (taille POSIX pour éviter bug de l'an 2038)
    - Le champ `PUBLIC` est un booleen permettant de savoir si le fichier est accessible
    publiquement. Si il est à 1, alors le fichier est public, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `UNUSED` est actuellement inutilisé, il sert juste à combler le 
    vide et compléter l'octet utilisé par `CONTENT_SIZE`.
    Ce paramètre est de taille fixe : 4 bits.
    - Les champs `CONTENT_SIZE` et `CONTENT` correspondent respectivement à la taille 
    du contenu du fichier et le contenu lui même.
    `CONTENT_SIZE` est de taille fixe : 11 bits (0-2047)
    `CONTENT` est de taille variable, sa taille est indiquée dans `CONTENT_SIZE`.

Errors : `ERR_PARAMETER_ERROR` `ERR_MISSING_LOGIN` `ERR_TOO_MANY_FILES`

Response Data : Aucune

Exemple : `FILENAME`= "file.txt"
Code :     `[UPLOAD_FILE][DATA_SIZE] [FILENAME_SIZE][FILENAME] [TIMESTAMP][PUBLIC][UNUSED][CONTENT_SIZE][CONTENT]`
Hex  :     `0x08         0x20        0x08           "file.txt" [TIMESTAMP]0x0     0x0     0x05          "Hello"`
Bin  :     `01000        00000100000 00001000       "file.txt" [TIMESTAMP]0       0000    00000000101   "Hello"`

### DELETE_FILE

Name : `DELETE_FILE`

Hex code : `0x09`

Description : 
    Cette commande permet au CLIENT de supprimer un fichier du SERVEUR.

Parameters : `FILENAME_SIZE` `FILENAME`
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du fichier.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` correspond au nom du fichier que le client veut téléchager.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.

Errors : `ERR_NO_SUCH_FILE` `ERR_PARAMETER_ERROR` `ERR_MISSING_LOGIN`

Response Data : Aucun


### LIST_FILE

Name : `LIST_FILE`

Hex code : `0x0A`

Description : 
    Cette commande permet au CLIENT de récupérer les informations d'un fichier
    du SERVEUR. Cela permet au CLIENT de comparer si son fichier local est à jour
    sans avoir à télécharger le fichier dans sa totalité.

Parameters : `FILENAME_SIZE` `FILENAME`
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du fichier.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` correspond au nom du fichier que le client veut téléchager.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.

Errors : `ERR_NO_SUCH_FILE` `ERR_PARAMETER_ERROR` `ERR_MISSING_LOGIN`

Response Data : `FILENAME_SIZE` `FILENAME` `TIMESTAMP` `PUBLIC` `PROJECT` `UNUSED` `MD5`
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du nom de fichier.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` contient le nom du fichier. Ceci est juste un rapel du 
    nom du fichier demandé.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.
    - Le champ `TIMESTAMP` contient le timestamp de la dernière modification du fichier.
    Ce paramètre est de taille fixe : 8 octets (taille POSIX pour éviter bug de l'an 2038)
    - Le champ `PUBLIC` est un booleen permettant de savoir si le fichier est accessible
    publiquement. Si il est à 1, alors le fichier est public, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `PROJECT` est un booleen permettant de savoir si le fichier est un fichier
    de project. Si il est à 1, alors le fichier est un fichier de projet, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `UNUSED` est actuellement inutilisé, il sert juste à compléter 
    l'octet utilisé par `PUBLIC`.
    Ce paramètre est de taille fixe : 6 bits.
    - Le champ `MD5` contient la somme md5 du contenu du fichier. Le client peut 
    l'utiliser pour comparer avec la sienne pour voir si il y a des différences 
    dans le contenu du fichier.
    Ce paramètre est de taille fixe : 16 octets (taille d'un checksum md5).


### UPLOAD_PROJECT

Name : `UPLOAD_PROJECT`

Hex code : `0x0C`

Description : 
    Cette commande permet au CLIENT d'envoyer un projet sur le SERVEUR.
    Cela équivaut à un upload du projet comme étant un fichier public, mais
    permet aussi au serveur de référencer ce fichier comme étant un fichier de projet.
    Les projets sont composés de 2 fichiers : un fichier contenant le projet lui-même, 
    et un fichier contenant la metadata du projet. Un projet n'est considéré valide
    et visible en tant que tel que si les 2 fichiers du projets ont étés upload.
    De plus, la date de modification du projet est basée sur celle du fichier de metadata, 
    donc il faut toujours upload le fichier de projet avant celui de metadata pour 
    eviter que les CLIENTS téléchargent le projet avant qu'il n'ai fini d'être upload. 
    A ce moment, le projet aparaitra dans la liste de projets retournés par `LIST_PROJECT`.
    Attention : Le CLIENT peut au maximum avoir 65535 fichiers différents sur
    le SERVEUR. (limité par la taille du champ `NB_FILES` de la command `LIST_FILES`)
    Particularité : 
    Si `CONTENT_SIZE` est mis à la valeur spéciale 2047, `CONTENT` aura une taille de 
    2 octets et contiendra le nombre de blocks à récupérer pour former le vrai `CONTENT`.
    Ces blocks seront envoyés a la suite du champ `CONTENT` et seront tous précédés par 
    leur taille sur 2 ocets.

Parameters : `NAME_SIZE` `NAME` `TIMESTAMP` `GAME_VERSION` `IS_METADATA` `UNUSED` `CONTENT_SIZE` `CONTENT`
    - Le champ `NAME_SIZE` contient le nombre de caractères du nom du projet.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `NAME` correspond au nom du projet que le client upload.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.
    - Le champ `TIMESTAMP` contient le timestamp de la dernière modification du fichier.
    Ce paramètre est de taille fixe : 8 octets (taille POSIX pour éviter bug de l'an 2038)
    - Le champ `GAME_VERSION` représente la version du jeu pour laquelle ce
    projet à été créé. Il ne peut pas être utilisé sur une version du jeu différente.
    Ce paramètre est de taille fixe : 1 octet (0-255)
    - Le champ `IS_METADATA` est un booleen permettant de savoir si le fichier 
    correspond au projet ou à son fichier de metadata. 
    Si il est à 1, alors le fichier est le fichier de metadata, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `UNUSED` est actuellement inutilisé, il sert juste à combler le 
    vide et compléter l'octet utilisé par `CONTENT_SIZE`.
    Ce paramètre est de taille fixe : 4 bits.
    - Les champs `CONTENT_SIZE` et `CONTENT` correspondent respectivement à la taille 
    du contenu du fichier et le contenu lui même.
    `CONTENT_SIZE` est de taille fixe : 11 bits (0-2047)
    `CONTENT` est de taille variable, sa taille est indiquée dans `CONTENT_SIZE`.

Errors : `ERR_PARAMETER_ERROR` `ERR_MISSING_LOGIN` `ERR_TOO_MANY_FILES`

Response Data : Aucune

Exemple : `FILENAME`= "file.txt"
Code :     `[UPLOAD_PROJECT][DATA_SIZE] [NAME_SIZE][NAME] [TIMESTAMP][IS_METADATA][UNUSED][CONTENT_SIZE] [CONTENT]`
Hex  :     `0x08            0x20        0x08       "project1" [TIMESTAMP]0x0          0x0     0x05           "Hello"`
Bin  :     `01000           00000100000 00001000   "project1" [TIMESTAMP]0            0000    00000000101    "Hello"`


### LIST_PROJECTS

Name : `LIST_PROJECTS`

Hex code : `0x0D`

Description : 
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 
    Cette commande permet au CLIENT de lister les projets existants sur le SERVEUR.
    Le CLIENT peut ensuite télécharger le ou les projets de son choix avec la commande 
    `DOWNLOAD_PUBLIC_FILE` en utilisant le nom de fichier du projet qu'il 
    souhaite télécharger. Les projets sont séparés en 2 fichiers pour permettre au 
    CLIENT de télécharger uniquement la metadata du projet sans avoir à télécharger 
    le projet en entier.

Parameters : Aucun

Errors : Aucune

Response Data : `NB_PROJECT` [`NAME_SIZE` `NAME` `OWNER_SIZE` `OWNER` `META_FILENAME_SIZE` `META_FILENAME` `FILENAME_SIZE` `FILENAME` `TIMESTAMP` `GAME_VERSION` `APPROVED` `UNUSED`]
    - Le champ `NB_PROJECT` contient le nombre de projets actuellement reconnus 
    par le server. De plus, cela indique le nombre de champs restant dans la réponse.
    Ce paramètre est de taille fixe : 4 octet (0-4294967295).
    Cette commande est particulière et renvoie une liste. Chaque élément de la liste
    possède ses propores paramètres.
    Donc les paramètres suivants peuvent aparaitre zéro ou plus d'une fois, toujours en 
    respectant l'ordre donné ci dessus. Chaque élément est **forcément** composé
    de **tous** les champs.
    - Le champ `NAME_SIZE` contient le nombre de caractères du nom du projet.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `NAME` correspond au nom du projet.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `NAME_SIZE`.
    - Le champ `OWNER_SIZE` contient le nombre de caractères du nom du créateur du projet.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `OWNER` correspond au nom du créateur du projet.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `OWNER_SIZE`.
    - Le champ `META_FILENAME_SIZE` contient le nombre de caractères du fichier 
    de metadata du projet.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `META_FILENAME` correspond au nom du fichier de metadata du projet.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `META_FILENAME_SIZE`.
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du fichier du projet.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` correspond au nom du fichier du projet.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.
    - Le champ `TIMESTAMP` contient le timestamp de la dernière modification du projet 
    (la date de modification du projet est basée sur la date de modification du 
    fichier de metadata).
    Ce paramètre est de taille fixe : 8 octets (taille POSIX pour éviter bug de l'an 2038)
    - Le champ `GAME_VERSION` indique la version du jeu compatible avec ce projet.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `APPROVED` est un booléen indiquant si le projet à été approuvé.
    Ce paramètre est de taille fixe : 1 bit.
    - Le champ `UNUSED` est actuellement initilisé et sert à compléter le vide.
    Ce paramètre est de taille fixe : 7 bits.


Exemple : 
Code :     `[LIST_PROJECTS][DATA_SIZE]  [NB_PROJECT] [NAME_SIZE][NAME][OWER_SIZE][OWNER][META_FILENAME_SIZE][META_FILENAME] [FILENAME_SIZE][FILENAME] [TIMESTAMP]`
Hex  :     `0x08           0x20         0x01         0x04       "toto" 0x04      "tata" 0x0C                "proj.meta.zip" 0x08           "proj.zip" [TIMESTAMP]`
Bin  :     `01000          00000100000  000000000001 00000100   "toto" 00000100  "tata" 00001101            "proj.meta.zip" 00001000       "proj.zip" [TIMESTAMP]`


### DELETE_PROJECT

Name : `DELETE_PROJECT`

Hex code : `0x0E`

Description : 
    Cette commande permet au CLIENT de supprimer un de ses projets du SERVEUR.
    Cette action est irréversible.

Parameters : `NAME_SIZE` `PROJECT_NAME`
    - Le champ `NAME_SIZE` contient le nombre de caractères du nom du projet.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `NAME` correspond au nom du projet.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `NAME_SIZE`.

Errors : `NO_SUCH_PROJECT`

Response Data : Aucune

Exemple : 
Code :     `[DELETE_PROJECT][DATA_SIZE] [NAME_SIZE] [NAME]`
Hex  :     `0x08            0x05        0x04        "toto"`
Bin  :     `01000           00000000101 00000100    "toto"`


### LIST_USERS

Name : `LIST_USERS`

Hex code : `0x10`

Description : 
    Cette commande permet au CLIENT de récupérer la liste des joueurs.
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 

Parameters : Aucuns

Errors : Aucune

Response Data : `NB_USERS`[ `USERNAME_SIZE` `USERNAME` ]
    - Le champ `NB_USERS` contient le nombre de joueurs actuellement reconnus 
    par le server. De plus, cela indique le nombre de champs restant dans la réponse.
    Ce paramètre est de taille fixe : 4 octet (0-4294967295).
    Cette commande est particulière et renvoie une liste. Chaque élément de la liste
    possède son propore `USERNAME_SIZE` et `USERNAME`.
    Donc les paramètres suivants peuvent aparaitre zéro ou plus d'une fois, toujours en 
    respectant l'ordre donné ci dessus. Chaque élément est **forcément** composé
    de **tous** les champs.
    - Le champ `USERNAME_SIZE` contient le nombre de caractères du username du joueur.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `USERNAME` contient le username du joueur.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `USERNAME_SIZE`.

Exemple Response Data : 
Code :     `[NB_USERS][USERNAME_SIZE][USERNAME][USERNAME_SIZE][USERNAME]`
Hex  :     `0x02      0x04           "Toto"  0x05           "Hello"`
Bin  :     `00000010  00000100       "Toto"  00000101       "Hello"`


### LIST_PUBLIC_FILES

Name : `LIST_PUBLIC_FILES`

Hex code : `0x14`

Description : 
    Cette commande permet au CLIENT de lister les fichiers publics d'un autre joueur
    disponibles sur le SERVEUR.
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 

Parameters : `USERNAME_SIZE` `USERNAME`
    - Le champ `USERNAME_SIZE` contient le nombre de caractères du username du joueur.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `USERNAME` contient le username du joueur duquel on veut récupérer 
    les fichiers publics.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `USERNAME_SIZE`.

Errors : `ERR_PARAMETER_ERROR` `ERR_UNKNOWN_USERNAME`

Response Data : `NB_FILES`[`FILENAME_SIZE` `FILENAME` `TIMESTAMP` `PUBLIC` `PROJECT` `UNUSED` `MD5`]
    - Le champ `NB_FILES` contient le nombre de fichiers actuellement syncronisés 
    sur le server. De plus, cela indique le nombre de champs restant dans la réponse.
    Ce paramètre est de taille fixe : 2 octet (0-65535).
    Cette commande est particulière et renvoie une liste. Chaque élément de la liste
    possède son propore `FILENAME_SIZE` `FILENAME` `TIMESTAMP` et `MD5`.
    Donc les paramètres suivants peuvent aparaitre zéro ou plus d'une fois, toujours en 
    respectant l'ordre donné ci dessus. Chaque élément est **forcément** composé
    de **tous** les champs.
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du nom de fichier.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` contient le nom du fichier.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.
    - Le champ `TIMESTAMP` contient le timestamp de la dernière modification du fichier.
    Ce paramètre est de taille fixe : 8 octets (taille POSIX pour éviter bug de l'an 2038)
    - Le champ `PUBLIC` est un booleen permettant de savoir si le fichier est accessible
    publiquement. Si il est à 1, alors le fichier est public, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `PROJECT` est un booleen permettant de savoir si le fichier est un fichier
    de project. Si il est à 1, alors le fichier est un fichier de projet, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `UNUSED` est actuellement inutilisé, il sert juste à compléter 
    l'octet utilisé par `PUBLIC`.
    Ce paramètre est de taille fixe : 6 bits.
    - Le champ `MD5` contient la somme md5 du contenu du fichier. Le client peut 
    l'utiliser pour comparer avec la sienne pour voir si il y a des différences 
    dans le contenu du fichier.
    Ce paramètre est de taille fixe : 16 octets (taille d'un checksum md5).

Exemple : 2 fichiers publics : `file.txt` et `image.png`
Code :     `[LIST_PUBLIC_FILES][DATA_SIZE] [USERNAME]         `
Hex  :     `0x14               0x11        "12312312312312312"`
Bin  :     `10100              00000010001 "12312312312312312"`
Exemple Response Data : 
Code :     `[NB_FILES][FILENAME_SIZE][FILENAME] [TIMESTAMP] [MD5] [FILENAME_SIZE][FILENAME] [TIMESTAMP] [MD5]`
Hex  :     `0x02      0x08           "file.txt" [TIMESTAMP] [MD5] 0x09           "image.png" [TIMESTAMP] [MD5]`
Bin  :     `00000010  00001000       "file.txt" [TIMESTAMP] [MD5] 00001001       "image.png" [TIMESTAMP] [MD5]`


### DOWNLOAD_PUBLIC_FILE

Name : `DOWNLOAD_PUBLIC_FILE`

Hex code : `0x15`

Description : 
    Cette commande permet au CLIENT de télécharger un fichier public d'un autre joueur
    du SERVEUR.
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 
    Particularité : 
    Si `CONTENT_SIZE` est mis à la valeur spéciale 2047, `CONTENT` aura une taille de 
    2 octets et contiendra le nombre de blocks à récupérer pour former le vrai `CONTENT`.
    Ces blocks seront envoyés a la suite du champ `CONTENT` et seront tous précédés par 
    leur taille sur 2 ocets.

Parameters : `USERNAME_SIZE` `USERNAME` `FILENAME_SIZE` `FILENAME`
    - Le champ `USERNAME_SIZE` contient le nombre de caractères du username du joueur.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `USERNAME` contient le username du joueur duquel on veut récupérer 
    les fichiers publics.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `USERNAME_SIZE`.
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du fichier.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` correspond au nom du fichier que le client veut téléchager.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.

Errors : `ERR_NO_SUCH_FILE` `ERR_PARAMETER_ERROR` `ERR_UNKNOWN_USERNAME`

Response Data : `FILENAME_SIZE` `FILENAME` `TIMESTAMP` `PUBLIC` `PROJECT` `UNUSED` `CONTENT_SIZE` `CONTENT`
    - Le champ `FILENAME_SIZE` contient le nombre de caractères du nom de fichier.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    - Le champ `FILENAME` contient le nom du fichier. Ceci est juste un rapel du 
    nom du fichier demandé.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `FILENAME_SIZE`.
    - Le champ `TIMESTAMP` contient le timestamp de la dernière modification du fichier.
    Ce paramètre est de taille fixe : 8 octets (taille POSIX pour éviter bug de l'an 2038)
    - Le champ `PUBLIC` est un booleen permettant de savoir si le fichier est accessible
    publiquement. Si il est à 1, alors le fichier est public, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `PROJECT` est un booleen permettant de savoir si le fichier est un fichier
    de project. Si il est à 1, alors le fichier est un fichier de projet, sinon non.
    Ce paramètre est de taille fixe : 1 bits.
    - Le champ `UNUSED` est actuellement inutilisé, il sert juste à combler le 
    vide et compléter l'octet utilisé par `CONTENT_SIZE`.
    Ce paramètre est de taille fixe : 3 bits.
    - Les champs `CONTENT_SIZE` et `CONTENT` correspondent respectivement à la taille 
    du contenu du fichier et le contenu lui même.
    `CONTENT_SIZE` est de taille fixe : 11 bits (0-2047)
    `CONTENT` est de taille variable, sa taille est indiquée dans `CONTENT_SIZE`.

Exemple : `FILENAME`= "file.txt"
Code :     `[DOWNLOAD_PUBLIC_FILE][DATA_SIZE] [USERNAME]          [FILENAME_SIZE][FILENAME]`
Hex  :     `0x15                  0x26        "12312312312312312" 0x08           "file.txt"`
Bin  :     `10101                 00000011010 "12312312312312312" 00001000       "file.txt"`
Exemple Response Data : 
Code :     `[FILENAME_SIZE][FILENAME] [TIMESTAMP] [UNUSED][CONTENT_SIZE][CONTENT]    `
Hex  :     `0x08           "file.txt" [TIMESTAMP] 0x0     0x0B          "Hello world"`
Bin  :     `00001000       "file.txt" [TIMESTAMP] 00000   00000001011   "Hello world"`


### GET_LANGUAGES

Name : `GET_LANGUAGES`

Hex code : `0x17`

Description : 
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 
    Cette commande permet au CLIENT de lister les languages qui possèdent un 
    wiki sur le SERVER.


Parameters : Aucuns

Errors : Aucune

Response Data : `VERSION` `NB_LANG` `NAME_SIZE` `NAME`
    - Le champ `VERSION` contient la version du wiki actuellement sur le SERVER.
    Le client est censé utiliser cette information pour ne pas afficher des 
    informations ne correspondant pas à la version du jeu.
    Ce paramètre est de taille fixe : 2 octet (0-65535).
    - Le champ `NB_LANG` contient le nombre de languages actuellement disponibles 
    sur le server. De plus, cela indique le nombre de champs restant dans la réponse.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    Cette commande est particulière et renvoie une liste. Chaque élément de la liste
    possède son propore `NAME_SIZE` et `NAME`.
    Donc les paramètres suivants peuvent aparaitre zéro ou plus d'une fois, toujours en 
    respectant l'ordre donné ci dessus. Chaque élément est **forcément** composé
    de **tous** les champs.
    - Les champs `NAME_SIZE` et `NAME` correspondent respectivement à la taille 
    du nom du language et le nom lui même.
    `NAME_SIZE` est de taille fixe : 8 bits (0-255)
    `NAME` est de taille variable, sa taille est indiquée dans `NAME_SIZE`.

Exemple Response Data : 
Code :     `[VERSION][NB_LANG][NAME_SIZE][CONTENT][NAME_SIZE][CONTENT]`
Hex  :     `0x08     0x2      0x03       "PHP"     0x06      "Python"`
Bin  :     `00001000 00000010 00000011   "PHP"     00000110  "Python"`


### GET_CATEGORIES

Name : `GET_CATEGORIES`

Hex code : `0x18`

Description : 
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 
    Cette commande permet au CLIENT de lister les catégories du wiki d'un language


Parameters : `LANG_SIZE` `LANG`
    - Les champs `LANG_SIZE` et `LANG` correspondent respectivement à la taille 
    du nom du language pour lequel le CLIENT demande la liste de carégories
    et le nom lui même.
    `LANG_SIZE` est de taille fixe : 8 bits (0-255)
    `LANG` est de taille variable, sa taille est indiquée dans `LANG_SIZE`.

Errors : `ERR_UNKNOWN_LANGUAGE`

Response Data : `VERSION` `NB_CATEG` `NAME_SIZE` `NAME`
    - Le champ `VERSION` contient la version du wiki actuellement sur le SERVER.
    Le client est censé utiliser cette information pour ne pas afficher des 
    informations ne correspondant pas à la version du jeu.
    Ce paramètre est de taille fixe : 2 octet (0-65535).
    - Le champ `NB_CATEG` contient le nombre de catégories pour ce language. 
    De plus, cela indique le nombre de champs restant dans la réponse.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    Cette commande est particulière et renvoie une liste. Chaque élément de la liste
    possède son propore `NAME_SIZE` et `NAME`.
    Donc les paramètres suivants peuvent aparaitre zéro ou plus d'une fois, toujours en 
    respectant l'ordre donné ci dessus. Chaque élément est **forcément** composé
    de **tous** les champs.
    - Les champs `NAME_SIZE` et `NAME` correspondent respectivement à la taille 
    du nom de la catégorie et le nom lui même.
    `NAME_SIZE` est de taille fixe : 8 bits (0-255)
    `NAME` est de taille variable, sa taille est indiquée dans `NAME_SIZE`.

Exemple Response Data : 
Code :     `[VERSION][NB_CATEG][NAME_SIZE][CONTENT][NAME_SIZE][CONTENT]`
Hex  :     `0x08     0x2      0x03       "Map"     0x06      "Player"`
Bin  :     `00001000 00000010 00000011   "Map"     00000110  "Player"`



### GET_FUNCTIONS_NAMES

Name : `GET_FUNCTIONS_NAMES`

Hex code : `0x19`

Description : 
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 
    Cette commande permet au CLIENT de lister les noms de fonction d'une catégorie
    d'un language sur le wiki


Parameters : `LANG_SIZE` `LANG` `CATEG_SIZE` `CATEG`
    - Les champs `LANG_SIZE` et `LANG` correspondent respectivement à la taille 
    du nom du language pour lequel le CLIENT demande la liste de fonctions
    et le nom lui même.
    `LANG_SIZE` est de taille fixe : 8 bits (0-255)
    `LANG` est de taille variable, sa taille est indiquée dans `LANG_SIZE`.
    - Les champs `CATEG_SIZE` et `CATEG` correspondent respectivement à la taille 
    du nom de la catégorie pour lequel le CLIENT demande la liste de fonctions
    et le nom lui même.
    `CATEG_SIZE` est de taille fixe : 8 bits (0-255)
    `CATEG` est de taille variable, sa taille est indiquée dans `CATEG_SIZE`.

Errors : `ERR_UNKNOWN_LANGUAGE` `ERR_UNKNOWN_CATEGORY`

Response Data : `VERSION` `NB_FUN` `NAME_SIZE` `NAME`
    - Le champ `VERSION` contient la version du wiki actuellement sur le SERVER.
    Le client est censé utiliser cette information pour ne pas afficher des 
    informations ne correspondant pas à la version du jeu.
    Ce paramètre est de taille fixe : 2 octet (0-65535).
    - Le champ `NB_FUN` contient le nombre de fonctions pour ce language et catégorie.
    De plus, cela indique le nombre de champs restant dans la réponse.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    Cette commande est particulière et renvoie une liste. Chaque élément de la liste
    possède son propore `NAME_SIZE` et `NAME`.
    Donc les paramètres suivants peuvent aparaitre zéro ou plus d'une fois, toujours en 
    respectant l'ordre donné ci dessus. Chaque élément est **forcément** composé
    de **tous** les champs.
    - Les champs `NAME_SIZE` et `NAME` correspondent respectivement à la taille 
    du nom de la fonction et le nom lui même.
    `NAME_SIZE` est de taille fixe : 8 bits (0-255)
    `NAME` est de taille variable, sa taille est indiquée dans `NAME_SIZE`.

Exemple Response Data : 
Code :     `[VERSION][NB_FUN][NAME_SIZE][CONTENT][NAME_SIZE][CONTENT]`
Hex  :     `0x08     0x2      0x03       "Map"     0x06      "Player"`
Bin  :     `00001000 00000010 00000011   "Map"     00000110  "Player"`



### GET_FUNCTIONS

Name : `GET_FUNCTIONS`

Hex code : `0x1A`

Description : 
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 
    Cette commande permet au CLIENT de lister les fonctions d'une catégorie
    d'un language sur le wiki.
    Cette commande retourne un JSON pour chaque fonction avec des informations 
    sur la fonction et comment la mettre en page.


Parameters : `LANG_SIZE` `LANG` `CATEG_SIZE` `CATEG`
    - Les champs `LANG_SIZE` et `LANG` correspondent respectivement à la taille 
    du nom du language pour lequel le CLIENT demande la liste de fonctions
    et le nom lui même.
    `LANG_SIZE` est de taille fixe : 8 bits (0-255)
    `LANG` est de taille variable, sa taille est indiquée dans `LANG_SIZE`.
    - Les champs `CATEG_SIZE` et `CATEG` correspondent respectivement à la taille 
    du nom de la catégorie pour lequel le CLIENT demande la liste de fonctions
    et le nom lui même.
    `CATEG_SIZE` est de taille fixe : 8 bits (0-255)
    `CATEG` est de taille variable, sa taille est indiquée dans `CATEG_SIZE`.

Errors : `ERR_UNKNOWN_LANGUAGE` `ERR_UNKNOWN_CATEGORY`

Response Data : `VERSION` `NB_FUN` `DATA_SIZE` `DATA`
    - Le champ `VERSION` contient la version du wiki actuellement sur le SERVER.
    Le client est censé utiliser cette information pour ne pas afficher des 
    informations ne correspondant pas à la version du jeu.
    Ce paramètre est de taille fixe : 2 octet (0-65535).
    - Le champ `NB_FUN` contient le nombre de fonctions pour ce language et catégorie.
    De plus, cela indique le nombre de champs restant dans la réponse.
    Ce paramètre est de taille fixe : 1 octet (0-255).
    Cette commande est particulière et renvoie une liste. Chaque élément de la liste
    possède son propore `DATA_SIZE` et `DATA`.
    Donc les paramètres suivants peuvent aparaitre zéro ou plus d'une fois, toujours en 
    respectant l'ordre donné ci dessus. Chaque élément est **forcément** composé
    de **tous** les champs.
    - Les champs `DATA_SIZE` et `DATA` correspondent respectivement à la taille 
    du contenu de la fonction et le contenu lui même.
    `DATA_SIZE` est de taille fixe : 2 octets (0-65535)
    `DATA` est de taille variable, sa taille est indiquée dans `NAME_SIZE`.

Exemple Response Data : 
Code :     `[VERSION][NB_FUN][NAME_SIZE][CONTENT][NAME_SIZE][CONTENT]`
Hex  :     `0x08     0x2      0x03       "Map"     0x06      "Player"`
Bin  :     `00001000 00000010 00000011   "Map"     00000110  "Player"`


### GET_FUNCTION

Name : `GET_FUNCTION`

Hex code : `0x1B`

Description : 
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 
    Cette commande permet au CLIENT de recupérer les informations du wiki d'une fonction.
    Cette commande retourne un JSON avec des informations 
    sur la fonction et comment la mettre en page.


Parameters : `LANG_SIZE` `LANG` `NAME_SIZE` `NAME`
    - Les champs `LANG_SIZE` et `LANG` correspondent respectivement à la taille 
    du nom du language pour lequel le CLIENT demande la liste de fonctions
    et le nom lui même.
    `LANG_SIZE` est de taille fixe : 8 bits (0-255)
    `LANG` est de taille variable, sa taille est indiquée dans `LANG_SIZE`.
    - Les champs `NAME_SIZE` et `NAME` correspondent respectivement à la taille 
    du nom de la fonction et le nom lui même.
    `NAME_SIZE` est de taille fixe : 8 bits (0-255)
    `NAME` est de taille variable, sa taille est indiquée dans `NAME_SIZE`.

Errors : `ERR_UNKNOWN_LANGUAGE` `ERR_UNKNOWN_FUNCTION`

Response Data : `VERSION` `DATA_SIZE` `DATA`
    - Le champ `VERSION` contient la version du wiki actuellement sur le SERVER.
    Le client est censé utiliser cette information pour ne pas afficher des 
    informations ne correspondant pas à la version du jeu.
    Ce paramètre est de taille fixe : 2 octet (0-65535).
    - Les champs `DATA_SIZE` et `DATA` correspondent respectivement à la taille 
    du contenu de la fonction et le contenu lui même.
    `DATA_SIZE` est de taille fixe : 2 octets (0-65535)
    `DATA` est de taille variable, sa taille est indiquée dans `NAME_SIZE`.

Exemple Response Data : 
Code :     `[VERSION][DATA_SIZE][DATA]`
Hex  :     `0x08     0x06      "Player"`
Bin  :     `00001000 00000110  "Player"`


### REPORT_CRASH

Name : `REPORT_CRASH`

Hex code : `0x1E`

Description : 
    Cette commande permet au CLIENT de reporter un bug.
    Cette commande est l'une des rares qui peut être envoyée avant `LOGIN`. 
    Particularité :
    Si cette commande est executée quand le CLIENT est login, elle prendra en 
    compte l'utilisateur qui reporte le bug. Sinon, elle le reportera anonymement.

Parameters : `STACKTRACE_SIZE` `STACKTRACE` `MESSAGE_SIZE` `MESSAGE` `SCENE_NAME_SIZE` `SCENE_NAME` `SOFTWARE` `UNUSED` `OS` `RAM_INT` `RAM_FLOAT` `UUID`
    - Le champ `STACKTRACE_SIZE` contient le nombre de caractères de la stacktrace.
    Ce parametre est de taille fixe : 2 octets (0-65535).
    - Le champ `STACKTRACE` contient la stacktrace de l'erreur que le CLIENT à rencontré.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `STACKTRACE_SIZE`.
    - Le champ `MESSAGE_SIZE` contient le nombre de caractères du message d'erreur.
    Ce parametre est de taille fixe : 2 octets (0-65535).
    - Le champ `MESSAGE` contient le message d'erreur que le CLIENT à rencontré.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `MESSAGE_SIZE`.
    - Le champ `SCENE_NAME_SIZE` contient le nombre de caractères du nom de la scene.
    Ce parametre est de taille fixe : 1 octets (0-255).
    - Le champ `SCENE_NAME` contient le nom de la scene où le CLIENT à rencontré l'erreur.
    Ce paramètre est de taille variable, sa taille est indiquée dans le champ `MESSAGE_SIZE`.
    - Le champ `SOFTWARE` indique si l'erreur est survenue dans le jeu ou dans l'editeur de 
    niveaux. 0 signifie le jeu, 1 signifie l'éditeur.
    Ce paramètre est de taille fixe : 1bit.
    - Le champ `UNUSED` est actuellement inutilisé, il permet de combler le vide 
    entre `SOFTWARE` et `OS`.
    Il est de taille fixe : 5bits;
    - Le champ `OS` représente l'os sur lequel l'erreur est survenue. Il peut 
    prendre 4 différentes valeurs : 0 pour un os inconnu, 1 pour linux, 2 pour 
    windows et 3 pour mac.
    Ce paramètre est de taille fixe : 2bits.
    - Le champ `RAM_INT` represente la partie entière du pourcentage de RAM utilisée
    par le client au moment de l'erreur.
    Ce paramètre est de taille fixe : 1octet(0-255).
    - Le champ `RAM_FLOAT` represente la partie flotante du pourcentage de RAM utilisée
    par le client au moment de l'erreur.
    Ce paramètre est de taille fixe : 1octet(0-255).
    - Le champ `UUID` est un identifiant unique généré par le CLIENT et qui permet 
    de les différencier. Le CLIENT est requis de stoker cet identifiant et de 
    toujours envoyer le même pour permettre au server de regroupper les rapports 
    de bugs venant du même CLIENT.
    Ce paramètre est de taille fixe : 36octets.
    
Errors : `ERR_PARAMETER_ERROR`

Response Data : Aucune

Exemple : Ram usage = 8.5%
Code :     `[REPORT_CRASH][DATA_SIZE] [STACKTRACE_SIZE][STACKTRACE]            [MESSAGE_SIZE]   [MESSAGE]            [SOFTWARE][UNUSED][OS] [RAM_INT][RAM_FLOAT][UUID]`
Hex  :     `0x1A          0x26        0x15             "file.cs:42: main():12" 0x12             "Index out of range" 0x00      0x00    0x02 0x08     0x05       [UUID]`
Bin  :     `11010         00000011010 0000000000010101 "file.cs:42: main():12" 0000000000010010 "Index out of range" 0         00000   10   00001000 00000101   [UUID]`



## PROTOCOL VERSION

Ce document documente la version 5.1.1 du protocole.
