{
  "title": "Character::attack",
  "action_cost": 5,
  "move_cost": 0,
  "signature": [
    {
      "value": "Character::attack($pos)"
    },
    {
      "value": "Character::attack($posX, $posY)"
    }
  ],
  "parameters": [
    {
      "name": "posX",
      "type": "int",
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Position en X de la destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Position X of the destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "optional": true
    },
    {
      "name": "posY",
      "type": "int",
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Position en Y de la destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Position Y of the destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "optional": true
    },
    {
      "name": "Pos",
      "type": "Vector",
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Position de la destination",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Position of the destination",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "optional": false
    }
  ],
  "description": [
    {
      "language": "fr",
      "content": [
        {
          "value": "Cette fonction utilise l'attaque de base du personnage dans une direction donnée.",
          "inline": false,
          "color": null
        }
      ]
    },
    {
      "language": "en",
      "content": [
        {
          "value": "Use the attack of the character in a defined direction.",
          "inline": false,
          "color": null
        }
      ]
    }
  ],
  "return_value": {
    "type": "bool",
    "description": [
      {
        "language": "fr",
        "content": [
          {
            "value": "true",
            "inline": false,
            "color": "code"
          },
          {
            "value": " si l'attaque a été appliquée. Sinon, ",
            "inline": true,
            "color": null
          },
          {
            "value": "false",
            "inline": true,
            "color": "code"
          },
          {
            "value": " Lorsqu'une erreur est survenue.",
            "inline": true,
            "color": null
          }
        ]
      },
      {
        "language": "en",
        "content": [
          {
            "value": "true",
            "inline": false,
            "color": "code"
          },
          {
            "value": " if the attack succeed, otherwise ",
            "inline": true,
            "color": null
          },
          {
            "value": "false",
            "inline": true,
            "color": "code"
          },
          {
            "value": " if an error occurred.",
            "inline": true,
            "color": null
          }
        ]
      }
    ]
  },
  "URLs": [],
  "examples": [
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Utilise l'attaque dans une direction donnée.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Use the attack in an specific direction.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "$res = Character::attack(10,10);\nConsole::log($res);",
          "output": "true"
        },
        {
          "language": "en",
          "code": "$res = Character::attack(10,10);\nConsole::log($res);",
          "output": "true"
        }
      ]
    },
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Récupère les positions du personnage et de l'ennemi le plus proche.",
              "inline": false,
              "color": null
            },
            {
              "value": "Utilise l'attaque en direction de l'ennemi si la portée du personnage est suffisante.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Get the positions of the character and the nearest enemy.",
              "inline": false,
              "color": null
            },
            {
              "value": "Use the skill in the direction of the enemy if the range of the character is sufficient.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "$pos_p = Character::position();\n$pos_e = Map::positionNearestEnemy();\n$range = Character::range();\nif (abs($pos_e->x - $pos_p->x) <= $range && abs($pos_e->y - $pos_p->y) <= $range) {\n\tCharacter::attack($pos_e);\n}",
          "output": ""
        },
        {
          "language": "en",
          "code": "$pos_p = Character::position();\n$pos_e = Map::positionNearestEnemy();\n$range = Character::range();\nif (abs($pos_e->x - $pos_p->x) <= $range && abs($pos_e->y - $pos_p->y) <= $range) {\n\tCharacter::attack($pos_e);\n}",
          "output": ""
        }
      ]
    }
  ]
}