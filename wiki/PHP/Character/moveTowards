{
  "title": "Character::moveTowards",
  "action_cost": 0,
  "move_cost": 1,
  "signature": [
    {
      "value": "Character::moveTowards($pos)"
    },
    {
      "value": "Character::moveTowards($posX, $posY)"
    }
  ],
  "parameters": [
    {
      "name": "posX",
      "type": "int",
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Position en X de la destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Position X of the destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "optional": true
    },
    {
      "name": "posY",
      "type": "int",
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Position en Y de la destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Position Y of the destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "optional": true
    },
    {
      "name": "Pos",
      "type": "Vector",
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Position de la destination",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Position of the destination",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "optional": false
    }
  ],
  "description": [
    {
      "language": "fr",
      "content": [
        {
          "value": "Cette fonction permet de déplacer le personnage pas à pas.",
          "inline": false,
          "color": null
        }
      ]
    },
    {
      "language": "en",
      "content": [
        {
          "value": "Allows to move the character to a specific position, step by step.",
          "inline": false,
          "color": null
        }
      ]
    }
  ],
  "return_value": {
    "type": "bool",
    "description": [
      {
        "language": "fr",
        "content": [
          {
            "value": "true",
            "inline": false,
            "color": "code"
          },
          {
            "value": " si le personnage a pu se déplacer vers la position demandée et qu'il reste des déplacements à effectuer. Sinon ",
            "inline": true,
            "color": null
          },
          {
            "value": "false",
            "inline": true,
            "color": "code"
          }
        ]
      },
      {
        "language": "en",
        "content": [
          {
            "value": "true",
            "inline": false,
            "color": "code"
          },
          {
            "value": " if the character has moved and if there are still moves to make in order to reach the destination. Otherwise ",
            "inline": true,
            "color": null
          },
          {
            "value": "false",
            "inline": true,
            "color": "code"
          }
        ]
      }
    ]
  },
  "URLs": [],
  "examples": [
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Exemple pour déplacer le personnage sur la case en face de l'ennemi le plus proche en plusieurs fois, ce qui permet de suivre l'ennemi si il se déplace.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Example, to move the character to the tile next to the nearest enemy, step by step, so the character can follow the enemy if the latter is moving.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "$enemyPos = Map::positionNearestEnemy();\n$enemyPos->x = $enemyPos->x - 1;\nwhile (Character::moveTowards($enemyPos)) {\n\t$enemyPos = Map::positionNearestEnemy();\n\t$enemyPos->x = $enemyPos->x - 1;\n}",
          "output": null
        },
        {
          "language": "en",
          "code": "$enemyPos = Map::positionNearestEnemy();\n$enemyPos->x = $enemyPos->x - 1;\nwhile (Character::moveTowards($enemyPos)) {\n\t$enemyPos = Map::positionNearestEnemy();\n\t$enemyPos->x = $enemyPos->x - 1;\n}",
          "output": null
        }
      ]
    }
  ]
}