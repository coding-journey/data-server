{
  "title": "Character::drinkPotion",
  "action_cost": 5,
  "move_cost": 0,
  "signature": [
    {
      "value": "Character::drinkPotion()"
    }
  ],
  "parameters": [],
  "description": [
    {
      "language": "fr",
      "content": [
        {
          "value": "Cette fonction utilise une potion possédée par le personnage, selon un type donné.",
          "inline": false,
          "color": null
        }
      ]
    },
    {
      "language": "en",
      "content": [
        {
          "value": "Use a potion held by the character, according to its type.",
          "inline": false,
          "color": null
        }
      ]
    }
  ],
  "return_value": {
    "type": "bool",
    "description": [
      {
        "language": "fr",
        "content": [
          {
            "value": "true",
            "inline": false,
            "color": "code"
          },
          {
            "value": " si la potion a été utilisé avec succès. Sinon ",
            "inline": true,
            "color": null
          },
          {
            "value": "false",
            "inline": true,
            "color": "code"
          }
        ]
      },
      {
        "language": "en",
        "content": [
          {
            "value": "true",
            "inline": false,
            "color": "code"
          },
          {
            "value": " if the potion has been used. Otherwise ",
            "inline": true,
            "color": null
          },
          {
            "value": "false",
            "inline": true,
            "color": "code"
          }
        ]
      }
    ]
  },
  "URLs": [],
  "examples": [
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Vérifie le retour de la fonction pour une potion possédée (Speed), et une potion non possédée (Strength).",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Check the return value of the function for a 'Speed' potion held, and a 'Strength' potion not held by the character.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "Console::log(Character::drinkPotion(\"Speed\"));\nConsole::log(Character::drinkPotion(\"Strength\"));",
          "output": "true\nfalse"
        },
        {
          "language": "en",
          "code": "Console::log(Character::drinkPotion(\"Speed\"));\nConsole::log(Character::drinkPotion(\"Strength\"));",
          "output": "true\nfalse"
        }
      ]
    },
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Ramasse et utilise une potion de type Speed, en affichant si elle est possédée ou non par le personnage.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Pick and use a 'Speed' potion, displaying whether or not it is held by the character.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "Console::log(Character::hasPotion(\"Speed\")); // ne possède pas encore la potion\nCharacter::pickPotion();\nConsole::log(Character::hasPotion(\"Speed\")); // possède la potion\nCharacter::drinkPotion(\"Speed\");\nConsole::log(Character::hasPotion(\"Speed\")); // ne possède plus la potion après utilisation",
          "output": "false\ntrue\nfalse"
        },
        {
          "language": "en",
          "code": "Console::log(Character::hasPotion(\"Speed\")); // does not have the potion yet\nCharacter::pickPotion();\nConsole::log(Character::hasPotion(\"Speed\")); // has the potion\nCharacter::drinkPotion(\"Speed\");\nConsole::log(Character::hasPotion(\"Speed\")); // does not have the potion anymore after usage",
          "output": "false\ntrue\nfalse"
        }
      ]
    }
  ]
}