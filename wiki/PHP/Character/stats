{
  "title": "Character::stats",
  "action_cost": 0,
  "move_cost": 0,
  "signature": [
    {
      "value": "Character::stats()"
    }
  ],
  "parameters": [],
  "description": [
    {
      "language": "fr",
      "content": [
        {
          "value": "Récupère les statistiques du personnage à un instant donné.",
          "inline": false,
          "color": null
        },
        {
          "value": "La fonction peut être utilisée pour vérifier l'état de certaines statistiques avant d'exécuter une action.",
          "inline": true,
          "color": null
        }
      ]
    },
    {
      "language": "en",
      "content": [
        {
          "value": "Get the current statistics of the character.",
          "inline": false,
          "color": null
        },
        {
          "value": "Can be used to check the state of any character statistics before acting.",
          "inline": true,
          "color": null
        }
      ]
    }
  ],
  "return_value": {
    "type": "Stats",
    "description": [
      {
        "language": "fr",
        "content": [
          {
            "value": "Contient les statistiques du personnage à un instant donné.",
            "inline": false,
            "color": null
          }
        ]
      },
      {
        "language": "en",
        "content": [
          {
            "value": "Hold the current statistics of the character.",
            "inline": false,
            "color": null
          }
        ]
      }
    ]
  },
  "URLs": [],
  "examples": [
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Ce code récupère et affiche les statistiques du personnage.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Get and display the current statistics of the character.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "$stats = Character::stats();\nConsole::log($stats);",
          "output": "Stats(hp = 80, hp_max = 100, position = Vector(5, 8), strength = 20, range = 2, pa = 5, pa_max = 10, pm = 5, pm_max = 10)"
        },
        {
          "language": "en",
          "code": "$stats = Character::stats();\nConsole::log($stats);",
          "output": "Stats(hp = 80, hp_max = 100, position = Vector(5, 8), strength = 20, range = 2, pa = 5, pa_max = 10, pm = 5, pm_max = 10)"
        }
      ]
    },
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Ce code vérifie les points de mouvement du personnage avant d'exécuter un déplacement.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Check the move points of the character before using a move.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "$stats = Character::stats();\nif ($stats->pm >= 5) {\n\tCharacter::move(10,10);\n}",
          "output": ""
        },
        {
          "language": "en",
          "code": "$stats = Character::stats();\nif ($stats->pm >= 5) {\n\tCharacter::move(10,10);\n}",
          "output": ""
        }
      ]
    }
  ]
}