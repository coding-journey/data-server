{
  "title": "Character.PM",
  "action_cost": 0,
  "move_cost": 0,
  "signature": [
    {
      "value": "Character.PM()"
    }
  ],
  "parameters": [],
  "description": [
    {
      "language": "fr",
      "content": [
        {
          "value": "Récupère les points de mouvement actuels du personnage (Points utilisés pour chaque fonction de mouvement appelée).",
          "inline": false,
          "color": null
        },
        {
          "value": "Lorsque les points d'action et de mouvement du personnage sont à 0, son tour est terminé. Les points sont réinitialisés au tour suivant.",
          "inline": false,
          "color": null
        }
      ]
    },
    {
      "language": "en",
      "content": [
        {
          "value": "Get the current move points of the character (points used for each move function called).",
          "inline": false,
          "color": null
        },
        {
          "value": "When both action points and move points are null, the turn of the character is over. Points are reset when the next turn starts.",
          "inline": false,
          "color": null
        }
      ]
    }
  ],
  "return_value": {
    "type": "int",
    "description": [
      {
        "language": "fr",
        "content": [
          {
            "value": "Contient les points d'action actuels du personnage.",
            "inline": false,
            "color": null
          }
        ]
      },
      {
        "language": "en",
        "content": [
          {
            "value": "Holds the current action points of the character.",
            "inline": false,
            "color": null
          }
        ]
      }
    ]
  },
  "URLs": [],
  "examples": [
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Ce code affiche les points de mouvement du personnage.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Displays the current move points of the character.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "pm = Character.PM()\nConsole.log(pm)",
          "output": "2"
        },
        {
          "language": "en",
          "code": "pm = Character.PM()\nConsole.log(pm)",
          "output": "2"
        }
      ]
    },
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Ce code vérifie les points de mouvement du personnage avant d'exécuter un déplacement.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Check if the move points of the character are sufficient before using a move.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "pm = Character.PM()\nif pm >= 5:\n\tCharacter.move(10,10)",
          "output": ""
        },
        {
          "language": "en",
          "code": "pm = Character.PM()\nif pm >= 5:\n\tCharacter.move(10,10)",
          "output": ""
        }
      ]
    },
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Note: la valeur récupérée via PM est la même que celle contenue dans la structure Stats.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Note that the value get with PM is the same as the one held in the Stats structure",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "pm = Character.PM()\nstats = Character.stats()\nConsole.log(pm)\nConsole.log(stats.pm)",
          "output": "2\n2"
        },
        {
          "language": "en",
          "code": "pm = Character.PM()\nstats = Character.stats()\nConsole.log(pm)\nConsole.log(stats.pm)",
          "output": "2\n2"
        }
      ]
    }
  ]
}