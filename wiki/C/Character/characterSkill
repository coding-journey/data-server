{
  "title": "characterSkill",
  "action_cost": 10,
  "move_cost": 0,
  "signature": [
    {
      "value": "characterSkill(x, y)"
    }
  ],
  "parameters": [
    {
      "name": "x",
      "type": "int",
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Position en X de la destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Position X of the destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "optional": true
    },
    {
      "name": "y",
      "type": "int",
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Position en Y de la destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Position Y of the destination",
              "inline": false,
              "color": null
            },
            {
              "value": "Default : 0",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "optional": true
    }
  ],
  "description": [
    {
      "language": "fr",
      "content": [
        {
          "value": "Cette fonction utilise la capacité spéciale du personnage dans une direction donnée.",
          "inline": false,
          "color": null
        }
      ]
    },
    {
      "language": "en",
      "content": [
        {
          "value": "Use the special skill of the character in a defined direction.",
          "inline": false,
          "color": null
        }
      ]
    }
  ],
  "return_value": {
    "type": "int",
    "description": [
      {
        "language": "fr",
        "content": [
          {
            "value": "1",
            "inline": false,
            "color": "code"
          },
          {
            "value": " si l'attaque a été appliquée. Sinon, ",
            "inline": true,
            "color": null
          },
          {
            "value": "0",
            "inline": true,
            "color": "code"
          },
          {
            "value": " Lorsqu'une erreur est survenue.",
            "inline": true,
            "color": null
          }
        ]
      },
      {
        "language": "en",
        "content": [
          {
            "value": "1",
            "inline": false,
            "color": "code"
          },
          {
            "value": " if the skill succeed, otherwise ",
            "inline": true,
            "color": null
          },
          {
            "value": "0",
            "inline": true,
            "color": "code"
          },
          {
            "value": " if an error occurred.",
            "inline": true,
            "color": null
          }
        ]
      }
    ]
  },
  "URLs": [],
  "examples": [
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Utilise l'attaque spéciale dans une direction donnée.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Use the special skill in an specific direction.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "int res = characterSkill(10,10);\nconsoleLg(res);",
          "output": "1"
        },
        {
          "language": "en",
          "code": "int res = characterSkill(10,10);\nconsoleLg(res);",
          "output": "1"
        }
      ]
    },
    {
      "description": [
        {
          "language": "fr",
          "content": [
            {
              "value": "Utilise l'attaque spéciale en direction de l'ennemi le plus proche.",
              "inline": false,
              "color": null
            }
          ]
        },
        {
          "language": "en",
          "content": [
            {
              "value": "Use the special skill in the direction of the nearest enemy.",
              "inline": false,
              "color": null
            }
          ]
        }
      ],
      "code_examples": [
        {
          "language": "fr",
          "code": "Vector pos = mapPositionNearestEnemy();\ncharacterSkill(pos);",
          "output": ""
        },
        {
          "language": "en",
          "code": "Vector pos = mapPositionNearestEnemy();\ncharacterSkill(pos);",
          "output": ""
        }
      ]
    }
  ]
}